USE `ritmbs`;
DROP TABLE IF EXISTS `propertytype`;
CREATE TABLE `propertytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `propertytype`(`name`) VALUES ('Flat'),('House');

DROP TABLE IF EXISTS `structureproperty`;
CREATE TABLE `structureproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_structure` int(11) NOT NULL,
  `id_property` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `structure` ADD COLUMN isEnable character not null;

ALTER TABLE `user` ADD COLUMN `idritmuser` CHAR(26) NOT NULL  AFTER `password`;

ALTER TABLE `structure` ADD COLUMN `videolink` VARCHAR(45) NULL  AFTER `taxNumber` ;

ALTER TABLE `structure` ADD COLUMN `haveroomavailable` character not null default '1';

ALTER TABLE `room` ADD COLUMN `availableforbooking` character not null default '1';

ALTER TABLE `user` ADD COLUMN `id_language` character not null default '0';

ALTER TABLE `user` ADD COLUMN `id_role` int(11) NULL DEFAULT 1;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `role`(id, `name`,`description`) VALUES (1,'Admin','Role as admin'),(2,'User','Role as user');

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `permission`(`name`,`description`) VALUES ('goCreateAccount','Create account'),('goUpdateAccount','Update account')
,('creatAccount',' Create account')
,('updateBookingDates','Update booking')
,('updateRoom','Update room')
,('updateNrGuests','Update Number Of Guest')
,('updateExtras','Update extras')
,('updateConvention','Update convention')
,('displayQuantitySelect','Display quantity select')
,('saveUpdateBooking','Save Update Booking')
,('goAddBookingFromPlanner','Add booking from planner')
,('goAddNewBooking','Add new booking')
,('goAddNewBookingFromGuest','Add new booking from guest')
,('goUpdateBooking','Update booking')
,('goUpdateBookingFromPlanner','Update booking from planner')
,('findAllBookingsJson','Find all booking')
,('findAllBookingsByStartDateAndLengthOfStay','Find all booking')
,('checkBookingDates','Check booking date')
,('checkBookingDatesNotNull','Check booking date')
,('deleteBooking','Delete booking')
,('goOnlineBookings','Booking online')
,('findAllConventions','Find convention')
,('goExport','Export')
,('findAllExtras','Findk extras')
,('goFindAllExtraPriceLists','Find extras price list')
,('findAllExtraPriceLists','Find extras price list')
,('findExtraPriceListItems','Find extras price list')
,('findExtraPriceListItemsJson','Find extras price list')
,('updateExtraPriceListItems','Update extras price list')
,('findAllFacilities','Find facilities')
,('findAllGuests','Find guest')
,('goUpdateGuestsFromPlanner','Update guest from planner')
,('goAboutInfo','About info')
,('home','Home page')
,('goLogin','Login')
,('findAllImages','Find images')
,('login','Login')
,('logout','Logout')
,('mobile','Mobile')
,('online','Online')
,('goOnlineBookingRooms','Booking room online')
,('goOnlineBookingFinal','Booking final online')
,('goOnlineBookingResult','Booking result online')
,('calculateTotalCost','Calculate total cost')
,('goPayPalPayment','Paypal payment')
,('findAllPermissions','Find permission')
,('findAllPermissionsJson','Find permission')
,('findAllPropertyTypes','Find property type')
,('findAllPropertyTypesJson','Find property type')
,('findAllRoles','Find role')
,('findAllRolesJson','Find role')
,('findAllRooms','Find room')
,('findAllRoomsJson','Find room')
,('findAllTreeRoomsJson','Find room')
,('goUpdateRoomFacilities','Update room facilities')
,('goFindAllRoomPriceLists','Find room price list')
,('toBlankPage','Blank page')
,('findAllRoomPriceLists','Find room price list')
,('findRoomPriceListItems','Find room price list')
,('findRoomPriceListItemsJson','Find room price list')
,('updateRoomPriceListItems','Update room price list')
,('findAllRoomTypes','Find room type')
,('findAllRoomTypesJson','Find room type')
,('findAllSeasons','Find season')
,('goUpdateDetails','Update details')
,('updateAccount','Update account')
,('resetstructure','Reset structure')
,('PropertyTypeResource.save','Save property type')
,('PropertyTypeResource.update','Update property type')
,('PropertyTypeResource.delete','Delete property type')
,('FacilityResource.insert','Insert facility')
,('FacilityResource.update','Update facility')
,('FacilityResource.delete','Delete facility')
,('RoomTypeResource.save','Save room type')
,('RoomTypeResource.update','Update room type')
,('RoomTypeResource.delete','Delete room type')
,('goWizard','Setting wizard');

DROP TABLE IF EXISTS `rolepermission`;
CREATE TABLE `rolepermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Initial permission for admin
INSERT INTO `rolepermission`(`id_role`,`id_permission`) VALUES (1,(SELECT id from `permission` WHERE name = 'goLogin'))
,(1,(SELECT id from `permission` WHERE name = 'login'))
,(1,(SELECT id from `permission` WHERE name = 'creatAccount'))
,(1,(SELECT id from `permission` WHERE name = 'home'))
,(1,(SELECT id from `permission` WHERE name = 'logout'))
,(1,(SELECT id from `permission` WHERE name = 'findAllPermissions'))
,(1,(SELECT id from `permission` WHERE name = 'findAllPermissionsJson'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRoles'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRolesJson'))
,(1,(SELECT id from `permission` WHERE name = 'updateAccount'))
,(1,(SELECT id from `permission` WHERE name = 'resetstructure'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateDetails'))
,(1,(SELECT id from `permission` WHERE name = 'goCreateAccount'))
,(1,(SELECT id from `permission` WHERE name = 'updateBookingDates'))
,(1,(SELECT id from `permission` WHERE name = 'updateRoom'))
,(1,(SELECT id from `permission` WHERE name = 'updateNrGuests'))
,(1,(SELECT id from `permission` WHERE name = 'updateExtras'))
,(1,(SELECT id from `permission` WHERE name = 'updateConvention'))
,(1,(SELECT id from `permission` WHERE name = 'displayQuantitySelect'))
,(1,(SELECT id from `permission` WHERE name = 'saveUpdateBooking'))
,(1,(SELECT id from `permission` WHERE name = 'goAddBookingFromPlanner'))
,(1,(SELECT id from `permission` WHERE name = 'goAddNewBooking'))
,(1,(SELECT id from `permission` WHERE name = 'goAddNewBookingFromGuest'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateBooking'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateBookingFromPlanner'))
,(1,(SELECT id from `permission` WHERE name = 'findAllBookingsJson'))
,(1,(SELECT id from `permission` WHERE name = 'findAllBookingsByStartDateAndLengthOfStay'))
,(1,(SELECT id from `permission` WHERE name = 'checkBookingDates'))
,(1,(SELECT id from `permission` WHERE name = 'checkBookingDatesNotNull'))
,(1,(SELECT id from `permission` WHERE name = 'deleteBooking'))
,(1,(SELECT id from `permission` WHERE name = 'goOnlineBookings'))
,(1,(SELECT id from `permission` WHERE name = 'findAllConventions'))
,(1,(SELECT id from `permission` WHERE name = 'goExport'))
,(1,(SELECT id from `permission` WHERE name = 'findAllExtras'))
,(1,(SELECT id from `permission` WHERE name = 'goFindAllExtraPriceLists'))
,(1,(SELECT id from `permission` WHERE name = 'findAllExtraPriceLists'))
,(1,(SELECT id from `permission` WHERE name = 'findExtraPriceListItems'))
,(1,(SELECT id from `permission` WHERE name = 'findExtraPriceListItemsJson'))
,(1,(SELECT id from `permission` WHERE name = 'updateExtraPriceListItems'))
,(1,(SELECT id from `permission` WHERE name = 'findAllFacilities'))
,(1,(SELECT id from `permission` WHERE name = 'findAllGuests'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateGuestsFromPlanner'))
,(1,(SELECT id from `permission` WHERE name = 'goAboutInfo'))
,(1,(SELECT id from `permission` WHERE name = 'findAllImages'))
,(1,(SELECT id from `permission` WHERE name = 'mobile'))
,(1,(SELECT id from `permission` WHERE name = 'online'))
,(1,(SELECT id from `permission` WHERE name = 'goOnlineBookingRooms'))
,(1,(SELECT id from `permission` WHERE name = 'goOnlineBookingFinal'))
,(1,(SELECT id from `permission` WHERE name = 'goOnlineBookingResult'))
,(1,(SELECT id from `permission` WHERE name = 'calculateTotalCost'))
,(1,(SELECT id from `permission` WHERE name = 'goPayPalPayment'))
,(1,(SELECT id from `permission` WHERE name = 'findAllPropertyTypes'))
,(1,(SELECT id from `permission` WHERE name = 'findAllPropertyTypesJson'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRooms'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRoomsJson'))
,(1,(SELECT id from `permission` WHERE name = 'findAllTreeRoomsJson'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateRoomFacilities'))
,(1,(SELECT id from `permission` WHERE name = 'goFindAllRoomPriceLists'))
,(1,(SELECT id from `permission` WHERE name = 'toBlankPage'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRoomPriceLists'))
,(1,(SELECT id from `permission` WHERE name = 'findRoomPriceListItems'))
,(1,(SELECT id from `permission` WHERE name = 'findRoomPriceListItemsJson'))
,(1,(SELECT id from `permission` WHERE name = 'updateRoomPriceListItems'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRoomTypes'))
,(1,(SELECT id from `permission` WHERE name = 'findAllRoomTypesJson'))
,(1,(SELECT id from `permission` WHERE name = 'findAllSeasons'))
,(1,(SELECT id from `permission` WHERE name = 'PropertyTypeResource.save'))
,(1,(SELECT id from `permission` WHERE name = 'PropertyTypeResource.update'))
,(1,(SELECT id from `permission` WHERE name = 'PropertyTypeResource.delete'))
,(1,(SELECT id from `permission` WHERE name = 'FacilityResource.insert'))
,(1,(SELECT id from `permission` WHERE name = 'FacilityResource.update'))
,(1,(SELECT id from `permission` WHERE name = 'FacilityResource.delete'))
,(1,(SELECT id from `permission` WHERE name = 'RoomTypeResource.save'))
,(1,(SELECT id from `permission` WHERE name = 'RoomTypeResource.update'))
,(1,(SELECT id from `permission` WHERE name = 'RoomTypeResource.delete'))
,(1,(SELECT id from `permission` WHERE name = 'goUpdateAccount'))
,(1,(SELECT id from `permission` WHERE name = 'goWizard'));

-- Initial permission for normal user
INSERT INTO `rolepermission`(`id_role`,`id_permission`) VALUES (1,(SELECT id from `permission` WHERE name = 'goLogin'))
,(2,(SELECT id from `permission` WHERE name = 'login'))
,(2,(SELECT id from `permission` WHERE name = 'creatAccount'))
,(2,(SELECT id from `permission` WHERE name = 'home'))
,(2,(SELECT id from `permission` WHERE name = 'logout'))
,(2,(SELECT id from `permission` WHERE name = 'findAllPermissions'))
,(2,(SELECT id from `permission` WHERE name = 'findAllPermissionsJson'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRoles'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRolesJson'))
,(2,(SELECT id from `permission` WHERE name = 'resetstructure'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateDetails'))
,(2,(SELECT id from `permission` WHERE name = 'goCreateAccount'))
,(2,(SELECT id from `permission` WHERE name = 'updateBookingDates'))
,(2,(SELECT id from `permission` WHERE name = 'updateRoom'))
,(2,(SELECT id from `permission` WHERE name = 'updateNrGuests'))
,(2,(SELECT id from `permission` WHERE name = 'updateExtras'))
,(2,(SELECT id from `permission` WHERE name = 'updateConvention'))
,(2,(SELECT id from `permission` WHERE name = 'displayQuantitySelect'))
,(2,(SELECT id from `permission` WHERE name = 'saveUpdateBooking'))
,(2,(SELECT id from `permission` WHERE name = 'goAddBookingFromPlanner'))
,(2,(SELECT id from `permission` WHERE name = 'goAddNewBooking'))
,(2,(SELECT id from `permission` WHERE name = 'goAddNewBookingFromGuest'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateBooking'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateBookingFromPlanner'))
,(2,(SELECT id from `permission` WHERE name = 'findAllBookingsJson'))
,(2,(SELECT id from `permission` WHERE name = 'findAllBookingsByStartDateAndLengthOfStay'))
,(2,(SELECT id from `permission` WHERE name = 'checkBookingDates'))
,(2,(SELECT id from `permission` WHERE name = 'checkBookingDatesNotNull'))
,(2,(SELECT id from `permission` WHERE name = 'deleteBooking'))
,(2,(SELECT id from `permission` WHERE name = 'goOnlineBookings'))
,(2,(SELECT id from `permission` WHERE name = 'findAllConventions'))
,(2,(SELECT id from `permission` WHERE name = 'goExport'))
,(2,(SELECT id from `permission` WHERE name = 'findAllExtras'))
,(2,(SELECT id from `permission` WHERE name = 'goFindAllExtraPriceLists'))
,(2,(SELECT id from `permission` WHERE name = 'findAllExtraPriceLists'))
,(2,(SELECT id from `permission` WHERE name = 'findExtraPriceListItems'))
,(2,(SELECT id from `permission` WHERE name = 'findExtraPriceListItemsJson'))
,(2,(SELECT id from `permission` WHERE name = 'updateExtraPriceListItems'))
,(2,(SELECT id from `permission` WHERE name = 'findAllFacilities'))
,(2,(SELECT id from `permission` WHERE name = 'findAllGuests'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateGuestsFromPlanner'))
,(2,(SELECT id from `permission` WHERE name = 'goAboutInfo'))
,(2,(SELECT id from `permission` WHERE name = 'findAllImages'))
,(2,(SELECT id from `permission` WHERE name = 'mobile'))
,(2,(SELECT id from `permission` WHERE name = 'online'))
,(2,(SELECT id from `permission` WHERE name = 'goOnlineBookingRooms'))
,(2,(SELECT id from `permission` WHERE name = 'goOnlineBookingFinal'))
,(2,(SELECT id from `permission` WHERE name = 'goOnlineBookingResult'))
,(2,(SELECT id from `permission` WHERE name = 'calculateTotalCost'))
,(2,(SELECT id from `permission` WHERE name = 'goPayPalPayment'))
,(2,(SELECT id from `permission` WHERE name = 'findAllPropertyTypes'))
,(2,(SELECT id from `permission` WHERE name = 'findAllPropertyTypesJson'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRooms'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRoomsJson'))
,(2,(SELECT id from `permission` WHERE name = 'findAllTreeRoomsJson'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateRoomFacilities'))
,(2,(SELECT id from `permission` WHERE name = 'goFindAllRoomPriceLists'))
,(2,(SELECT id from `permission` WHERE name = 'toBlankPage'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRoomPriceLists'))
,(2,(SELECT id from `permission` WHERE name = 'findRoomPriceListItems'))
,(2,(SELECT id from `permission` WHERE name = 'findRoomPriceListItemsJson'))
,(2,(SELECT id from `permission` WHERE name = 'updateRoomPriceListItems'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRoomTypes'))
,(2,(SELECT id from `permission` WHERE name = 'findAllRoomTypesJson'))
,(2,(SELECT id from `permission` WHERE name = 'findAllSeasons'))
,(2,(SELECT id from `permission` WHERE name = 'goUpdateAccount'))
,(2,(SELECT id from `permission` WHERE name = 'goWizard'));

DROP TABLE IF EXISTS `publishing`;
CREATE TABLE `publishing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modify` datetime,
  `id_user_owner` int(11) NOT NULL,
  `id_user_publisher` int(11),
  `id_structure` int(11) NOT NULL,
  `status` character default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `guest` ADD COLUMN `id_room` int(11) NULL;
ALTER TABLE `structure` CHANGE COLUMN `videolink` `videolink` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `structure` CHANGE COLUMN `notes` `notes` VARCHAR(10000) NULL DEFAULT NULL ;
ALTER TABLE `booker` ADD COLUMN `is_authorized` INT(1) NULL DEFAULT 1 AFTER `id_guest`;
INSERT INTO `facility` VALUES (1,'Wi-Fi','',1)
,(2,'Microwave','',1)
,(3,'Air conditioned','',2)
,(4,'Cable TV','',2)
,(5,'Satellite TV','',2)
,(6,'Fridge','',2)
,(7,'Dishwasher','',2)
,(8,'Washer','',2)
,(9,'Dryer','',2)
,(10,'Hairdryers','',2)
,(11,'Stereo','',2);

ALTER TABLE `structure` ADD COLUMN `number` VARCHAR(100) NULL;
ALTER TABLE `room` ADD COLUMN `code` VARCHAR(100) NOT NULL;
ALTER TABLE `booking` ADD COLUMN `bookingTime` DATETIME NULL AFTER `id_room`;
ALTER TABLE `structure` ADD COLUMN `reach_us` VARCHAR(5000) NULL AFTER `number`;
ALTER TABLE `room` ADD COLUMN `minRentalTime` INT(11) NULL DEFAULT 7 AFTER `code`;
ALTER TABLE `structure` ADD COLUMN `id_static_map` INT(11) NULL AFTER `reach_us`;
ALTER TABLE `structure` ADD COLUMN `cleaningFee` DOUBLE NULL DEFAULT 0 AFTER `id_static_map`;
ALTER TABLE `structure` ADD COLUMN `deposit` DOUBLE NULL DEFAULT 0 AFTER `cleaningFee`;
ALTER TABLE `structure` ADD COLUMN `personalPhone` VARCHAR(50) NULL DEFAULT 0 AFTER `deposit`;
ALTER TABLE `structure` ADD COLUMN `houseRules` VARCHAR(1000) NULL AFTER `personalPhone`;

DROP TABLE IF EXISTS `wizard`;
CREATE TABLE `wizard` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idPropertyType` INT(11) NOT NULL,
  `userId` INT(11) NOT NULL,
  `progress` INT(11) NOT NULL,
  PRIMARY KEY (`id`));

  DROP TABLE IF EXISTS `roomwz`;
  CREATE TABLE `roomwz` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idRoomType` INT(11) NOT NULL,
  `numbGuest` INT(11) NOT NULL,
  `idWz` INT(11) NOT NULL,
  PRIMARY KEY (`id`));

 INSERT INTO `roomtype` VALUES (1,'Single (1 single bed)',1,0,'')
 ,(2,'Double 1 (double bed for 1 person)',1,0,'')
 ,(3,'Double 2 (double bed for 2 people)',2,0,'')
 ,(4,'Twin (2 single beds)',2,0,'')
 ,(5,'Triple (3 single beds)',3,0,'')
 ,(6,'Shared4 (4 single beds)',4,0,'');

 ALTER TABLE `booking` ADD COLUMN `guid` VARCHAR(255) NULL AFTER `bookingTime`
 ,ADD COLUMN `token` VARCHAR(255) NULL AFTER `guid`
 ,ADD COLUMN `payerid` VARCHAR(255) NULL AFTER `token`;
 
 ALTER TABLE `wizard` ADD COLUMN `idStructure` INT(11) NULL AFTER `progress`;
 ALTER TABLE `roomwz` ADD COLUMN `idRoom` INT(11) NULL AFTER `idWz`;
 INSERT INTO `permission`(`name`,`description`) VALUES ('saveStructure','Save Structure');
 INSERT INTO `rolepermission`(`id_role`,`id_permission`) VALUES (1,(SELECT id from `permission` WHERE name = 'saveStructure'));
 INSERT INTO `rolepermission`(`id_role`,`id_permission`) VALUES (2,(SELECT id from `permission` WHERE name = 'saveStructure'));
 ALTER TABLE `room` ADD COLUMN `weeklyPrice` DOUBLE NULL AFTER `minRentalTime`;
 ALTER TABLE `wizard` ADD COLUMN `idSeason` INT(11) NULL AFTER `idStructure`;
 ALTER TABLE `user` CHANGE COLUMN `id_role` `id_role` INT(11) NULL DEFAULT '2';
 ALTER TABLE `room` CHANGE COLUMN `availableforbooking` `availableforbooking` CHAR(1) NOT NULL DEFAULT '0' , CHANGE COLUMN `minRentalTime` `minRentalTime` INT(11) NULL DEFAULT '14';
 ALTER TABLE `user` CHANGE COLUMN `idritmuser` `idritmuser` CHAR(36) NOT NULL;

DROP TABLE IF EXISTS `wzimage`;
CREATE TABLE `wzimage` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idWizard` INT NULL,
  `idImage` INT NULL,
  `status` TINYINT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `structure` CHANGE COLUMN `cleaningFee` `cleaningFee` DOUBLE NULL DEFAULT 0 , CHANGE COLUMN `deposit` `deposit` DOUBLE NULL DEFAULT 0 ;
ALTER TABLE `room` CHANGE COLUMN `notes` `notes` VARCHAR(2550) NULL DEFAULT NULL ;