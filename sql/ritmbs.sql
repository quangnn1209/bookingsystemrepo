DROP DATABASE IF EXISTS `ritmbs`;
CREATE DATABASE  IF NOT EXISTS `ritmbs`;
USE `ritmbs`;
--
-- Tables structure
--
DROP TABLE IF EXISTS `adjustment`;
CREATE TABLE `adjustment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `id_booking` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `booker`;
CREATE TABLE `booker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) DEFAULT NULL,
  `id_guest` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateIn` date DEFAULT NULL,
  `dateOut` date DEFAULT NULL,
  `nrGuests` int(11) DEFAULT NULL,
  `roomSubtotal` double DEFAULT NULL,
  `extraSubtotal` double DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `id_convention` int(11) DEFAULT NULL,
  `id_room` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `convention`;
CREATE TABLE `convention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `activationCode` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policeCode` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `expiryDate` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `creditCard`;
CREATE TABLE `creditCard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardType` varchar(255) DEFAULT NULL,
  `cardNumber` varchar(255) DEFAULT NULL,
  `expMonth` tinyint(4) DEFAULT NULL,
  `expYear` smallint(6) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `securityCode` varchar(255) DEFAULT NULL,
  `id_booking` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `extra`;
CREATE TABLE `extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `timePriceType` varchar(255) DEFAULT NULL,
  `resourcePriceType` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `availableOnline` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `extraItem`;
CREATE TABLE `extraItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) DEFAULT NULL,
  `id_extra` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `maxQuantity` int(11) DEFAULT NULL,
  `unitaryPrice` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `extraPriceList`;
CREATE TABLE `extraPriceList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_roomType` int(11) DEFAULT NULL,
  `id_season` int(11) DEFAULT NULL,
  `id_convention` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `extraPriceListItem`;
CREATE TABLE `extraPriceListItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `id_extra` int(11) DEFAULT NULL,
  `id_extraPriceList` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `facility`;
CREATE TABLE `facility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `facilityImage`;
CREATE TABLE `facilityImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_facility` int(11) DEFAULT NULL,
  `id_image` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `groupLeader`;
CREATE TABLE `groupLeader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) DEFAULT NULL,
  `id_housed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `guest`;
CREATE TABLE `guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `birthPlace` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `idNumber` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `id_countryOfBirth` int(11) DEFAULT NULL,
  `id_municipalityOfBirth` int(11) DEFAULT NULL,
  `id_countryOfResidence` int(11) DEFAULT NULL,
  `id_municipalityOfResidence` int(11) DEFAULT NULL,
  `id_citizenship` int(11) DEFAULT NULL,
  `id_idType` int(11) DEFAULT NULL,
  `id_idPlace` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `housed`;
CREATE TABLE `housed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) DEFAULT NULL,
  `id_guest` int(11) DEFAULT NULL,
  `id_housedType` int(11) DEFAULT NULL,
  `id_tourismType` int(11) DEFAULT NULL,
  `id_transport` int(11) DEFAULT NULL,
  `checkInDate` date DEFAULT NULL,
  `checkOutDate` date DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `touristTax` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `housedExport`;
CREATE TABLE `housedExport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_housed` int(11) DEFAULT NULL,
  `mode` int(11) DEFAULT NULL,
  `exported` tinyint(4) DEFAULT NULL,
  `exportedQuestura` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `housedType`;
CREATE TABLE `housedType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `identificationType`;
CREATE TABLE `identificationType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policeCode` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `imageFile`;
CREATE TABLE `imageFile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_image` int(11) DEFAULT NULL,
  `id_file` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `municipality`;
CREATE TABLE `municipality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policeCode` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `expiryDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `id_booking` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `period`;
CREATE TABLE `period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `id_season` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `id_roomType` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomFacility`;
CREATE TABLE `roomFacility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) DEFAULT NULL,
  `id_facility` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomImage`;
CREATE TABLE `roomImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_room` int(11) DEFAULT NULL,
  `id_image` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomPriceList`;
CREATE TABLE `roomPriceList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_roomType` int(11) DEFAULT NULL,
  `id_season` int(11) DEFAULT NULL,
  `id_convention` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomPriceListItem`;
CREATE TABLE `roomPriceListItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numGuests` int(11) DEFAULT NULL,
  `priceSunday` double DEFAULT NULL,
  `priceMonday` double DEFAULT NULL,
  `priceTuesday` double DEFAULT NULL,
  `priceWednesday` double DEFAULT NULL,
  `priceThursday` double DEFAULT NULL,
  `priceFriday` double DEFAULT NULL,
  `priceSaturday` double DEFAULT NULL,
  `id_roomPriceList` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomType`;
CREATE TABLE `roomType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `maxGuests` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomTypeFacility`;
CREATE TABLE `roomTypeFacility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_roomType` int(11) DEFAULT NULL,
  `id_facility` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `roomTypeImage`;
CREATE TABLE `roomTypeImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_roomType` int(11) DEFAULT NULL,
  `id_image` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `season`;
CREATE TABLE `season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `structure`;
CREATE TABLE `structure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `taxNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `structureFacility`;
CREATE TABLE `structureFacility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_structure` int(11) DEFAULT NULL,
  `id_facility` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `structureImage`;
CREATE TABLE `structureImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_structure` int(11) DEFAULT NULL,
  `id_image` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tourismType`;
CREATE TABLE `tourismType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `transport`;
CREATE TABLE `transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Initial data
--	
INSERT INTO `housedType`(`id`,`code`,`description`) VALUES 
(1,16,'OSPITE SINGOLO'),
(2,17,'CAPOFAMIGLIA'),
(3,18,'CAPOGRUPPO'),
(4,19,'FAMILIARE'),
(5,20,'MEMBRO GRUPPO');
																																																																																																																																																																																																																												
INSERT INTO `transport`(`id`,`name`) VALUES 
(1, 'transportCar'),
(2, 'transportAirplane'),
(3, 'transportTrain'),
(4, 'transportBus'),
(5, 'transportShip'),
(6, 'transportMotorcycle'),
(7, 'transportBike'),
(8, 'transportFlyDrive'),
(9, 'transportOther');   

INSERT INTO `tourismType`(`id`,`name`) VALUES 
(1, 'tourismTypeSeaside'),
(2, 'tourismTypeFoodAndWine'),
(3, 'tourismTypeCultural'),
(4, 'tourismTypeThemeParks'),
(5, 'tourismTypeSocial'),
(6, 'tourismTypeSports'),
(7, 'tourismTypeBusinessCongress'),
(8, 'tourismTypeFitness'),
(9, 'tourismTypeSchool'),
(10, 'tourismTypeReligious'),
(11, 'tourismTypeOther');

INSERT INTO `identificationType`(`id`,`policeCode`,`description`) VALUES
(1, 'IDENT', 'idTypeIDCard'),
(2, 'PASOR', 'idTypePassport'),
(3, 'PATEN', 'idTypeDrivingLicence');
																																																																																																																																																																																																										
INSERT INTO `country`(`id`, `policeCode`, `province`, `expiryDate`, `description`) VALUES																																																																																																																																																																																																																												
(1, '100000100', 'ES', NULL, 'IT');

INSERT INTO `municipality`(`id`, `policeCode`, `description`, `province`, `expiryDate`) VALUES
(223, '401001223', 'unknow', '--', NULL);
