package action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import model.Structure;
import model.internal.GeocodeResponse;
import model.internal.Location;
import model.internal.OpenTrustManager;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import service.StructureService;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "default")
public class GoogleMapAction extends ActionSupport {
	private static final String URL = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=";

	private String idStructure = null;
	private Structure structure;
	private Location local = null;
	private String keyAPI = null;
	@Autowired
	private StructureService structureService;

	@Actions({ @Action(value = "/goGoogleMap", results = { @Result(name = "success", location = "/WEB-INF/jsp/googlemap.jsp") }) })
	public String goPayPalPayment() throws IOException {
		Gson gson = new Gson();
		StringBuilder address = new StringBuilder();

		// Find structure by id
		if (this.getStructure().getId() != null) {
			Structure structure = this.getStructureService().findStructureById(this.getStructure().getId());
			// Get address and convert to lat longtitue
			address.append(structure.getAddress());
			address.append(",");
			address.append(structure.getCity());
			address.append(",");
			address.append(structure.getCountry());
		} else {
			// Get address and convert to lat longtitue
			address.append(this.getStructure().getAddress());
			address.append(",");
			address.append(this.getStructure().getCity());
			address.append(",");
			address.append(this.getStructure().getCountry());
		}
		// Open ssl connection
		Properties prop = new Properties();
		prop.load(GoogleMapAction.class.getResourceAsStream("/google.properties"));
		URL url = new URL(URL + address.toString().replaceAll(" ", "%20") + "&key=" + prop.getProperty("google.keyAPI"));
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
		try {
			OpenTrustManager.apply(con);
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		con.setHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
		});

		GeocodeResponse response = gson.fromJson(new BufferedReader(new InputStreamReader(con.getInputStream())), GeocodeResponse.class);
		this.setLocal(response.getResults().get(0).getGeometry().getLocation());
		this.setKeyAPI(prop.getProperty("google.keyAPI"));
		return SUCCESS;
	}

	public String getIdStructure() {
		return idStructure;
	}

	public void setIdStructure(String idStructure) {
		this.idStructure = idStructure;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public Location getLocal() {
		return local;
	}

	public void setLocal(Location local) {
		this.local = local;
	}

	public String getKeyAPI() {
		return keyAPI;
	}

	public void setKeyAPI(String keyAPI) {
		this.keyAPI = keyAPI;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

}
