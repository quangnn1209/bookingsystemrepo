/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "default")
@InterceptorRefs({ @InterceptorRef("userAwareStack") })
@Result(name = "accessDenied", location = "/WEB-INF/jsp/homeNotLogged.jsp")
public class ImageAction extends ActionSupport {

	@Actions(@Action(value = "/findAllImages", results = { @Result(name = "success", location = "/WEB-INF/jsp/images.jsp") }))
	public String findAllImages() {
		return SUCCESS;
	}
	
	@Actions(@Action(value = "/embedImageFunction", results = { @Result(name = "success", location = "/WEB-INF/jsp/embedImages.jsp") }))
	public String embedImageFunction() {
		return SUCCESS;
	}
}