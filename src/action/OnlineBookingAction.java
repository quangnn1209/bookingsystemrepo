/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package action;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Booking;
import model.Extra;
import model.Guest;
import model.Room;
import model.Structure;
import model.internal.Message;
import model.internal.OnlineBooking;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import service.BookerService;
import service.BookingService;
import service.ConventionService;
import service.ExtraService;
import service.GuestService;
import service.ImageService;
import service.RoomService;
import service.RoomTypeService;
//import service.SendMailService;
import service.StructureService;
import utils.CommonUtils;
import utils.RITMBSConstants;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "default")
public class OnlineBookingAction extends ActionSupport implements SessionAware {
	private Map<String, Object> session = null;

	private List<Room> rooms = null;
	private Booking booking = null;
	private Guest onlineGuest = null;
	private Integer id;
	private Integer numNights = 1;
	private List<Extra> extras;
	private List<Integer> bookingExtrasId = null;
	private Integer idStructure;
	private Structure structure;
	private Integer maxGuest;
	private Message message = new Message();
	private String command;
	private Double subTotal;
	private Double preCost;
	private String dateIn;
	private String dateOut;
	private String authorized = "false";
	private OnlineBooking ajaxObject = new OnlineBooking();
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String NUMBER_PATTERN = "^\\d+$";
	private static final String STRING_PATTERN = "^[a-zA-Z ,.-]*$";
	@Autowired
	private ExtraService extraService = null;
	@Autowired
	private GuestService guestService = null;
	@Autowired
	private StructureService structureService = null;
	@Autowired
	private BookingService bookingService = null;
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private ConventionService conventionService = null;
	@Autowired
	private ImageService imageService = null;
	@Autowired
	private RoomTypeService roomTypeService = null;
	@Autowired
	private BookerService bookerService = null;

	// @Autowired
	// private SendMailService sendMailService;

	@Actions({ @Action(value = "/mobile", results = { @Result(name = "success", location = "/WEB-INF/jsp/online/widget1.html") }),
			@Action(value = "/online", results = { @Result(name = "success", location = "/WEB-INF/jsp/online/widget1.html") }) })
	public String goOnlineBooking() {
		return SUCCESS;
	}

	private static Logger logger = Logger.getLogger(Logger.class);

	@Actions({ @Action(value = "/goOnlineBookingFinal", results = {
			@Result(name = "success", type = "redirectAction", params = { "actionName", "goPayPalPayment" }),
			@Result(name = "error", location = "/WEB-INF/jsp/online/validationError.jsp"),
			@Result(name = "input", location = "/WEB-INF/jsp/online/validationError.jsp") }) })
	public String goOnlineBookingFinal() throws IOException {
		Structure structure = (Structure) this.getSession().get("structure");

		if (structure == null) {
			structure = new Structure();
			structure.setId(-1);
		}

		Booking booking = (Booking) this.getSession().get(RITMBSConstants.ONLINE_BOOKING_SESSION);
		if (booking == null) {
			booking = new Booking();
		}

		Guest onlineGuest;
		// The first case is this method called after jump
		if (this.getOnlineGuest() == null || this.getOnlineGuest().getEmail() == null || this.getOnlineGuest().getEmail().isEmpty()) {
			// Select guest info from session
			onlineGuest = (Guest) this.getSession().get("onlineGuest");
			this.setOnlineGuest(onlineGuest);
		} else {
			// Search guest by email
			List<Guest> gList = this.getGuestService().findGuestsByEmail(this.getOnlineGuest().getEmail());

			// Only validate if registration new guest.
			if (gList == null || gList.isEmpty()) {

				// Validate required
				if (this.getOnlineGuest().getFirstName() == null || this.getOnlineGuest().getFirstName().isEmpty()) {
					this.getMessage().setDescription("First name is required!");
					return ERROR;
				}
				if (this.getOnlineGuest().getLastName() == null || this.getOnlineGuest().getLastName().isEmpty()) {
					this.getMessage().setDescription("Last name is required!");
					return ERROR;
				}
				if (this.getOnlineGuest().getPhone() == null || this.getOnlineGuest().getPhone().isEmpty()) {
					this.getMessage().setDescription("Phone is required!");
					return ERROR;
				}
				if (this.getOnlineGuest().getEmail() == null || this.getOnlineGuest().getEmail().isEmpty()) {
					this.getMessage().setDescription("Email is required!");
					return ERROR;
				}

				// Validate max length
				if (this.getOnlineGuest().getFirstName().length() > 60) {
					this.getMessage().setDescription("First name is limited at 60 characters!");
					return ERROR;
				}
				if (this.getOnlineGuest().getLastName().length() > 60) {
					this.getMessage().setDescription("Last name is limited at 60 characters!");
					return ERROR;
				}
				if (this.getOnlineGuest().getPhone().length() > 35) {
					this.getMessage().setDescription("Phone is limited at 35 characters!");
					return ERROR;
				}
				if (this.getOnlineGuest().getEmail().length() > 55) {
					this.getMessage().setDescription("Email is limited at 55 characters!");
					return ERROR;
				}

				// Validate formal
				// Validate email pattern
				Pattern pattern = Pattern.compile(STRING_PATTERN);
				Matcher matcher = pattern.matcher(this.getOnlineGuest().getFirstName());
				if (!matcher.matches()) {
					this.getMessage().setDescription("Please input only alphabet into First name field!");
					return ERROR;
				}

				matcher = pattern.matcher(this.getOnlineGuest().getLastName());
				if (!matcher.matches()) {
					this.getMessage().setDescription("Please input only alphabet into last name field!");
					return ERROR;
				}

				pattern = Pattern.compile(NUMBER_PATTERN);
				matcher = pattern.matcher(this.getOnlineGuest().getPhone());
				if (!matcher.matches()) {
					this.getMessage().setDescription("Please input only number in phone field!");
					return ERROR;
				}

				pattern = Pattern.compile(EMAIL_PATTERN);
				matcher = pattern.matcher(this.getOnlineGuest().getEmail());
				if (!matcher.matches()) {
					this.getMessage().setDescription("Email's format invalid!");
					return ERROR;
				}
			}

		}

		this.getOnlineGuest().setId_structure(structure.getId());
		this.getOnlineGuest().setId_room(booking.getRoom() == null ? null : booking.getRoom().getId());
		// Search email in database
		List<Guest> gList = this.getGuestService().findGuestsByEmail(this.getOnlineGuest().getEmail());

		// If exist
		Integer gId = 0;
		if (!CommonUtils.isEmpty(gList)) {
			// Update
			this.getOnlineGuest().setId(gList.get(0).getId());
			gId = this.getOnlineGuest().getId();
			this.getGuestService().updateGuest(this.getOnlineGuest());
		} else {
			// Insert
			if (this.getGuestService().insertGuest(this.getOnlineGuest()) != 0) {
				gId = this.getOnlineGuest().getId();
			}
		}

		booking.setBooker(this.getOnlineGuest());

		// Calculate price
		booking.getRoom().setPrice(this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), booking));
		// Plus with service cost
		booking.getRoom().setPrice(booking.getRoom().getPrice() * RITMBSConstants.SERVICE_COST);
		booking.setRoomSubtotal(booking.getRoom().getPrice());

		// Save booking with status = pending payment
		booking.setStatus("pending");
		Integer result = 0;
		result = this.getBookingService().saveOnlineBooking(booking);
		if (result == 0) {
			this.getSession().put(RITMBSConstants.ONLINE_BOOKING_SESSION, null);
			addActionError("This booking cannot be saved");
			return ERROR;
		} else {
			// Insert booker
			this.getBookerService().insert(gId, booking.getId());
		}

		// Send mail temporary disabled
		/*
		 * try { sendMailService.sendMailGuest(booking);
		 * sendMailService.sendMailOwner(booking); } catch (Exception e) {
		 * logger.error("error sending mail " + e.getMessage()); }
		 */
		this.setBooking(booking);
		this.getSession().put(RITMBSConstants.ONLINE_BOOKING_SESSION, booking);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/goOnlineBookingResult", results = { @Result(name = "success", location = "/WEB-INF/jsp/online/widget5.html"),
			@Result(name = "input", location = "/WEB-INF/jsp/online/validationError.jsp") }) })
	public String goOnlineBookingResult() {
		Structure structure = this.getIdStructure() == null ? (Structure) this.getSession().get("structure") : this.getStructureService().findStructureById(
				this.getIdStructure());

		if (structure == null) {
			structure = new Structure();
			structure.setId(this.getIdStructure() == null ? -1 : this.getIdStructure());
		}

		Booking bookingObj = (Booking) this.getSession().get(RITMBSConstants.ONLINE_BOOKING_SESSION);
		if (bookingObj != null) {
			this.setBooking(bookingObj);
		}

		// Update booking after payment success
		bookingObj.setStatus("online");
		if (this.getBookingService().updateOnlineBooking(bookingObj, bookingObj.getBooker()) == 0) {
			this.getSession().put(RITMBSConstants.ONLINE_BOOKING_SESSION, null);
			addActionError("This booking cannot be updated");
			return ERROR;
		}

		this.getAjaxObject().setFirstName(bookingObj.getBooker().getFirstName());
		this.getAjaxObject().setLastName(bookingObj.getBooker().getLastName());
		this.getAjaxObject().setDateIn(CommonUtils.formatDate(bookingObj.getDateIn()));
		this.getAjaxObject().setNumNights(bookingObj.calculateNumNights());
		this.getAjaxObject().setNrGuest(bookingObj.getNrGuests());
		this.getAjaxObject().setBookingPrice(bookingObj.getRoom().getPrice());
		this.getAjaxObject().setStructureName(structure.getName());
		this.getAjaxObject().setStructureEmail(structure.getEmail());
		this.getAjaxObject().setStructurePhone(structure.getPhone());
		this.getSession().put("bookingResultObj", this.getAjaxObject());
		return SUCCESS;
	}

	@Actions({ @Action(value = "/goBookingResultAjax", results = { @Result(type = "json", name = "success", params = { "root", "ajaxObject" }) }) })
	public String goBookingResultAjax() {
		this.setAjaxObject((OnlineBooking) this.getSession().get("bookingResultObj"));
		return SUCCESS;
	}

	public List<Extra> getExtras() {
		return extras;
	}

	public void setExtras(List<Extra> extras) {
		this.extras = extras;
	}

	public Integer getNumNights() {
		return numNights;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNumNights(Integer numNight) {
		this.numNights = numNight;
	}

	public List<Integer> getBookingExtrasId() {
		return bookingExtrasId;
	}

	public void setBookingExtrasId(List<Integer> bookingExtrasId) {
		this.bookingExtrasId = bookingExtrasId;
	}

	public Integer getIdStructure() {
		return idStructure;
	}

	public void setIdStructure(Integer idStructure) {
		this.idStructure = idStructure;
	}

	public Integer getMaxGuest() {
		return maxGuest;
	}

	public void setMaxGuest(Integer maxGuest) {
		this.maxGuest = maxGuest;
	}

	public ExtraService getExtraService() {
		return extraService;
	}

	public void setExtraService(ExtraService extraService) {
		this.extraService = extraService;
	}

	public GuestService getGuestService() {
		return guestService;
	}

	public void setGuestService(GuestService guestService) {
		this.guestService = guestService;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public BookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public ConventionService getConventionService() {
		return conventionService;
	}

	public void setConventionService(ConventionService conventionService) {
		this.conventionService = conventionService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public Guest getOnlineGuest() {
		return onlineGuest;
	}

	public void setOnlineGuest(Guest onlineGuest) {
		this.onlineGuest = onlineGuest;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public RoomTypeService getRoomTypeService() {
		return roomTypeService;
	}

	public void setRoomTypeService(RoomTypeService roomTypeService) {
		this.roomTypeService = roomTypeService;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getPreCost() {
		return preCost;
	}

	public void setPreCost(Double preCost) {
		this.preCost = preCost;
	}

	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	public BookerService getBookerService() {
		return bookerService;
	}

	public void setBookerService(BookerService bookerService) {
		this.bookerService = bookerService;
	}

	public String getAuthorized() {
		return authorized;
	}

	public void setAuthorized(String authorized) {
		this.authorized = authorized;
	}

	public OnlineBooking getAjaxObject() {
		return ajaxObject;
	}

	public void setAjaxObject(OnlineBooking ajaxObject) {
		this.ajaxObject = ajaxObject;
	}
}