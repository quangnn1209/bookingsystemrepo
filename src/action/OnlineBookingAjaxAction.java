package action;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Booker;
import model.Booking;
import model.Guest;
import model.Room;
import model.RoomType;
import model.Structure;
import model.internal.OnlineBooking;
import model.listini.Convention;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import service.BookerService;
import service.BookingService;
import service.ConventionService;
import service.GuestService;
import service.RoomService;
import service.RoomTypeService;
import service.StructureService;
import utils.CommonUtils;
import utils.I18nUtils;
import utils.RITMBSConstants;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class OnlineBookingAjaxAction extends HttpServlet {
	private static final long serialVersionUID = 6485924791898779907L;
	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";
	public static final String JUMP = "JUMP";
	public static final String BOOKED = "BOOKED";
	public static final String NOTPUBLISHED = "NOTPUBLISHED";
	public static final String STRUCTURENOROOM = "STRUCTURENOROOM";
	public static final String WRONGDATEINPUT = "WRONGDATEINPUT";
	public static final String SEASONERROR = "SEASONERROR";
	public static final String GUESTERROR = "GUESTERROR";
	public static final String UPDATEERROR = "UPDATEERROR";
	public static final String STRUCTURENOROOMTYPE = "STRUCTURENOROOMTYPE";
	public static final String ROOMERROR = "ROOMERROR";
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Autowired
	private StructureService structureService = null;
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private RoomTypeService roomTypeService = null;
	@Autowired
	private BookingService bookingService = null;
	@Autowired
	private ConventionService conventionService = null;
	@Autowired
	private BookerService bookerService = null;
	@Autowired
	private GuestService guestService = null;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Properties prop = new Properties();
		prop.load(this.getClass().getResourceAsStream("/sdk_config.properties"));
		// Get param from request
		String callback = request.getParameter("callback");
		String calledMethod = request.getParameter("calledMethod");

		// Define printer
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		response.setHeader("Cache-control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "-1");

		// Define json object
		Gson gson = new Gson();
		JsonObject jsonObj = new JsonObject();
		JsonElement obObj = null;
		OnlineBooking ob = new OnlineBooking();
		// Call method base on request
		if ("calculateTotalCost".equals(calledMethod)) {
			try {
				ob = this.calculateCost(request);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if ("goOnlineBookingRooms".equals(calledMethod)) {
			try {
				ob = this.goOnlineBookingRooms(request);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else if ("goOnlineBookingResult".equals(calledMethod)) {
			ob = this.onlineBookingResult(request);
		} else if ("hasRoomFreeInPeriodInitial".equals(calledMethod)) {
			try {
				ob = this.hasRoomFreeInPeriodInitial(request);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			// Default will be initial case
			ob = this.initialWidget(request);
		}

		// Set concurrency unit
		ob.setUnitSymbol(RITMBSConstants.CONCURRENCY_MAP.get(prop.getProperty("concurrency.unit")) == null ? RITMBSConstants.CONCURRENCY_MAP
				.get(RITMBSConstants.CONCURRENCY_UNIT) : RITMBSConstants.CONCURRENCY_MAP.get(prop.getProperty("concurrency.unit")));
		obObj = gson.toJsonTree(ob);

		// Generate json string
		jsonObj.add("onlinebooking", obObj);
		if (callback != null) {
			out.println(callback + "(" + jsonObj.toString() + ");");
		} else {
			out.println(jsonObj.toString());
		}

		out.close();
	}

	public OnlineBooking hasRoomFreeInPeriodInitial(HttpServletRequest request) throws ParseException {
		OnlineBooking ob = new OnlineBooking();
		Integer idRoom = CommonUtils.parseInt(request.getParameter("idRoom"), -1);
		String dateIn = request.getParameter("dateIn") == null ? CommonUtils.formatDate(RITMBSConstants.DEFAULT_BOOKING_DATE) : request.getParameter("dateIn");
		String dateOut = request.getParameter("dateOut") == null ? CommonUtils.formatDate(RITMBSConstants.DEFAULT_BOOKING_DATE) : request
				.getParameter("dateOut");
		if (this.getStructureService().hasRoomFreeInPeriodInitial(idRoom, CommonUtils.convertStringToDate(dateIn), CommonUtils.convertStringToDate(dateOut))) {
			Booking booking = new Booking();
			booking.setDateIn(dateIn);
			booking.setDateOut(dateOut);
			Date endSeason = this.getBookingService().findTheLastDayOfSeason(this.getRoomService().findIdStructureByIdRoom(idRoom), booking);
			if (endSeason != null) {
				ob.setHasRoomFreeInPeriod("0");
			} else {
				ob.setHasRoomFreeInPeriod("1");
			}
		} else {
			ob.setHasRoomFreeInPeriod("0");
		}
		return ob;
	}

	public OnlineBooking goOnlineBookingRooms(HttpServletRequest request) throws ParseException {
		OnlineBooking ob = new OnlineBooking();
		String gEmail = request.getParameter("gEmail");
		String dateIn = request.getParameter("dateIn") == null ? CommonUtils.formatDate(RITMBSConstants.DEFAULT_BOOKING_DATE) : request.getParameter("dateIn");
		String dateOut = request.getParameter("dateOut") == null ? "" : request.getParameter("dateOut");

		Integer nrGuest = request.getParameter("nrGuest") == null ? RITMBSConstants.DEFAULT_NUMBER_OF_GUEST : CommonUtils.parseInt(
				request.getParameter("nrGuest"), -1);

		Convention defaultConvention = null;
		Structure structure = (Structure) request.getSession().getAttribute("structure");

		if (structure == null) {
			// Set to view
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(NOTPUBLISHED);
			ob.setMessage(I18nUtils.getProperty("currentStructureNotPublished"));
			return ob;
		}

		Booking bookingObj = (Booking) request.getSession().getAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION);
		if (bookingObj == null) {
			// Set to view
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(ERROR);
			ob.setMessage("Please do not try to hack my app");
			return ob;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(RITMBSConstants.DEFAULT_DATE_PATTERN);
		// Set booking
		bookingObj.setDateIn(dateIn);
		if (dateOut != null) {
			if (sdf.parse(dateOut).after(bookingObj.getDateIn())) {
				bookingObj.setDateOut(sdf.parse(dateOut));
			} else {
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(WRONGDATEINPUT);
				ob.setMessage(I18nUtils.getProperty("dateinMustBeforeDateout"));
				return ob;
			}
		}
		bookingObj.setNrGuests(nrGuest);

		// Search convention
		for (Convention each : this.getConventionService().findConventionsByIdStructure(structure.getId())) {
			if (each.getActivationCode().equals(RITMBSConstants.ACTIVE_CODE)) {
				defaultConvention = each;
				break;
			}
		}
		bookingObj.setConvention(defaultConvention);
		assert defaultConvention != null;
		bookingObj.setId_convention(defaultConvention.getId());

		// Validate available of room.
		if (bookingObj.getRoom().getAvailableforbooking().equals(RITMBSConstants.AVAILABLEFORBOOKING)) {
			// Validate min rental time
			// Re-find room to update min rental time
			Room latestRoom = this.getRoomService().findRoomById(bookingObj.getRoom().getId());
			if (latestRoom.getMinRentalTime() <= (int) ((bookingObj.getDateOut().getTime() - bookingObj.getDateIn().getTime()) / (1000 * 60 * 60 * 24))) {
				if (this.getStructureService().hasRoomFreeInPeriod(structure.getId(), bookingObj.getRoom().getId(), bookingObj.getDateIn(),
						bookingObj.getDateOut())) {
					// Validate out of season
					Date endSeason = this.getBookingService().findTheLastDayOfSeason(structure.getId(), bookingObj);
					if (endSeason != null) {
						if (bookingObj.getDateIn().before(endSeason)) {
							ob.setMessage("The season is available from " + dateIn + " - " + endSeason.toString() + ". Please select another term.");
						} else {
							ob.setMessage(I18nUtils.getProperty("seasonNotAvailable"));
						}
						ob.setStatusCodeDetail(SEASONERROR);
						ob.setStatusCode(ERROR);
						return ob;
					} else {
						// Validate number of guest
						Date invalidDate = this.getBookingService().validateNumberOfGuest(structure.getId(), bookingObj);
						if (invalidDate != null) {
							ob.setStatusCode(ERROR);
							ob.setStatusCodeDetail(GUESTERROR);
							ob.setMessage("Number of guest is over with maximum at " + invalidDate.toString());
							return ob;
						}

						// Validate season
						Date notExistDate = this.getBookingService().validateSeasonConfigured(structure.getId(), bookingObj);
						if (notExistDate != null) {
							ob.setStatusCode(ERROR);
							ob.setStatusCodeDetail(SEASONERROR);
							ob.setMessage(I18nUtils.getProperty("periodSeasonErrorAtDate") + " " + CommonUtils.formatDate(notExistDate));
							return ob;
						}
						// Calculate price
						Double price = this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), bookingObj);
						if (price == null) {
							ob.setStatusCode(ERROR);
							ob.setStatusCodeDetail(SEASONERROR);
							ob.setMessage(I18nUtils.getProperty("periodSeasonError"));
							return ob;
						}

						// Plus with service cost
						bookingObj.getRoom().setPrice(Double.parseDouble(CommonUtils.formatConcurrency(price * RITMBSConstants.SERVICE_COST)));
					}
				} else {
					ob.setMessage(I18nUtils.getProperty("haveNoRoomAvailable") + " " + bookingObj.getDateIn().toString() + " - "
							+ bookingObj.getDateOut().toString());
					ob.setStatusCode(ERROR);
					ob.setStatusCodeDetail(BOOKED);
					return ob;
				}
			} else {
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(ROOMERROR);
				ob.setMessage(I18nUtils.getProperty("minRentalTimeMsg") + latestRoom.getMinRentalTime() + " days");
				return ob;
			}
		} else {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(STRUCTURENOROOM);
			ob.setMessage(I18nUtils.getProperty("structureHasNoRoomAvailable"));
			return ob;
		}

		// Set to session
		request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
		if (gEmail != null && !gEmail.isEmpty()) {
			// Validate email pattern
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(gEmail);

			if (matcher.matches()) {
				// Search email in table guest.
				List<Guest> gList = this.getGuestService().findGuestsByEmail(gEmail);
				if (!CommonUtils.isEmpty(gList)) {
					request.getSession().setAttribute("onlineGuest", gList.get(0));
					ob.setStatusCodeDetail(JUMP);
					ob.setStatusCode(JUMP);
					return ob;
				}
			}
		}

		ob.setDateIn(CommonUtils.formatDate(bookingObj.getDateIn()));
		ob.setNumNights(bookingObj.calculateNumNights());
		ob.setNrGuest(bookingObj.getNrGuests());
		ob.setBookingPrice(bookingObj.getRoom().getPrice());

		// return message
		ob.setStatusCode(SUCCESS);
		ob.setStatusCodeDetail(SUCCESS);
		return ob;
	}

	private OnlineBooking calculateCost(HttpServletRequest request) throws ParseException {
		OnlineBooking ob = new OnlineBooking();
		Integer idRoom = CommonUtils.parseInt(request.getParameter("idRoom"), -1);
		String dateIn = request.getParameter("dateIn") == null ? CommonUtils.formatDate(RITMBSConstants.DEFAULT_BOOKING_DATE) : request.getParameter("dateIn");
		String dateOut = request.getParameter("dateOut") == null ? "" : request.getParameter("dateOut");

		Integer nrGuest = request.getParameter("nrGuest") == null ? RITMBSConstants.DEFAULT_NUMBER_OF_GUEST : CommonUtils.parseInt(
				request.getParameter("nrGuest"), -1);
		String command = request.getParameter("command") == null ? "" : request.getParameter("command");
		Integer numNights = request.getParameter("numNights") == null ? 1 : CommonUtils.parseInt(request.getParameter("numNights"), 0);

		Structure structure = (Structure) request.getSession().getAttribute("structure");

		if (structure == null) {
			// Set to view
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(NOTPUBLISHED);
			ob.setMessage(I18nUtils.getProperty("currentStructureNotPublished"));
			return ob;
		}

		Booking bookingObj = (Booking) request.getSession().getAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION);
		if (bookingObj == null) {
			// Set to view
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(ERROR);
			ob.setMessage("Please do not try to hack my app");
			return ob;
		}
		bookingObj.setId_room(idRoom);

		boolean isSelectBox = false;
		// Priority of num night higher
		if ("selectbox".equalsIgnoreCase(command)) {
			isSelectBox = true;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(RITMBSConstants.DEFAULT_DATE_PATTERN);
		// Set booking
		bookingObj.setDateIn(dateIn);
		if (!isSelectBox && dateOut != null) {
			if (sdf.parse(dateOut).after(bookingObj.getDateIn())) {
				bookingObj.setDateOut(dateOut);
			} else {
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(WRONGDATEINPUT);
				ob.setMessage(I18nUtils.getProperty("dateinMustBeforeDateout"));
				return ob;
			}
		}

		bookingObj.setNrGuests(nrGuest);

		// Priority of num night higher
		if (isSelectBox) {
			bookingObj.setDateOut(DateUtils.addDays(bookingObj.getDateIn(), numNights));
			bookingObj.setNrGuests(1);
		}

		Convention defaultConvention = null;
		if (bookingObj.getConvention() == null) {
			// Find convention by structure id
			for (Convention each : this.getConventionService().findConventionsByIdStructure(structure.getId())) {
				if (each.getActivationCode().equals(RITMBSConstants.ACTIVE_CODE)) {
					defaultConvention = each;
					break;
				}
			}
			bookingObj.setConvention(defaultConvention);
			assert defaultConvention != null;
			bookingObj.setId_convention(defaultConvention.getId());
		}

		// Find room in case get paramter from URL
		bookingObj.setRoom(this.getRoomService().findRoomById(bookingObj.getId_room()));

		if (bookingObj.getRoom() != null && bookingObj.getRoom().getAvailableforbooking().equals(RITMBSConstants.AVAILABLEFORBOOKING)) {
			// Validate available of room.
			if (this.getStructureService().hasRoomFreeInPeriodInitial(bookingObj.getRoom().getId(), bookingObj.getDateIn(), bookingObj.getDateOut())) {
				// Validate out of season
				Date endSeason = this.getBookingService().findTheLastDayOfSeason(structure.getId(), bookingObj);
				if (endSeason != null) {

					if (bookingObj.getDateIn().before(endSeason)) {
						ob.setMessage("The season is available from " + dateIn + " - " + endSeason.toString() + ". Please select another term.");
					} else {
						ob.setMessage(I18nUtils.getProperty("seasonNotAvailable"));
					}
					ob.setStatusCodeDetail(SEASONERROR);
					ob.setStatusCode(ERROR);
					return ob;
				} else {
					// Validate number of guest
					Date invalidDate = this.getBookingService().validateNumberOfGuest(structure.getId(), bookingObj);
					if (invalidDate != null) {
						ob.setStatusCode(ERROR);
						ob.setStatusCodeDetail(GUESTERROR);
						ob.setMessage("Number of guest is over with maximum at " + invalidDate.toString());
						return ob;
					}

					// Set room type
					RoomType roomType = this.getRoomTypeService().findRoomTypeById(bookingObj.getRoom().getId_roomType());
					if (CommonUtils.isEmpty(roomType)) {
						// Keep the booking info
						request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
						request.getSession().setAttribute("structure", structure);

						// Set to view
						ob.setStatusCode(ERROR);
						ob.setStatusCodeDetail(STRUCTURENOROOMTYPE);
						ob.setMessage(I18nUtils.getProperty("currentStructureHasNoRoomType"));
						return ob;
					}

					bookingObj.getRoom().setRoomType(roomType);

					// Validate season
					Date notExistDate = this.getBookingService().validateSeasonConfigured(structure.getId(), bookingObj);
					if (notExistDate != null) {
						ob.setStatusCode(ERROR);
						ob.setStatusCodeDetail(SEASONERROR);
						ob.setMessage(I18nUtils.getProperty("periodSeasonErrorAtDate") + " " + CommonUtils.formatDate(notExistDate));
						return ob;
					}

					// Calculate price
					if (isSelectBox) {
						Double price = null;

						if (numNights >= 30) {
							price = this.getBookingService().calculateRoomSubtotalForBookingByMonth(structure.getId(), bookingObj);
						} else {
							price = this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), bookingObj);
						}

						if (price == null) {
							ob.setStatusCode(ERROR);
							ob.setStatusCodeDetail(SEASONERROR);
							ob.setMessage(I18nUtils.getProperty("periodSeasonError"));
							return ob;
						}
						ob.setPreCost(Double.parseDouble(CommonUtils.formatConcurrency(price)));
						ob.setCleaning(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee())));
						ob.setDeposit(Double.parseDouble(CommonUtils.formatConcurrency(structure.getDeposit())));
						ob.setSubTotalOffline(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee() + structure.getDeposit())));
						ob.setTotal(ob.getCleaning() + ob.getDeposit() + ob.getPreCost());
					} else {
						Double price = this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), bookingObj);
						if (price == null) {
							ob.setStatusCode(ERROR);
							ob.setStatusCodeDetail(SEASONERROR);
							ob.setMessage(I18nUtils.getProperty("periodSeasonError"));
							return ob;
						}
						bookingObj.getRoom().setPrice(price);
						ob.setSubTotal(Double.parseDouble(CommonUtils.formatConcurrency(bookingObj.getRoom().getPrice() * RITMBSConstants.SERVICE_COST)));
						ob.setRoomPrice(Double.parseDouble(CommonUtils.formatConcurrency(bookingObj.getRoom().getPrice())));
						ob.setServiceCost(Double.parseDouble(CommonUtils
								.formatConcurrency(bookingObj.getRoom().getPrice() * (RITMBSConstants.SERVICE_COST - 1))));
						ob.setCleaning(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee())));
						ob.setDeposit(Double.parseDouble(CommonUtils.formatConcurrency(structure.getDeposit())));
						ob.setSubTotalOffline(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee() + structure.getDeposit())));
						ob.setTotal(ob.getCleaning() + ob.getDeposit() + ob.getSubTotal());
					}
				}
			} else {
				ob.setMessage(I18nUtils.getProperty("haveNoRoomAvailable") + " " + bookingObj.getDateIn().toString() + " - "
						+ bookingObj.getDateOut().toString());
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(BOOKED);
				return ob;
			}
		} else {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(STRUCTURENOROOM);
			ob.setMessage(I18nUtils.getProperty("structureHasNoRoomAvailable"));
			return ob;
		}

		// update session
		request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);

		// return message
		ob.setStatusCode(SUCCESS);
		if (isSelectBox) {
			ob.setMessage("preCost:" + CommonUtils.formatConcurrency(ob.getPreCost()));
		} else {
			ob.setMessage("subTotal:" + CommonUtils.formatConcurrency(ob.getSubTotal()));
		}
		ob.setStatusCodeDetail(SUCCESS);
		return ob;
	}

	private OnlineBooking initialWidget(HttpServletRequest request) throws IOException {
		OnlineBooking ob = new OnlineBooking();
		// Integer idStructure =
		// CommonUtils.parseInt(request.getParameter("idStructure"), -1);
		Integer idRoom = CommonUtils.parseInt(request.getParameter("idRoom"), -1);
		String dateIn = request.getParameter("dateIn") == null ? CommonUtils.formatDate(RITMBSConstants.DEFAULT_BOOKING_DATE) : request.getParameter("dateIn");
		String dateOut = request.getParameter("dateOut") == null ? "" : request.getParameter("dateOut");

		Integer nrGuest = request.getParameter("nrGuest") == null ? RITMBSConstants.DEFAULT_NUMBER_OF_GUEST : CommonUtils.parseInt(
				request.getParameter("nrGuest"), -1);
		String gEmail = request.getParameter("gEmail");
		Structure structure = null;
		// Initial booking
		Booking bookingObj = new Booking();

		// Set value for booking
		bookingObj.setStatus("online");
		bookingObj.setId_room(idRoom);
		bookingObj.setDateIn(dateIn);

		if (dateOut == "") {
			bookingObj.setDateOut(DateUtils.addDays(bookingObj.getDateIn(), RITMBSConstants.DEFAULT_BOOKING_TERM));
		} else {
			bookingObj.setDateOut(dateOut);
		}

		if (bookingObj.getDateIn().before(RITMBSConstants.DEFAULT_BOOKING_DATE)) {
			bookingObj.setDateIn(RITMBSConstants.DEFAULT_BOOKING_DATE);
		}

		if (bookingObj.getDateOut().before(bookingObj.getDateIn())) {
			bookingObj.setDateOut(DateUtils.addDays(bookingObj.getDateIn(), RITMBSConstants.DEFAULT_BOOKING_TERM));
		}

		bookingObj.setNrGuests(nrGuest);

		if (bookingObj.getId_room() == null) {
			// Set to view
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(NOTPUBLISHED);
			ob.setMessage(I18nUtils.getProperty("currentStructureHasNoRoomType"));
			return ob;
		} else {
			// Find room by id
			Room room = this.getRoomService().findRoomById(idRoom);
			if (room == null || room.getAvailableforbooking().equals(RITMBSConstants.NOTAVAILABLEFORBOOKING)) {

				// Set to view
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(STRUCTURENOROOM);
				ob.setMessage(I18nUtils.getProperty("structureHasNoRoomAvailable"));
				return ob;
			}

			// Find structure by id
			// Initial structure
			structure = this.getStructureService().findStructureById(room.getId_structure());
			if (structure == null) {
				// Set to view
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(NOTPUBLISHED);
				ob.setMessage(I18nUtils.getProperty("currentStructureNotPublished"));
				return ob;
			}
			bookingObj.setId_structure(structure.getId());

			// Validate available of room.
			if (this.getStructureService().hasRoomFreeInPeriodInitial(bookingObj.getId_room(), bookingObj.getDateIn(), bookingObj.getDateOut())) {
				// Validate out of season
				Date endSeason = this.getBookingService().findTheLastDayOfSeason(structure.getId(), bookingObj);
				if (endSeason != null) {
					if (bookingObj.getDateIn().before(endSeason)) {
						ob.setMessage("The season is available from " + dateIn + " - " + endSeason.toString() + ". Please select another term.");
					} else {
						ob.setMessage(I18nUtils.getProperty("seasonNotAvailable"));
					}
					ob.setStatusCodeDetail(SEASONERROR);
					ob.setStatusCode(ERROR);

					// Keep the booking info
					request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
					request.getSession().setAttribute("structure", structure);
					return ob;
				} else {
					// get room type
					RoomType roomType = this.getRoomTypeService().findRoomTypeById(room.getId_roomType());
					if (CommonUtils.isEmpty(roomType)) {
						// Keep the booking info
						request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
						request.getSession().setAttribute("structure", structure);

						// Set to view
						ob.setStatusCode(ERROR);
						ob.setStatusCodeDetail(STRUCTURENOROOMTYPE);
						ob.setMessage(I18nUtils.getProperty("currentStructureHasNoRoomType"));
						return ob;
					} else {
						room.setRoomType(roomType);
					}

					bookingObj.setRoom(room);
				}
			} else {
				// Keep the booking info
				request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
				request.getSession().setAttribute("structure", structure);
				ob.setMessage(I18nUtils.getProperty("haveNoRoomAvailable") + " " + dateIn + " - " + dateOut);
				ob.setStatusCode(ERROR);
				ob.setStatusCodeDetail(BOOKED);
				return ob;
			}
		}
		// Search convention
		Convention defaultConvention = null;
		for (Convention each : this.getConventionService().findConventionsByIdStructure(structure.getId())) {
			if (each.getActivationCode().equals(RITMBSConstants.ACTIVE_CODE)) {
				defaultConvention = each;
				break;
			}
		}
		bookingObj.setConvention(defaultConvention);
		assert defaultConvention != null;
		bookingObj.setId_convention(defaultConvention.getId());

		// Validate number of guest
		Date invalidDate = this.getBookingService().validateNumberOfGuest(structure.getId(), bookingObj);
		if (invalidDate != null) {
			// Keep the booking info
			request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
			request.getSession().setAttribute("structure", structure);
			ob.setStatusCodeDetail(GUESTERROR);
			ob.setStatusCode(ERROR);
			ob.setMessage("Number of guest is over with maximum at " + invalidDate.toString());
			return ob;
		}
		// Validate season
		Date notExistDate = this.getBookingService().validateSeasonConfigured(structure.getId(), bookingObj);
		if (notExistDate != null) {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(SEASONERROR);
			ob.setMessage(I18nUtils.getProperty("periodSeasonErrorAtDate") + " " + CommonUtils.formatDate(notExistDate));
			return ob;
		}

		// Calculate price for sub total
		Double price = this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), bookingObj);
		if (price == null) {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(SEASONERROR);
			ob.setMessage(I18nUtils.getProperty("periodSeasonError"));
			return ob;
		}
		bookingObj.getRoom().setPrice(price);
		ob.setSubTotal(Double.parseDouble(CommonUtils.formatConcurrency(bookingObj.getRoom().getPrice() * RITMBSConstants.SERVICE_COST)));
		ob.setRoomPrice(Double.parseDouble(CommonUtils.formatConcurrency(bookingObj.getRoom().getPrice())));
		ob.setServiceCost(Double.parseDouble(CommonUtils.formatConcurrency(bookingObj.getRoom().getPrice() * (RITMBSConstants.SERVICE_COST - 1))));

		// Calculate price for pre cost
		Date oldDateOut = bookingObj.getDateOut();
		bookingObj.setDateOut(DateUtils.addDays(bookingObj.getDateIn(), RITMBSConstants.DEFAULT_BOOKING_TERM));
		// Validate season
		Date newNotExistDate = this.getBookingService().validateSeasonConfigured(structure.getId(), bookingObj);
		if (newNotExistDate != null) {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(SEASONERROR);
			ob.setMessage(I18nUtils.getProperty("periodSeasonErrorAtDate") + " " + CommonUtils.formatDate(newNotExistDate));
			return ob;
		}
		Double newPrice = this.getBookingService().calculateRoomSubtotalForBooking(structure.getId(), bookingObj);
		if (newPrice == null) {
			ob.setStatusCode(ERROR);
			ob.setStatusCodeDetail(SEASONERROR);
			ob.setMessage(I18nUtils.getProperty("periodSeasonError"));
			return ob;
		}
		ob.setPreCost(Double.parseDouble(CommonUtils.formatConcurrency(newPrice)));
		bookingObj.setDateOut(oldDateOut);

		// Set max guest for select box
		ob.setMaxGuest(bookingObj.getRoom().getRoomType().getMaxGuests());
		if (gEmail != null) {
			Guest g = new Guest();
			g.setEmail(gEmail);
			request.getSession().setAttribute("onlineGuest", g);
			ob.setEmailGuest(gEmail);
		}

		// Process to show "booking" or "ask the owner"
		ob.setAuthorized("false");
		if (RITMBSConstants.HAVEROOMAVAILABLE.equals(structure.getHaveroomavailable())) {
			ob.setAuthorized("true");
		} else {

			// Find the booking
			Booking bookingDB = this.getBookingService().findByTermByIdRoom(bookingObj.getDateIn(), bookingObj.getDateOut(), bookingObj.getRoom().getId());
			if (bookingDB != null) {
				bookingDB.getStatus();
				// Find booker
				Booker booker = this.getBookerService().findBookerByIdBooking(bookingDB.getId());
				if (booker != null && RITMBSConstants.AUTHORIZED.equals(booker.getIs_authorized())
						&& RITMBSConstants.BOOKING_ASKED.equals(bookingDB.getStatus())) {
					ob.setAuthorized("true");
				}
			}
		}

		// Save booking info to session avoid research db
		request.getSession().setAttribute(RITMBSConstants.ONLINE_BOOKING_SESSION, bookingObj);
		request.getSession().setAttribute("structure", structure);

		// Set to object
		ob.setCleaning(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee())));
		ob.setDeposit(Double.parseDouble(CommonUtils.formatConcurrency(structure.getDeposit())));
		ob.setSubTotalOffline(Double.parseDouble(CommonUtils.formatConcurrency(structure.getCleaningFee() + structure.getDeposit())));
		ob.setTotal(ob.getCleaning() + ob.getDeposit() + ob.getSubTotal());
		ob.setStatusCode(SUCCESS);
		ob.setStatusCodeDetail(SUCCESS);
		ob.setDateIn(CommonUtils.formatDate(bookingObj.getDateIn()));
		ob.setDateOut(CommonUtils.formatDate(bookingObj.getDateOut()));
		ob.setNrGuest(bookingObj.getNrGuests());
		ob.setIdRoom(bookingObj.getId_room());

		return ob;
	}

	private OnlineBooking onlineBookingResult(HttpServletRequest request) {
		OnlineBooking ob = new OnlineBooking();
		Integer idStructure = CommonUtils.parseInt(request.getParameter("idStructure"), -1);
		Integer idBooking = CommonUtils.parseInt(request.getParameter("idBooking"), -1);
		Structure structure = this.getStructureService().findStructureById(idStructure);
		Booking bookingObj = this.getBookingService().findBookingById(idBooking);

		if (bookingObj != null) {
			// Update booking after payment success
			bookingObj.setStatus("online");
			if (this.getBookingService().updateOnlineBooking(bookingObj, bookingObj.getBooker()) == 0) {
				ob.setStatusCode("ERROR");
				ob.setStatusCodeDetail(UPDATEERROR);
				ob.setMessage("This booking cannot be updated");
				return ob;
			}
		}

		if (structure != null) {
			ob.setStructureEmail(structure.getEmail());
			ob.setStructureName(structure.getName());
			ob.setStructurePhone(structure.getPhone());
		}
		ob.setStatusCode("SUCCESS");
		ob.setStatusCodeDetail(SUCCESS);
		return ob;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public RoomTypeService getRoomTypeService() {
		return roomTypeService;
	}

	public void setRoomTypeService(RoomTypeService roomTypeService) {
		this.roomTypeService = roomTypeService;
	}

	public BookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}

	public ConventionService getConventionService() {
		return conventionService;
	}

	public void setConventionService(ConventionService conventionService) {
		this.conventionService = conventionService;
	}

	public BookerService getBookerService() {
		return bookerService;
	}

	public void setBookerService(BookerService bookerService) {
		this.bookerService = bookerService;
	}

	public GuestService getGuestService() {
		return guestService;
	}

	public void setGuestService(GuestService guestService) {
		this.guestService = guestService;
	}
}