package action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import utils.RITMBSConstants;

public class OnlineBookingJob implements Job {
	private static Logger logger = Logger.getLogger(Logger.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("===========Remove pending booking job=============");
		Connection conn = null;
		try {
			Properties prop = new Properties();
			prop.load(OnlineBookingJob.class.getResourceAsStream("/quartz_job.properties"));
			conn = DriverManager.getConnection(prop.getProperty("quartz.connection.driver") + "?" + "user=" + prop.getProperty("quartz.connection.username")
					+ "&password=" + prop.getProperty("quartz.connection.password"));

			// Select booking id
			PreparedStatement selectStatement = conn
					.prepareStatement("SELECT * FROM booking WHERE status = ? AND TIMESTAMPDIFF(SECOND,bookingTime,NOW()) >= ?");
			selectStatement.setString(1, "pending");
			selectStatement.setLong(2, RITMBSConstants.QUARTZ_SCHEDULE_MINUTES * 60);
			ResultSet rs = selectStatement.executeQuery();
			List<Integer> bookingIdList = new ArrayList<Integer>();
			while (rs.next()) {
				bookingIdList.add(Integer.parseInt(rs.getString("id")));
			}

			// Delete (logically) booker by booking id
			for (Integer bkerId : bookingIdList) {
                PreparedStatement deleteBookerStatement = conn.prepareStatement("UPDATE booker SET is_authorized = ? WHERE id_booking = ?");
				deleteBookerStatement.setInt(1, 0);
                deleteBookerStatement.setInt(2, bkerId);
				deleteBookerStatement.execute();
			}

			// Delete (logically) booking that have staus = pending
			PreparedStatement deleteBookingStatement = conn
					.prepareStatement("UPDATE booking SET status = ? WHERE status = ? AND TIMESTAMPDIFF(SECOND,bookingTime,NOW()) >= ?");
            deleteBookingStatement.setString(1, "sysdeleted");
			deleteBookingStatement.setString(2, "pending");
			deleteBookingStatement.setLong(3, RITMBSConstants.QUARTZ_SCHEDULE_MINUTES * 60);
			deleteBookingStatement.execute();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}