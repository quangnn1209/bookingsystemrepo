package action;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import model.Booking;
import model.internal.Message;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;

import utils.CommonUtils;
import utils.GenerateAccessToken;
import utils.RITMBSConstants;

import com.opensymphony.xwork2.ActionSupport;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.PayPalRESTException;
import com.paypal.core.rest.PayPalResource;

@ParentPackage(value = "default")
public class PayPalAction extends ActionSupport implements SessionAware {
	private Map<String, Object> session = null;
	private String PayerID;
	private String guid;
	private String redirectUrl;
	private String token;
	private String accessToken;
	private Message message = new Message();

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Actions({ @Action(value = "/goPayPalPayment", results = { @Result(name = "success", location = "${redirectUrl}", type = "redirect"),
			@Result(name = "input", location = "/WEB-INF/jsp/online/validationError.jsp"),
			@Result(type = "json", name = "error", params = { "root", "message" }) }) })
	public String goPayPalPayment() throws IOException {
		HttpServletRequest req = ServletActionContext.getRequest();
		Booking booking = (Booking) this.getSession().get(RITMBSConstants.ONLINE_BOOKING_SESSION);
		Properties prop = new Properties();
		prop.load(PayPalAction.class.getResourceAsStream("/sdk_config.properties"));

		Map<String, String> sdkConfig = initialSdkConfig();

		// ##Load Configuration
		// Load SDK configuration for
		// the resource. This intialization code can be
		// done as Init Servlet.
		InputStream is = PayPalAction.class.getResourceAsStream("/sdk_config.properties");
		try {
			PayPalResource.initConfig(is);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}

		// ###AccessToken
		// Retrieve the access token from
		// OAuthTokenCredential by passing in
		// ClientID and ClientSecret
		APIContext apiContext = null;
		String accessToken = null;
		try {
			accessToken = GenerateAccessToken.getAccessToken();

			// ### Api Context
			// Pass in a `ApiContext` object to authenticate
			// the call and to send a unique request id
			// (that ensures idempotency). The SDK generates
			// a request id if you do not pass one explicitly.
			apiContext = new APIContext(accessToken);
			// Use this variant if you want to pass in a request id
			// that is meaningful in your application, ideally
			// a order id.
			/*
			 * String requestId = Long.toString(System.nanoTime(); APIContext
			 * apiContext = new APIContext(accessToken, requestId ));
			 */
			apiContext.setConfigurationMap(sdkConfig);
			if (PayerID != null) {
				Payment payment = new Payment();
				if (guid != null) {
					payment.setId(guid);
				}

				PaymentExecution paymentExecution = new PaymentExecution();
				paymentExecution.setPayerId(PayerID);
				try {
					payment.execute(apiContext, paymentExecution);
				} catch (PayPalRESTException e) {
					e.printStackTrace();
				}
			} else {

				// ###Details
				// Let's you specify details of a payment amount.
				// Details details = new Details();
				// details.setShipping("1");
				// details.setSubtotal(totalBill);
				// details.setTax("1");

				// ###Amount
				// Let's you specify a payment amount.
				Amount amount = new Amount();
				amount.setCurrency(prop.getProperty("concurrency.unit"));
				// Total must be equal to sum of shipping, tax and subtotal.
				// Double totalBill = booking.getRoomSubtotal() +
				// booking.getExtraSubtotal();
				amount.setTotal(CommonUtils.formatConcurrency(booking.getRoom().getPrice()));
				// amount.setTotal(CommonUtils.formatConcurrency(0.0));
				// amount.setDetails(details);

				// ###Transaction
				// A transaction defines the contract of a
				// payment - what is the payment for and who
				// is fulfilling it. Transaction is created with
				// a `Payee` and `Amount` types
				Transaction transaction = new Transaction();
				transaction.setAmount(amount);
				transaction.setDescription(RITMBSConstants.PAYMENT_DESCRIPTION);

				// The Payment creation API requires a list of
				// Transaction; add the created `Transaction`
				// to a List
				List<Transaction> transactions = new ArrayList<Transaction>();
				transactions.add(transaction);

				// ###Payer
				// A resource representing a Payer that funds a payment
				// Payment Method
				// as 'paypal'
				Payer payer = new Payer();
				payer.setPaymentMethod(RITMBSConstants.PAYMENT_METHOD);

				// ###Payment
				// A Payment Resource; create one using
				// the above types and intent as 'sale'
				Payment payment = new Payment();
				payment.setIntent(RITMBSConstants.PAYMENT_INTENT);
				payment.setPayer(payer);
				payment.setTransactions(transactions);

				// ###Redirect URLs
				RedirectUrls redirectUrls = new RedirectUrls();
				String guid = UUID.randomUUID().toString().replaceAll("-", "");
				// Read redirect url in properties file

				String redirectCancelUrl = prop.getProperty("redirect.cancelUrl");
				String redirectReturnUrl = prop.getProperty("redirect.returnUrl");
				String host = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath();

				if (redirectCancelUrl != null && !redirectCancelUrl.isEmpty()) {
					redirectUrls.setCancelUrl(redirectCancelUrl.contains("?") ? redirectCancelUrl + "&guid=" + guid + "&idBooking=" + booking.getId()
							: redirectCancelUrl + "?guid=" + guid + "&idBooking=" + booking.getId());
				} else {
					redirectUrls.setCancelUrl(host + "/goPayPalPayment?guid=" + guid + "&idBooking=" + booking.getId());
				}

				// if (redirectReturnUrl != null &&
				// !redirectReturnUrl.isEmpty()) {
				// redirectUrls.setReturnUrl(redirectReturnUrl.contains("?") ?
				// redirectReturnUrl + "&guid=" + guid + "&idBooking=" +
				// booking.getId()
				// : redirectReturnUrl + "?guid=" + guid + "&idBooking=" +
				// booking.getId());
				// } else {
				// redirectUrls.setReturnUrl(host +
				// "/doExpressCheckoutPayment?guid=" + guid + "&idBooking=" +
				// booking.getId());
				redirectUrls.setReturnUrl(host + "/executePayment?guid=" + guid + "&idBooking=" + booking.getId() + "&accessToken="
						+ accessToken.replaceAll(" ", "%20"));
				this.getSession().put("redirectUrl", redirectReturnUrl + "?guid=" + guid + "&idBooking=" + booking.getId());
				// }

				payment.setRedirectUrls(redirectUrls);

				// Create a payment by posting to the APIService
				// using a valid AccessToken
				// The return object contains the status;
				try {
					Payment createdPayment = payment.create(apiContext);
					// Store payment id
					this.getSession().put("paymentId", createdPayment.getId());

					// ###Payment Approval Url
					Iterator<Links> links = createdPayment.getLinks().iterator();
					while (links.hasNext()) {
						Links link = links.next();
						if (link.getRel().equalsIgnoreCase("approval_url")) {
							this.setRedirectUrl(link.getHref());
						}
					}
				} catch (PayPalRESTException e) {
					this.getMessage().setDescription(e.getMessage());
					this.getMessage().setResult(ERROR);
					this.getSession().put("paymentId", null);
					return ERROR;
				}
			}
		} catch (PayPalRESTException e) {
			this.getMessage().setDescription(e.getMessage());
			this.getMessage().setResult(ERROR);
			this.getSession().put("paymentId", null);
			return ERROR;
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/executePayment", results = { @Result(name = "success", location = "${redirectUrl}", type = "redirect"),
			@Result(type = "json", name = "error", params = { "root", "message" }) }) })
	public String executePayment() throws IOException {
		Map<String, String> sdkConfig = initialSdkConfig();
		this.setRedirectUrl((String) this.getSession().get("redirectUrl"));

		APIContext apiContext = new APIContext(this.getAccessToken().replaceAll("%20", " "));
		apiContext.setConfigurationMap(sdkConfig);

		Payment payment = new Payment();
		payment.setId((String) this.getSession().get("paymentId"));

		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(this.getPayerID());
		Payment executed = null;
		try {
			executed = payment.execute(apiContext, paymentExecute);
		} catch (Exception e) {
			e.printStackTrace();
			this.getSession().put("paymentId", null);
			return ERROR;
		}
		if (executed != null && "pending".equals(executed.getState())) {
			this.getSession().put("paymentId", null);
			this.getSession().put("redirectUrl", null);
			return SUCCESS;
		}
		return ERROR;
	}

	private Map<String, String> initialSdkConfig() throws IOException {
		Properties prop = new Properties();
		prop.load(PayPalAction.class.getResourceAsStream("/sdk_config.properties"));

		Map<String, String> sdkConfig = new HashMap<String, String>();

		if (prop.getProperty("service.EndPoint").contains("sandbox")) {
			sdkConfig.put("mode", "sandbox");
		} else {
			sdkConfig.put("mode", "live");
		}
		return sdkConfig;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPayerID() {
		return PayerID;
	}

	public void setPayerID(String payerID) {
		PayerID = payerID;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
}
