package action;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import utils.RITMBSConstants;

public class QuartzSchedulerListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		JobDetail job = JobBuilder.newJob(OnlineBookingJob.class).withIdentity("removePendingJob", "group1").build();

		try {

			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("removePendingTrigger", "group1")
					.withSchedule(CronScheduleBuilder.cronSchedule("0 0/" + RITMBSConstants.QUARTZ_SCHEDULE_MINUTES + " * 1/1 * ? *")).build();

			Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);

		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
