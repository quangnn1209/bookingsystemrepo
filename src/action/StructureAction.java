/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package action;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import model.File;
import model.Image;
import model.Publishing;
import model.RoomType;
import model.RoomWz;
import model.Structure;
import model.User;
import model.UserAware;
import model.Wizard;
import model.internal.Message;

import org.apache.commons.io.IOUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import service.FacilityService;
import service.FileService;
import service.ImageService;
import service.PublishingService;
import service.RoomService;
import service.RoomTypeService;
import service.StructurePropertyTypeService;
import service.StructureService;
import service.UserService;
import service.WizardService;
import utils.RITMBSConstants;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "default")
@InterceptorRefs({ @InterceptorRef("userAwareStack") })
@Result(name = "accessDenied", location = "/WEB-INF/jsp/homeNotLogged.jsp")
public class StructureAction extends ActionSupport implements SessionAware, UserAware {
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private StructureService structureService = null;
	@Autowired
	private FacilityService facilityService = null;
	@Autowired
	private ImageService imageService = null;
	@Autowired
	private SolrServer solrServerStructure = null;
	@Autowired
	private ApplicationContext applicationContext = null;
	@Autowired
	private StructurePropertyTypeService structurePropertyTypeService = null;
	@Autowired
	private PublishingService publishingService;
	@Autowired
	private UserService userService;
	@Autowired
	private FileService fileService;
	@Autowired
	private WizardService wizardService;
	@Autowired
	private RoomTypeService roomTypeService;
	private Map<String, Object> session = null;
	private Message message = new Message();
	private Structure structure = null;
	private Image image = null;
	private User user;
	private String password;
	private String reTyped;
	private Integer idStructure;
	private String idPropertyType;

	@Actions({ @Action(value = "/goUpdateDetails", results = { @Result(name = "success", location = "/WEB-INF/jsp/structure.jsp") }) })
	public String goUpdateDetails() {
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveStructure", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "saveWizard3" }) }) })
	public String saveStructure() {
		// Get current user
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);
		structure.setId_user(user.getId());
		List<Structure> structures = this.getStructureService().findStructureByIdUserByStatus(user.getId(), RITMBSConstants.STRUCTURE_IS_ENABLE);
		if (structures != null && !structures.isEmpty()) {
			structure.setIsEnable(RITMBSConstants.STRUCTURE_IS_DISABLE);
		} else {
			structure.setIsEnable(RITMBSConstants.STRUCTURE_IS_ENABLE);
		}

		if (structure.getId() == null) {
			// Save static map
			insertStaticMap(structure);

			// Insert structure
			this.getStructureService().insertStructure(structure);

			try {
				this.getSolrServerStructure().addBean(structure);
				this.getSolrServerStructure().commit();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SolrServerException e) {
				e.printStackTrace();
			}

			// Get structure id
			Integer idStructureLatest = structure.getId();

			// Create table structure property type.
			for (String id : this.getIdPropertyType().split(",")) {
				if (!id.isEmpty() && !"undefined".equals(this.getIdPropertyType()))
					this.getStructurePropertyTypeService().insert(idStructureLatest, new Integer(id));
			}

			// Update wizard
			Wizard wz = (Wizard) this.getSession().get("wizard");
			wz.setIdStructure(idStructureLatest);
			this.getWizardService().updateWizard(wz);
			this.getSession().put("wizard", wz);

			// Save room type - structure
			List<RoomWz> roomWzList = wizardService.findRoomWzByWzId(wz.getId());

			// Delete room wz
			wizardService.deleteRoomWz(wz.getId());
			for (RoomWz rw : roomWzList) {
				RoomType roomType = this.getRoomTypeService().findRoomTypeById(rw.getIdRoomType());
				roomType.setId_structure(idStructureLatest);

				this.getRoomTypeService().insertRoomType(roomType);

				// Re-update room wz
				RoomWz roomWz = new RoomWz();
				roomWz.setIdRoomType(roomType.getId());
				roomWz.setIdWz(wz.getId());
				roomWz.setNumbGuest(roomType.getMaxGuests());

				wizardService.insertRoomWz(roomWz);
			}
			// Update room wz list
			this.getSession().put("roomWzList", wizardService.findRoomWzByWzId(wz.getId()));

			// Set publishing info
			Publishing publishing = new Publishing();
			publishing.setId_structure(idStructureLatest);
			publishing.setId_user_owner(user.getId());
			publishing.setStatus(RITMBSConstants.UN_PUBLISH);

			// Insert publishing to database.
			this.getPublishingService().insertPublishing(publishing);

			// Update structure for user
			if (user.getStructure() == null) {
				user.setStructure(structure);
				this.getSession().put(RITMBSConstants.SESSION_USER, user);
			}
		} else {
			// Detect if structure changed address
			Structure oldStruct = this.getStructureService().findStructureById(structure.getId());
			if (oldStruct != null && oldStruct.compareTo(structure) == 0) {
				// Remove file
				this.getFileService().delete(oldStruct.getId_static_map());

				// Update static map
				this.insertStaticMap(structure);
			}
			this.updateStructure(structure);
		}

		return SUCCESS;
	}

	@Actions({ @Action(value = "/updateAccount", results = { @Result(type = "json", name = "success", params = { "root", "message" }),
			@Result(type = "json", name = "error", params = { "root", "message" }) }) })
	public String updateAccount() {
		User user = null;
		User newUser = this.getUser();
		user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);
		if (RITMBSConstants.ROLE_ADMIN_ID.equals(user.getId_role())) {
			// Validate password
			if (((this.getPassword().length() <= 5) && (this.getReTyped().length() <= 5)) || this.getPassword().length() > RITMBSConstants.STRING_MAX_LENGTH) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("passwordLengthError"));
				return ERROR;
			}
			if (!this.getPassword().equals(this.getReTyped())) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("passwordConfirmError"));
				return ERROR;
			}

			// Validate maxlength
			if (newUser.getSurname().length() > RITMBSConstants.STRING_MAX_LENGTH) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("firstNameLengthError", RITMBSConstants.STRING_MAX_LENGTH.toString()));
				return ERROR;
			}
			if (newUser.getName().length() > RITMBSConstants.STRING_MAX_LENGTH) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("lastNameLengthError", RITMBSConstants.STRING_MAX_LENGTH.toString()));
				return ERROR;
			}
			if (newUser.getPhone().length() > RITMBSConstants.STRING_MAX_LENGTH) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("phoneLengthError", RITMBSConstants.STRING_MAX_LENGTH.toString()));
				return ERROR;
			}
			if (newUser.getIdritmuser().length() > RITMBSConstants.STRING_IDRITMUSER_MAX_LENGTH) {
				this.getMessage().setResult(Message.ERROR);
				this.getMessage().setDescription(getText("idritmuserLengthError", RITMBSConstants.STRING_IDRITMUSER_MAX_LENGTH.toString()));
				return ERROR;
			}

			user.setSurname(newUser.getSurname());
			user.setName(newUser.getName());
			user.setPhone(newUser.getPhone());
			user.setIdritmuser(newUser.getIdritmuser());
			user.setId_role(newUser.getId_role());
			user.setId_language(newUser.getId_language());
			user.setPassword(this.getPassword());
			this.getUserService().updateUser(user);
			this.getSession().put(RITMBSConstants.SESSION_USER, user);
			this.getSession().put("languageFile", "global_" + RITMBSConstants.LANGUAGE_MAP.get(user.getId_language()));
			this.getMessage().setResult(Message.SUCCESS);
			this.getMessage().setDescription(getText("passwordUpdateSuccessAction"));
		} else {
			this.getMessage().setResult(Message.ERROR);
			this.getMessage().setDescription(getText("dontHavePermission"));
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/resetstructure", results = { @Result(name = "success", location = "/WEB-INF/jsp/structure.jsp"),
			@Result(name = "loginError", location = "/WEB-INF/jsp/login.jsp") }) })
	public String resetStructure() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);
		List<Structure> structures = this.getStructureService().findStructureByIdUserByStatus(user.getId(), RITMBSConstants.STRUCTURE_IS_ENABLE);
		if (structures == null) {
			this.getSession().put(RITMBSConstants.SESSION_USER, null);
			return "loginError";
		} else {
			user.setStructure(structures.get(0));
			this.getSession().put(RITMBSConstants.SESSION_USER, user);
		}
		return SUCCESS;
	}

	private void insertStaticMap(Structure structure) {
		// Save static map
		if (!structure.getAddress().isEmpty() && !structure.getCity().isEmpty() && !structure.getCountry().isEmpty()) {
			// Build url
			String urlStr = "http://maps.google.com/maps/api/staticmap?size=512x512&maptype=roadmap&format=JPG&center=";
			urlStr += structure.getAddress();
			urlStr += ",";
			urlStr += structure.getCity();
			urlStr += ",";
			urlStr += structure.getCountry();
			urlStr = urlStr.replaceAll(" ", "%20");

			// Open url
			URL url;
			try {
				url = new URL(urlStr);
				URLConnection connection = url.openConnection();
				connection.setRequestProperty("User-Agent", "Java web application");

				// Create file model
				File fileModel = new File();
				fileModel.setName(structure.getName() + "_static_map.JPG");

				try {
					fileModel.setData(IOUtils.toByteArray(connection.getInputStream()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				// Insert file
				// set file id to structure
				structure.setId_static_map(this.getFileService().insert(fileModel));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void updateStructure(Structure s) {
		try {
			this.getStructureService().updateStructure(s);
			this.getSolrServerStructure().addBean(s);
			this.getSolrServerStructure().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public Integer getIdStructure() {
		return idStructure;
	}

	public void setIdStructure(Integer idStructure) {
		this.idStructure = idStructure;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getReTyped() {
		return reTyped;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setReTyped(String reTyped) {
		this.reTyped = reTyped;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public FacilityService getFacilityService() {
		return facilityService;
	}

	public void setFacilityService(FacilityService facilityService) {
		this.facilityService = facilityService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public SolrServer getSolrServerStructure() {
		return solrServerStructure;
	}

	public void setSolrServerStructure(SolrServer solrServerStructure) {
		this.solrServerStructure = solrServerStructure;
	}

	public StructurePropertyTypeService getStructurePropertyTypeService() {
		return structurePropertyTypeService;
	}

	public void setStructurePropertyTypeService(StructurePropertyTypeService structurePropertyTypeService) {
		this.structurePropertyTypeService = structurePropertyTypeService;
	}

	public PublishingService getPublishingService() {
		return publishingService;
	}

	public void setPublishingService(PublishingService publishingService) {
		this.publishingService = publishingService;
	}

	public FileService getFileService() {
		return fileService;
	}

	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}

	public String getIdPropertyType() {
		return idPropertyType;
	}

	public void setIdPropertyType(String idPropertyType) {
		this.idPropertyType = idPropertyType;
	}

	public WizardService getWizardService() {
		return wizardService;
	}

	public void setWizardService(WizardService wizardService) {
		this.wizardService = wizardService;
	}

	public RoomTypeService getRoomTypeService() {
		return roomTypeService;
	}

	public void setRoomTypeService(RoomTypeService roomTypeService) {
		this.roomTypeService = roomTypeService;
	}
}