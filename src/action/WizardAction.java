/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package action;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import model.Image;
import model.PropertyType;
import model.Room;
import model.RoomType;
import model.RoomWz;
import model.Structure;
import model.User;
import model.Wizard;
import model.WzImage;
import model.internal.Message;
import model.listini.Convention;
import model.listini.Period;
import model.listini.RoomPriceList;
import model.listini.RoomPriceListItem;
import model.listini.Season;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import service.ConventionService;
import service.FileService;
import service.ImageFileService;
import service.ImageService;
import service.PeriodService;
import service.PropertyTypeService;
import service.RoomImageService;
import service.RoomPriceListService;
import service.RoomService;
import service.RoomTypeService;
import service.SeasonService;
import service.StructureImageService;
import service.StructureService;
import service.UserService;
import service.WizardService;
import utils.CommonUtils;
import utils.I18nUtils;
import utils.RITMBSConstants;

import com.ibm.icu.util.Calendar;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(value = "default")
@InterceptorRefs({ @InterceptorRef("userAwareStack") })
@Result(name = "accessDenied", location = "/WEB-INF/jsp/homeNotLogged.jsp")
public class WizardAction extends ActionSupport implements SessionAware {

	private Map<String, Object> session = null;
	@Autowired
	private UserService userService = null;
	@Autowired
	private PropertyTypeService propertyTypeService;
	@Autowired
	private RoomTypeService roomTypeService;
	@Autowired
	private WizardService wizardService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private PeriodService periodService = null;
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private SeasonService seasonService;
	@Autowired
	private StructureService structureService;
	@Autowired
	private ConventionService conventionService;
	@Autowired
	private RoomPriceListService roomPriceListService;
	@Autowired
	private StructureImageService structureImageService;
	@Autowired
	private RoomImageService roomImageService;
	@Autowired
	private ImageFileService imageFileService;
	@Autowired
	private FileService fileService;
	private List<PropertyType> propertyTypeList = null;
	private List<RoomType> roomTypeList = null;
	private String idPropertyType;
	private String[] idRoomType;
	private List<Image> imageList = null;
	private Period period = null;
	private String startDate;
	private String endDate;
	private String roomTypeName;
	private String idRoomTypeSingle;
	private String idRoom;
	private Room room;
	private String roomWzId;
	private String propertyTypeName;
	private String action;
	private Structure structure;
	private Message message;
	private Integer idStructure;
	private Integer idImage;
	private String command;
	private String idFiles;

	@Actions({ @Action(value = "/goWizard", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/wizard1.jsp"),
			@Result(name = "success2", type = "redirectAction", params = { "actionName", "goWizard2" }),
			@Result(name = "success3", type = "redirectAction", params = { "actionName", "goWizard3" }),
			@Result(name = "success4", type = "redirectAction", params = { "actionName", "goWizard4" }),
			@Result(name = "success5", type = "redirectAction", params = { "actionName", "goWizard5" }),
			@Result(name = "success6", type = "redirectAction", params = { "actionName", "goWizard6" }) }) })
	public String goWizard() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);

		// Find wizard by user id
		List<Wizard> models = wizardService.findWizardByIdUser(user.getId());
		if (models != null && !models.isEmpty()) {
			this.getSession().put("wizard", models.get(0));
			this.getSession().put("roomWzList", wizardService.findRoomWzByWzId(models.get(0).getId()));
			switch (models.get(0).getProgress()) {
			case 15:
				return "success2";
			case 30:
				return "success3";
			case 45:
				return "success4";
			default:
				return "success5";
			}
		} else {
			// Start the new one
			this.setPropertyTypeList(propertyTypeService.findAll());
			this.setRoomTypeList(roomTypeService.findAll());
			return SUCCESS;
		}
	}

	@Actions({ @Action(value = "/goWizard2", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/wizard2.jsp"),
			@Result(name = "error", type = "redirectAction", params = { "actionName", "goWizard" }) }) })
	public String goWizard2() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");

		if (wizard == null) {
			return ERROR;
		}

		if (wizard.getProgress() == 15) {
			this.setAction("saveWizard2");
		} else {
			this.setAction("goWizard3");
		}

		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveWizard2", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "goWizard3" }) }) })
	public String saveWizard2() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);

		// Find wizard by user id
		List<Wizard> models = wizardService.findWizardByIdUser(user.getId());
		// Update progress
		if (models.get(0).getProgress() < 30) {
			models.get(0).setProgress(30);
			wizardService.updateWizard(models.get(0));
		}

		this.getSession().put("wizard", models.get(0));
		return SUCCESS;
	}

	@Actions({ @Action(value = "/goWizard3", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/wizard3.jsp"),
			@Result(name = "error", type = "redirectAction", params = { "actionName", "goWizard" }) }) })
	public String goWizard3() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard == null) {
			return ERROR;
		}

		if (wizard.getIdStructure() != null) {
			this.setStructure(this.getStructureService().findStructureById(wizard.getIdStructure()));
		}
		this.setPropertyTypeName(this.getPropertyTypeService().findPropertyById(wizard.getIdPropertyType()).getName());
		if (wizard.getProgress() == 30) {
			this.setAction("saveWizard3");
		} else {
			this.setAction("goWizard4");
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveWizard3", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "goWizard3" }) }) })
	public String saveWizard3() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);

		// Find wizard by user id
		List<Wizard> models = wizardService.findWizardByIdUser(user.getId());
		if (models.get(0).getProgress() < 45) {
			// Update id structure for image
			for (WzImage wzImage : wizardService.findWzImageByWzId(models.get(0).getId())) {
				Image image = this.getImageService().find(wzImage.getIdImage());
				image.setId_structure(models.get(0).getIdStructure());
				this.getImageService().update(image);
			}

			// Update progress
			models.get(0).setProgress(45);
			wizardService.updateWizard(models.get(0));
		}

		this.getSession().put("wizard", models.get(0));
		return SUCCESS;
	}

	@Actions({ @Action(value = "/goWizard4", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/wizard4.jsp"),
			@Result(name = "error", type = "redirectAction", params = { "actionName", "goWizard" }) }) })
	public String goWizard4() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard == null) {
			return ERROR;
		}

		List<Season> seasonList = this.getSeasonService().findSeasonsByIdStructure(wizard.getIdStructure());
		if (seasonList != null && !seasonList.isEmpty()) {
			// Find period
			List<Period> periods = this.getPeriodService().findPeriodsByIdSeason(seasonList.get(0).getId());
			if (periods != null && !periods.isEmpty())
				this.setPeriod(this.getPeriodService().findPeriodsByIdSeason(seasonList.get(0).getId()).get(0));
		}

		if (wizard.getProgress() == 45) {
			this.setAction("saveWizard4");
		} else {
			this.setAction("goWizard5");
		}

		this.setMessage((Message) this.getSession().get("message"));
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveWizard4", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "goWizard5" }) }) })
	public String saveWizard4() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);

		// Find wizard by user id
		List<Wizard> models = wizardService.findWizardByIdUser(user.getId());
		if (models.get(0).getProgress() < 60) {
			// Update progress
			models.get(0).setProgress(60);
			wizardService.updateWizard(models.get(0));
		}
		this.getSession().put("wizard", models.get(0));
		return SUCCESS;
	}

	@Actions({ @Action(value = "/goWizard5", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/wizard5.jsp"),
			@Result(name = "error", type = "redirectAction", params = { "actionName", "goWizard" }) }) })
	public String goWizard5() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard == null) {
			return ERROR;
		}

		this.getSession().put("roomWzList", wizardService.findRoomWzByWzId(wizard.getId()));

		this.setAction("saveWizard5");
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveWizard1", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "goWizard2" }) }) })
	public String saveWizard1() {
		User user = (User) this.getSession().get(RITMBSConstants.SESSION_USER);

		// Insert new wizard
		Wizard model = new Wizard();
		model.setIdPropertyType(Integer.parseInt(this.getIdPropertyType()));
		model.setUserId(user.getId());
		model.setProgress(15);
		wizardService.insertWizard(model);

		// get wizard id
		int idWz = model.getId();
		for (int i = 0; i < this.getIdRoomType().length; i++) {
			RoomWz roomWz = new RoomWz();
			roomWz.setIdRoomType(Integer.parseInt(this.getIdRoomType()[i].split("-")[0]));
			roomWz.setIdWz(idWz);
			roomWz.setNumbGuest(roomTypeService.findRoomTypeById(roomWz.getIdRoomType()).getMaxGuests());

			wizardService.insertRoomWz(roomWz);
		}
		this.getSession().put("wizard", wizardService.findWizardById(idWz));
		this.getSession().put("roomWzList", wizardService.findRoomWzByWzId(idWz));
		return SUCCESS;
	}

	@Actions({ @Action(value = "/initialGalleryStructure", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/initialGallery.jsp") }) })
	public String initialGalleryStructure() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard != null) {
			// Get all available images
			List<WzImage> wzImageList = this.getWizardService().findWzImageByWzId(wizard.getId());
			if (wzImageList != null) {
				List<Image> imageList = new ArrayList<Image>();

				for (WzImage wzImage : wzImageList) {
					imageList.add(this.getImageService().find(wzImage.getIdImage()));
				}

				// Find image belong to structure
				List<Integer> idImages = this.getStructureImageService().findIdImageByIdStructure(this.getIdStructure(), 0, Integer.MAX_VALUE);
				if (idImages != null) {
					for (Integer id : idImages) {
						Image image = this.getImageService().find(id);
						image.setIsChecked(true);
						Integer idFile = this.getImageFileService().findIdFileByIdImage(id);
						image.setFile(this.getFileService().findById(idFile));
						imageList.add(image);
					}
				}
				Collections.sort(imageList);
				this.setImageList(imageList);
			}
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/initialGalleryRoom", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/initialGallery.jsp") }) })
	public String initialGalleryRoom() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard != null) {
			// Get all available images
			List<WzImage> wzImageList = this.getWizardService().findWzImageByWzId(wizard.getId());
			if (wzImageList != null) {
				List<Image> imageList = new ArrayList<Image>();

				for (WzImage wzImage : wzImageList) {
					imageList.add(this.getImageService().find(wzImage.getIdImage()));
				}

				// Find image belong to room
				List<Integer> idImages = this.getRoomImageService().findIdImageByIdRoom(Integer.parseInt(this.getIdRoom()), 0, Integer.MAX_VALUE);
				if (idImages != null) {
					for (Integer id : idImages) {
						Image image = this.getImageService().find(id);
						image.setIsChecked(true);
						Integer idFile = this.getImageFileService().findIdFileByIdImage(id);
						image.setFile(this.getFileService().findById(idFile));
						imageList.add(image);
					}
				}
				Collections.sort(imageList);
				this.setImageList(imageList);
			}
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/initialGallery", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/initialGallery.jsp") }) })
	public String initialGallery() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard != null) {
			// Get all image
			List<WzImage> wzImageList = this.getWizardService().findAllWzImageByWzId(wizard.getId());
			if (wzImageList != null) {
				List<Image> imageList = new ArrayList<Image>();
				for (WzImage wzImage : wzImageList) {
					Integer idFile = this.getImageFileService().findIdFileByIdImage(wzImage.getIdImage());
					Image image = this.getImageService().find(wzImage.getIdImage());
					image.setFile(this.getFileService().findById(idFile));
					imageList.add(image);
				}
				Collections.sort(imageList);
				this.setImageList(imageList);
			}
		}
		return SUCCESS;
	}

	@Actions({ @Action(value = "/initialRoom", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/initialRoom.jsp") }) })
	public String initialRoom() {
		this.setRoomTypeName(this.getRoomTypeService().findRoomTypeById(Integer.parseInt(this.getIdRoomTypeSingle())).getName());
		this.setRoomWzId(this.getRoomWzId());
		return SUCCESS;
	}

	@Actions({ @Action(value = "/loadRoom", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/loadRoom.jsp") }) })
	public String loadRoom() {
		Room room = this.getRoomService().findRoomById(Integer.parseInt(this.getIdRoom()));
		Structure structure = this.getStructureService().findStructureById(room.getId_structure());
		Convention convention = this.getConventionService().findConventionsByIdStructure(structure.getId()).get(0);
		Season season = this.getSeasonService().findSeasonsByIdStructure(structure.getId()).get(0);
		RoomType roomType = this.getRoomTypeService().findRoomTypeById(room.getId_roomType());

		// Get room price list info
		RoomPriceList roomPriceList = this.getRoomPriceListService().findRoomPriceListByIdStructureAndIdSeasonAndIdRoomTypeAndIdConvention(structure.getId(),
				season.getId(), roomType.getId(), convention.getId());
		RoomPriceListItem roomPriceListItem = null;
		for (RoomPriceListItem rpli : roomPriceList.getItems()) {
			// Update room price list item
			if (rpli.getNumGuests().equals(roomType.getMaxGuests())) {
				roomPriceListItem = rpli;
			}
		}
		if (roomPriceListItem == null)
			roomPriceListItem = roomPriceList.getItems().get(0);
		room.setWeeklyPrice(Double.parseDouble(CommonUtils.formatConcurrency(roomPriceListItem.getPriceMonday() + roomPriceListItem.getPriceTuesday()
				+ roomPriceListItem.getPriceWednesday() + roomPriceListItem.getPriceThursday() + roomPriceListItem.getPriceFriday()
				+ roomPriceListItem.getPriceSaturday() + roomPriceListItem.getPriceSunday())));
		// Update roomwz
		RoomWz roomwz = this.getWizardService().findRoomWzById(Integer.parseInt(this.getRoomWzId()));
		roomwz.setIdRoom(room.getId());
		this.getWizardService().updateRoomWz(roomwz);

		// Set to view
		this.setRoom(room);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/updateProgress", results = { @Result(name = "success", location = "/WEB-INF/jsp/wizard/initialRoom.jsp") }) })
	public String updateProgress() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		if (wizard.getProgress() < 75) {
			// Update progress
			wizard.setProgress(75);
			wizardService.updateWizard(wizard);
		}

		// Update room Wz list to get room id
		this.getSession().put("roomWzList", wizardService.findRoomWzByWzId(wizard.getId()));
		this.getSession().put("wizard", wizard);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/finishWizard", results = { @Result(type = "json", name = "success", params = { "root", "message" }) }) })
	public String finishWizard() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");
		wizard.setProgress(100);
		wizardService.updateWizard(wizard);

		// Update room Wz list to get room id
		this.getSession().put("wizard", wizard);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/savePeriod", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "saveWizard4" }),
			@Result(name = "error", type = "redirectAction", params = { "actionName", "goWizard4" }) }) })
	public String savePeriod() throws ParseException {
		Wizard wizard = (Wizard) this.getSession().get("wizard");

		if (this.getPeriod() == null || this.getPeriod().getId() == null) {
			Season season = null;
			if (wizard.getIdSeason() == null) {
				// Create new season
				season = new Season();
				season.setId_structure(wizard.getIdStructure());
				season.setName("default");
				season.setYear(Calendar.getInstance().get(Calendar.YEAR));
				this.getSeasonService().insertSeason(season);

				// Update wizard
				wizard.setIdSeason(season.getId());
				this.getWizardService().updateWizard(wizard);
				this.getSession().put("wizard", wizard);
			} else {
				// Find season by id
				season = this.getSeasonService().findSeasonById(wizard.getIdSeason());
			}

			this.getStructureService().addPriceListsForSeason(season.getId_structure(), season.getId());

			if (this.getPeriod() == null) {
				this.setPeriod(new Period());
			}
			this.getPeriod().setStartDate(CommonUtils.convertStringToDate(this.getStartDate()));
			this.getPeriod().setEndDate(CommonUtils.convertStringToDate(this.getEndDate()));
			this.getPeriod().setId_season(season.getId());
			Boolean validDates = this.getPeriod().checkDates();
			Boolean validYear = this.getPeriodService().checkYears(this.getPeriod());
			Boolean overlap = this.getPeriodService().checkOverlappingPeriods(this.getPeriod());

			if (!validDates) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodDatesError"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}
			if (!validYear) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodYearError"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}
			if (overlap) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodOverlappedAction"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}
			this.getPeriodService().insertPeriod(this.getPeriod());
		} else {
			this.getPeriod().setStartDate(CommonUtils.convertStringToDate(this.getStartDate()));
			this.getPeriod().setEndDate(CommonUtils.convertStringToDate(this.getEndDate()));
			Boolean validDates = this.getPeriod().checkDates();
			Boolean validYear = this.getPeriodService().checkYears(this.getPeriod());
			Boolean overlap = this.getPeriodService().checkOverlappingPeriods(this.getPeriod());

			if (!validDates) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodDatesError"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}
			if (!validYear) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodYearError"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}
			if (overlap) {
				this.setMessage(new Message());
				this.getMessage().setDescription(I18nUtils.getProperty("periodOverlappedAction"));
				this.getSession().put("message", this.getMessage());
				return ERROR;
			}

			this.getPeriodService().updatePeriod(this.getPeriod());
		}

		this.getSession().put("message", null);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveWizard5", results = { @Result(name = "success", type = "redirectAction", params = { "actionName", "home" }) }) })
	public String saveWizard5() {
		Wizard wizard = (Wizard) this.getSession().get("wizard");

		// Delete room wz by wizard id
		wizardService.deleteRoomWz(wizard.getId());

		// Delete wizard image relationship
		wizardService.deleteWzImage(wizard.getId());

		// Delete wizard
		wizardService.deleteWizard(wizard.getId());

		this.getSession().put("wizard", null);
		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveStructureImage", results = { @Result(type = "json", name = "success", params = { "root", "message" }) }) })
	public String insertStructureImage() {
		this.setMessage(new Message());
		// Update status of image
		WzImage wzImage = this.getWizardService().findWzImageByImageId(this.getIdImage());
		if (this.getCommand().equals("add")) {
			wzImage.setStatus(0);
			this.getWizardService().updateWzImage(wzImage);
			this.getStructureImageService().insert(this.getIdStructure(), this.getIdImage());
		} else {
			wzImage.setStatus(1);
			this.getWizardService().updateWzImage(wzImage);
			Integer idStructureImage = this.getStructureImageService().findIdByIdStructureAndIdImage(this.getIdStructure(), this.getIdImage());
			// delete relationship
			if (idStructureImage != null)
				this.getStructureImageService().delete(idStructureImage);
		}

		return SUCCESS;
	}

	@Actions({ @Action(value = "/saveRoomImage", results = { @Result(type = "json", name = "success", params = { "root", "message" }) }) })
	public String insertRoomImage() {
		this.setMessage(new Message());
		// Update status of image
		WzImage wzImage = this.getWizardService().findWzImageByImageId(this.getIdImage());
		if (this.getCommand().equals("add")) {
			wzImage.setStatus(0);
			this.getWizardService().updateWzImage(wzImage);
			this.getRoomImageService().insert(Integer.parseInt(this.getIdRoom()), this.getIdImage());
		} else {
			wzImage.setStatus(1);
			this.getWizardService().updateWzImage(wzImage);
			Integer idRoomImage = this.getRoomImageService().findIdByIdRoomAndIdImage(Integer.parseInt(this.getIdRoom()), this.getIdImage());
			// delete relationship
			if (idRoomImage != null)
				this.getRoomImageService().delete(idRoomImage);
		}

		return SUCCESS;
	}

	@Actions({ @Action(value = "/deleteImage", results = { @Result(type = "json", name = "success", params = { "root", "message" }) }) })
	public String deleteImage() {
		this.setMessage(new Message());
		for (String idFile : this.getIdFiles().split(",")) {
			if (!idFile.isEmpty()) {
				// FInd image by file id
				Integer idImage = this.getImageFileService().findIdImageByIdFile(Integer.parseInt(idFile));

				// Delete relationship with structure
				this.getStructureImageService().deleteByIdImage(idImage);

				// Delete relationship with room
				this.getRoomImageService().deleteByIdImage(idImage);

				// Delete relationship wizard image
				this.getWizardService().deleteWzImageByIdImage(idImage);

				// Delete image
				this.getImageService().delete(idImage);
			}
		}
		return SUCCESS;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<PropertyType> getPropertyTypeList() {
		return propertyTypeList;
	}

	public void setPropertyTypeList(List<PropertyType> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}

	public List<RoomType> getRoomTypeList() {
		return roomTypeList;
	}

	public void setRoomTypeList(List<RoomType> roomTypeList) {
		this.roomTypeList = roomTypeList;
	}

	public PropertyTypeService getPropertyTypeService() {
		return propertyTypeService;
	}

	public void setPropertyTypeService(PropertyTypeService propertyTypeService) {
		this.propertyTypeService = propertyTypeService;
	}

	public RoomTypeService getRoomTypeService() {
		return roomTypeService;
	}

	public void setRoomTypeService(RoomTypeService roomTypeService) {
		this.roomTypeService = roomTypeService;
	}

	public String getIdPropertyType() {
		return idPropertyType;
	}

	public void setIdPropertyType(String idPropertyType) {
		this.idPropertyType = idPropertyType;
	}

	public String[] getIdRoomType() {
		return idRoomType;
	}

	public void setIdRoomType(String[] idRoomType) {
		this.idRoomType = idRoomType;
	}

	public WizardService getWizardService() {
		return wizardService;
	}

	public void setWizardService(WizardService wizardService) {
		this.wizardService = wizardService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public PeriodService getPeriodService() {
		return periodService;
	}

	public void setPeriodService(PeriodService periodService) {
		this.periodService = periodService;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public String getIdRoomTypeSingle() {
		return idRoomTypeSingle;
	}

	public void setIdRoomTypeSingle(String idRoomTypeSingle) {
		this.idRoomTypeSingle = idRoomTypeSingle;
	}

	public String getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(String idRoom) {
		this.idRoom = idRoom;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public String getRoomWzId() {
		return roomWzId;
	}

	public void setRoomWzId(String roomWzId) {
		this.roomWzId = roomWzId;
	}

	public SeasonService getSeasonService() {
		return seasonService;
	}

	public void setSeasonService(SeasonService seasonService) {
		this.seasonService = seasonService;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public String getPropertyTypeName() {
		return propertyTypeName;
	}

	public void setPropertyTypeName(String propertyTypeName) {
		this.propertyTypeName = propertyTypeName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Structure getStructure() {
		return structure;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public ConventionService getConventionService() {
		return conventionService;
	}

	public void setConventionService(ConventionService conventionService) {
		this.conventionService = conventionService;
	}

	public RoomPriceListService getRoomPriceListService() {
		return roomPriceListService;
	}

	public void setRoomPriceListService(RoomPriceListService roomPriceListService) {
		this.roomPriceListService = roomPriceListService;
	}

	public StructureImageService getStructureImageService() {
		return structureImageService;
	}

	public void setStructureImageService(StructureImageService structureImageService) {
		this.structureImageService = structureImageService;
	}

	public Integer getIdImage() {
		return idImage;
	}

	public void setIdImage(Integer idImage) {
		this.idImage = idImage;
	}

	public Integer getIdStructure() {
		return idStructure;
	}

	public void setIdStructure(Integer idStructure) {
		this.idStructure = idStructure;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public RoomImageService getRoomImageService() {
		return roomImageService;
	}

	public void setRoomImageService(RoomImageService roomImageService) {
		this.roomImageService = roomImageService;
	}

	public ImageFileService getImageFileService() {
		return imageFileService;
	}

	public void setImageFileService(ImageFileService imageFileService) {
		this.imageFileService = imageFileService;
	}

	public FileService getFileService() {
		return fileService;
	}

	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}

	public String getIdFiles() {
		return idFiles;
	}

	public void setIdFiles(String idFiles) {
		this.idFiles = idFiles;
	}
}