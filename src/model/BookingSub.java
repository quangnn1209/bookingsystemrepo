/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package model;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import model.listini.Convention;
import utils.CommonUtils;

public class BookingSub implements Serializable {

	private Integer id;

	private Guest booker;
	private Booker aBooker;
	private Integer nrGuests = 1;
	private Room room;
	private String dateIn;
	private String dateOut;
	private Double roomSubtotal = 0d;
	private Double extraSubtotal = 0d;
	private String notes;
	private List<Extra> extras;
	private List<Adjustment> adjustments;
	private List<Payment> payments = null;
	private String status = "confirmed";
	private List<Guest> guests = null;
	private Convention convention = null;
	private List<ExtraItem> extraItems;
	private CreditCard creditCard = null;
	private Integer id_structure = null;
	private Integer id_convention = null;
	private Integer id_room = null;

	private List<Housed> housedList;
	private Housed groupLeader;
	private GroupLeader aGroupLeader;

	public BookingSub() {
		this.extras = new ArrayList<Extra>();
		this.adjustments = new ArrayList<Adjustment>();
		this.payments = new ArrayList<Payment>();
		this.guests = new ArrayList<Guest>();
		this.extraItems = new ArrayList<ExtraItem>();
		this.housedList = new ArrayList<Housed>();
	}

	public Booking getBooking() throws ParseException {
		Booking booking = new Booking();
		booking.setaBooker(aBooker);
		booking.setAdjustments(adjustments);
		booking.setaGroupLeader(aGroupLeader);
		booking.setBooker(booker);
		booking.setConvention(convention);
		booking.setCreditCard(creditCard);
		booking.setDateIn(CommonUtils.convertStringToDate(dateIn));
		booking.setDateOut(CommonUtils.convertStringToDate(dateOut));
		booking.setExtraItems(extraItems);
		booking.setExtras(extras);
		booking.setExtraSubtotal(extraSubtotal);
		booking.setGroupLeader(groupLeader);
		booking.setGuests(guests);
		booking.setHousedList(housedList);
		booking.setId(id);
		booking.setId_convention(id_convention);
		booking.setId_room(id_room);
		booking.setId_structure(id_structure);
		booking.setNotes(notes);
		booking.setNrGuests(nrGuests);
		booking.setPayments(payments);
		booking.setRoom(room);
		booking.setRoomSubtotal(roomSubtotal);
		booking.setStatus(status);
		return booking;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Guest getBooker() {
		return booker;
	}

	public void setBooker(Guest booker) {
		this.booker = booker;
	}

	public Booker getaBooker() {
		return aBooker;
	}

	public void setaBooker(Booker aBooker) {
		this.aBooker = aBooker;
	}

	public Integer getNrGuests() {
		return nrGuests;
	}

	public void setNrGuests(Integer nrGuests) {
		this.nrGuests = nrGuests;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	public Double getRoomSubtotal() {
		return roomSubtotal;
	}

	public void setRoomSubtotal(Double roomSubtotal) {
		this.roomSubtotal = roomSubtotal;
	}

	public Double getExtraSubtotal() {
		return extraSubtotal;
	}

	public void setExtraSubtotal(Double extraSubtotal) {
		this.extraSubtotal = extraSubtotal;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<Extra> getExtras() {
		return extras;
	}

	public void setExtras(List<Extra> extras) {
		this.extras = extras;
	}

	public List<Adjustment> getAdjustments() {
		return adjustments;
	}

	public void setAdjustments(List<Adjustment> adjustments) {
		this.adjustments = adjustments;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

	public Convention getConvention() {
		return convention;
	}

	public void setConvention(Convention convention) {
		this.convention = convention;
	}

	public List<ExtraItem> getExtraItems() {
		return extraItems;
	}

	public void setExtraItems(List<ExtraItem> extraItems) {
		this.extraItems = extraItems;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public Integer getId_structure() {
		return id_structure;
	}

	public void setId_structure(Integer id_structure) {
		this.id_structure = id_structure;
	}

	public Integer getId_convention() {
		return id_convention;
	}

	public void setId_convention(Integer id_convention) {
		this.id_convention = id_convention;
	}

	public Integer getId_room() {
		return id_room;
	}

	public void setId_room(Integer id_room) {
		this.id_room = id_room;
	}

	public List<Housed> getHousedList() {
		return housedList;
	}

	public void setHousedList(List<Housed> housedList) {
		this.housedList = housedList;
	}

	public Housed getGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(Housed groupLeader) {
		this.groupLeader = groupLeader;
	}

	public GroupLeader getaGroupLeader() {
		return aGroupLeader;
	}

	public void setaGroupLeader(GroupLeader aGroupLeader) {
		this.aGroupLeader = aGroupLeader;
	}
}