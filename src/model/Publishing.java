/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Publishing implements Serializable {
	@Field
	private Integer id;
	@Field
	private Date date_modify;
	@Field
	private Integer id_user_owner;
	@Field
	private Integer id_user_publisher;
	@Field
	private Integer id_structure;
	@Field
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate_modify() {
		return date_modify;
	}

	public void setDate_modify(Date date_modify) {
		this.date_modify = date_modify;
	}

	public Integer getId_user_owner() {
		return id_user_owner;
	}

	public void setId_user_owner(Integer id_user_owner) {
		this.id_user_owner = id_user_owner;
	}

	public Integer getId_user_publisher() {
		return id_user_publisher;
	}

	public void setId_user_publisher(Integer id_user_publisher) {
		this.id_user_publisher = id_user_publisher;
	}

	public Integer getId_structure() {
		return id_structure;
	}

	public void setId_structure(Integer id_structure) {
		this.id_structure = id_structure;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}