/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomWz implements Serializable {

	@Field
	private Integer id;
	@Field
	private Integer idRoomType;
	@Field
	private Integer numbGuest;
	@Field
	private Integer idWz;
	@Field
	private Integer idRoom;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdRoomType() {
		return idRoomType;
	}

	public void setIdRoomType(Integer idRoomType) {
		this.idRoomType = idRoomType;
	}

	public Integer getNumbGuest() {
		return numbGuest;
	}

	public void setNumbGuest(Integer numbGuest) {
		this.numbGuest = numbGuest;
	}

	public Integer getIdWz() {
		return idWz;
	}

	public void setIdWz(Integer idWz) {
		this.idWz = idWz;
	}

	public Integer getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Integer idRoom) {
		this.idRoom = idRoom;
	}

}