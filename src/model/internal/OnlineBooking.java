/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package model.internal;

import java.io.Serializable;

public class OnlineBooking implements Serializable {
	private String message;
	private String resultType;
	private Double preCost;
	private Integer idRoom;
	private String dateIn;
	private String dateOut;
	private Integer maxGuest;
	private Integer nrGuest;
	private Double subTotal;
	private Double subTotalOffline;
	private Double roomPrice;
	private Double serviceCost;
	private String authorized;
	private String emailGuest;
	private Integer numNights;
	private Double bookingPrice;
	private String firstName;
	private String lastName;
	private String structureName;
	private String structurePhone;
	private String structureEmail;
	private String statusCode;
	private String hasRoomFreeInPeriod;
	private Double cleaning;
	private Double deposit;
	private Double total;
	private String unitSymbol;

	public String getStatusCodeDetail() {
		return statusCodeDetail;
	}

	public void setStatusCodeDetail(String statusCodeDetail) {
		this.statusCodeDetail = statusCodeDetail;
	}

	private String statusCodeDetail;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public Double getPreCost() {
		return preCost;
	}

	public void setPreCost(Double preCost) {
		this.preCost = preCost;
	}

	public Integer getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Integer idRoom) {
		this.idRoom = idRoom;
	}

	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	public Integer getMaxGuest() {
		return maxGuest;
	}

	public void setMaxGuest(Integer maxGuest) {
		this.maxGuest = maxGuest;
	}

	public Integer getNrGuest() {
		return nrGuest;
	}

	public void setNrGuest(Integer nrGuest) {
		this.nrGuest = nrGuest;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public String getAuthorized() {
		return authorized;
	}

	public void setAuthorized(String authorized) {
		this.authorized = authorized;
	}

	public String getEmailGuest() {
		return emailGuest;
	}

	public void setEmailGuest(String emailGuest) {
		this.emailGuest = emailGuest;
	}

	public Integer getNumNights() {
		return numNights;
	}

	public void setNumNights(Integer numNights) {
		this.numNights = numNights;
	}

	public Double getBookingPrice() {
		return bookingPrice;
	}

	public void setBookingPrice(Double bookingPrice) {
		this.bookingPrice = bookingPrice;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStructureName() {
		return structureName;
	}

	public void setStructureName(String structureName) {
		this.structureName = structureName;
	}

	public String getStructurePhone() {
		return structurePhone;
	}

	public void setStructurePhone(String structurePhone) {
		this.structurePhone = structurePhone;
	}

	public String getStructureEmail() {
		return structureEmail;
	}

	public void setStructureEmail(String structureEmail) {
		this.structureEmail = structureEmail;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Double getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(Double roomPrice) {
		this.roomPrice = roomPrice;
	}

	public Double getServiceCost() {
		return serviceCost;
	}

	public void setServiceCost(Double serviceCost) {
		this.serviceCost = serviceCost;
	}

	public String getHasRoomFreeInPeriod() {
		return hasRoomFreeInPeriod;
	}

	public void setHasRoomFreeInPeriod(String hasRoomFreeInPeriod) {
		this.hasRoomFreeInPeriod = hasRoomFreeInPeriod;
	}

	public Double getCleaning() {
		return cleaning;
	}

	public void setCleaning(Double cleaning) {
		this.cleaning = cleaning;
	}

	public Double getDeposit() {
		return deposit;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getUnitSymbol() {
		return unitSymbol;
	}

	public void setUnitSymbol(String unitSymbol) {
		this.unitSymbol = unitSymbol;
	}

	public Double getSubTotalOffline() {
		return subTotalOffline;
	}

	public void setSubTotalOffline(Double subTotalOffline) {
		this.subTotalOffline = subTotalOffline;
	}

}
