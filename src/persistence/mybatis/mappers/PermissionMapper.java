package persistence.mybatis.mappers;

import java.util.List;
import java.util.Map;

import model.Permission;

public interface PermissionMapper {
	public Integer insertPermission(Permission permission);

	public Integer updatePermission(Permission permission);

	public Integer deletePermission(Integer id);

	public List<Permission> findAll();

	public List<Permission> search(Map map);

	public Permission findPermissionById(Integer id);

	public List<Permission> searchPermissionByName(Map map);

	public Permission findPermissionByName(Map map);
}