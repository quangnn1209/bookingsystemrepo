package persistence.mybatis.mappers;

import java.util.List;
import java.util.Map;

import model.Role;

public interface RoleMapper {
	public Integer insertRole(Role role);

	public Integer updateRole(Role role);

	public Integer deleteRole(Integer id);

	public List<Role> findAll();

	public List<Role> search(Map map);

	public Role findRoleById(Integer id);

	public List<Role> searchRoleByName(Map map);

	public Role findRoleByName(Map map);
}