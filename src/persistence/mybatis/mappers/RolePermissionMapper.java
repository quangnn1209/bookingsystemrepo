package persistence.mybatis.mappers;

import java.util.List;
import java.util.Map;

public interface RolePermissionMapper {
	public Integer insert(Map map);

	public Integer delete(Integer id);

	public Integer deleteByIdRole(Integer id_role);

	public Integer deleteByIdPermission(Integer id_permission);

	public List<Map> findByIdRole(Map map);

	public List<Map> findAll();

	public List<Integer> findIdPermissionByIdRole(Integer id_role);

	public Integer findIdByIdRoleAndIdPermission(Map map);

	public Integer findPermissionIdById(Map map);
}
