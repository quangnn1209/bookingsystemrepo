package persistence.mybatis.mappers;

import java.util.List;
import java.util.Map;

public interface UserRoleMapper {
	public Integer insert(Map map);

	public Integer delete(Integer id);

	public Integer deleteByIdRole(Integer id_role);

	public Integer deleteByIdUser(Integer id_user);

	public List<Map> findByIdRole(Map map);

	public List<Map> findAll();

	public Integer findIdByIdRoleAndIdUser(Map map);

	public Integer findUserIdById(Map map);
}