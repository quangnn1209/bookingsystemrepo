/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package persistence.mybatis.mappers;

import java.util.List;
import java.util.Map;

import model.WzImage;

public interface WzImageMapper {
	public List<WzImage> findAll();

	public List<WzImage> findWzImageByWzId(Integer wzId);

	public List<WzImage> findAllWzImageByWzId(Integer wzId);

	public WzImage findWzImageById(Integer id);

	public WzImage findWzImageByImageId(Integer imageId);

	public WzImage findWzImageByIdByIdUser(Map param);

	public Integer updateWzImage(WzImage wzImage);

	public Integer insertWzImage(WzImage wzImage);

	public Integer deleteWzImage(Integer idWz);

	public Integer deleteWzImageByIdImage(Integer idImage);
}