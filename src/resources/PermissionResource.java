package resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.Permission;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.PermissionService;
import utils.CommonUtils;
import utils.I18nUtils;

import com.sun.jersey.api.NotFoundException;

@Path("/permissions/")
@Component
@Scope("prototype")
public class PermissionResource {
	@Autowired
	private PermissionService permissionService = null;
	@Autowired
	private SolrServer solrServerPermission = null;

	@PostConstruct
	public void init() {
		try {
			List<Permission> permissions = this.getPermissionService().findAll();
			if (!CommonUtils.isEmpty(permissions)) {
				this.getSolrServerPermission().addBeans(permissions);
				this.getSolrServerPermission().commit();
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("search/{start}/{rows}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Permission> search(@PathParam("start") Integer start, @PathParam("rows") Integer rows, @QueryParam("term") String term) {
		List<Permission> permissions = null;
		SolrQuery query = null;
		QueryResponse rsp = null;
		SolrDocumentList solrDocumentList = null;
		SolrDocument solrDocument = null;
		Permission aPermission = null;
		Integer id;

		if (term.trim().equals("")) {
			term = "*:*";
		}
		query = new SolrQuery();
		query.setQuery(term);

		try {
			rsp = this.getSolrServerPermission().query(query);

		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		permissions = new ArrayList<Permission>();
		if (rsp != null) {
			solrDocumentList = rsp.getResults();
			for (int i = 0; i < solrDocumentList.size(); i++) {
				solrDocument = solrDocumentList.get(i);
				id = (Integer) solrDocument.getFieldValue("id");
				aPermission = this.getPermissionService().findPermissionById(id);
				if (aPermission != null)
					permissions.add(aPermission);
			}
		}
		return this.getPermissionService().findAll();
	}

	@GET
	@Path("suggest")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> suggest(@QueryParam("term") String term) {
		SolrQuery query = null;
		QueryResponse rsp = null;
		List<String> ret = null;
		List<Count> values = null;

		query = new SolrQuery();
		query.setFacet(true);
		query.addFacetField("text");
		term = term.toLowerCase();
		query.setFacetPrefix(term);

		try {
			rsp = this.getSolrServerPermission().query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		ret = new ArrayList<String>();

		if (rsp != null) {
			values = rsp.getFacetField("text").getValues();
			if (values != null) {
				for (Count each : values) {
					// if (each.getCount() > 0) {
					ret.add(each.getName());
					// }
				}
			}
		}
		return ret;
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Permission getPermission(@PathParam("id") Integer id) {
		return this.getPermissionService().findPermissionById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Permission save(Permission permission) {
		this.getPermissionService().insertPermission(permission);
		try {
			this.getSolrServerPermission().addBean(permission);
			this.getSolrServerPermission().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		return permission;
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Permission update(Permission permission) {
		try {
			this.getPermissionService().updatePermission(permission);
			this.getSolrServerPermission().addBean(permission);
			this.getSolrServerPermission().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}
		return permission;
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer delete(@PathParam("id") Integer id) {
		// Get current user
		Integer count = 0;

		count = this.getPermissionService().deletePermission(id);
		if (count == 0) {
			throw new NotFoundException(I18nUtils.getProperty("permissionDeleteErrorAction"));
		}
		try {
			this.getSolrServerPermission().deleteById(id.toString());
			this.getSolrServerPermission().commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	public SolrServer getSolrServerPermission() {
		return solrServerPermission;
	}

	public void setSolrServerPermission(SolrServer solrServerPermission) {
		this.solrServerPermission = solrServerPermission;
	}
}