package resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.Permission;
import model.PropertyType;
import model.User;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import service.FacilityService;
import service.ImageService;
import service.PermissionService;
import service.PropertyTypeService;
import service.RolePermissionService;
import service.RoomService;
import service.StructureService;
import utils.CommonUtils;
import utils.I18nUtils;
import utils.RITMBSConstants;

import com.sun.jersey.api.NotFoundException;

@Path("/propertyTypes/")
@Component
@Scope("prototype")
public class PropertyTypeResource {
	@Autowired
	private PropertyTypeService propertyTypeService = null;
	@Autowired
	private StructureService structureService = null;
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private FacilityService facilityService = null;
	@Autowired
	private ImageService imageService = null;
	@Autowired
	private SolrServer solrServerProperty = null;
	@Autowired
	private RolePermissionService rolePermissionService = null;
	@Autowired
	private PermissionService permissionService = null;

	@PostConstruct
	public void init() {
		try {
			List<PropertyType> propertys = this.getPropertyTypeService().findAll();
			if (!CommonUtils.isEmpty(propertys)) {
				this.getSolrServerProperty().addBeans(propertys);
				this.getSolrServerProperty().commit();
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("search/{start}/{rows}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<PropertyType> search(@PathParam("start") Integer start, @PathParam("rows") Integer rows, @QueryParam("term") String term) {
		List<PropertyType> propertys = null;
		SolrQuery query = null;
		QueryResponse rsp = null;
		SolrDocumentList solrDocumentList = null;
		SolrDocument solrDocument = null;
		PropertyType aProperty = null;
		Integer id;

		if (term.trim().equals("")) {
			term = "*:*";
		}
		query = new SolrQuery();
		query.setQuery(term);
		// query.setStart(start);
		// query.setRows(rows);

		try {
			rsp = this.getSolrServerProperty().query(query);

		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		propertys = new ArrayList<PropertyType>();
		if (rsp != null) {
			solrDocumentList = rsp.getResults();
			for (int i = 0; i < solrDocumentList.size(); i++) {
				solrDocument = solrDocumentList.get(i);
				id = (Integer) solrDocument.getFieldValue("id");
				aProperty = this.getPropertyTypeService().findPropertyById(id);
				if (aProperty != null)
					propertys.add(aProperty);
			}
		}
		return propertys;
	}

	@GET
	@Path("suggest")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> suggest(@QueryParam("term") String term) {
		SolrQuery query = null;
		QueryResponse rsp = null;
		List<String> ret = null;
		List<Count> values = null;

		query = new SolrQuery();
		query.setFacet(true);
		query.addFacetField("text");
		term = term.toLowerCase();
		query.setFacetPrefix(term);

		try {
			rsp = this.getSolrServerProperty().query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		ret = new ArrayList<String>();

		if (rsp != null) {
			values = rsp.getFacetField("text").getValues();
			if (values != null) {
				for (Count each : values) {
					// if (each.getCount() > 0) {
					ret.add(each.getName());
					// }
				}
			}
		}
		return ret;
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public PropertyType getProperty(@PathParam("id") Integer id) {
		return this.getPropertyTypeService().findPropertyById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public PropertyType save(PropertyType property) {
		// Get current user
		if (this.havePermission("PropertyTypeResource.save")) {
			property.setName(property.getName().replaceAll("[^a-zA-Z0-9]+", ""));
			if (property.getName().isEmpty()) {
				return null;
			}

			// Find property type by name
			PropertyType pt = this.getPropertyTypeService().findPropertyByName(property.getName());
			if (!CommonUtils.isEmpty(pt)) {
				// Search property type by name
				List<PropertyType> propertyTypeList = this.getPropertyTypeService().searchPropertyByName(property.getName());
				if (propertyTypeList != null && !propertyTypeList.isEmpty()) {
					Integer maxIndex = 1;
					for (PropertyType pType : propertyTypeList) {
						String extension = pType.getName().replace(property.getName(), "");
						if (extension.contains("(") && extension.contains(")")) {
							Integer curIndex = new Integer(extension.substring(extension.indexOf("("), extension.length()).replace(")", "").replace("(", ""));
							if (curIndex > maxIndex) {
								maxIndex = curIndex;
							}
						}
					}

					property.setName(property.getName() + "(" + (maxIndex + 1) + ")");
				}
			}

			this.getPropertyTypeService().insertProperty(property);
			try {
				this.getSolrServerProperty().addBean(property);
				this.getSolrServerProperty().commit();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SolrServerException e) {
				e.printStackTrace();
			}
		} else {
			property.setScreenMessage(RITMBSConstants.ERROR_PREFIX + I18nUtils.getProperty("dontHavePermission"));
			// throw new
			// MessageException(I18nUtils.getProperty("dontHavePermission"));
		}
		return property;
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public PropertyType update(PropertyType property) {
		// Get current user
		if (this.havePermission("PropertyTypeResource.update")) {
			// Replace all special character
			property.setName(property.getName().replaceAll("[^a-zA-Z0-9]+", ""));

			// Find property type by name
			PropertyType pt = this.getPropertyTypeService().findPropertyByName(property.getName());
			if (!CommonUtils.isEmpty(pt)) {
				// Search property type by name
				List<PropertyType> propertyTypeList = this.getPropertyTypeService().searchPropertyByName(property.getName());
				if (!CommonUtils.isEmpty(propertyTypeList)) {
					Integer maxIndex = 1;
					for (PropertyType pType : propertyTypeList) {
						if (!pType.getId().equals(property.getId())) {
							String extension = pType.getName().replace(property.getName(), "");
							if (extension.contains("(") && extension.contains(")")) {
								Integer curIndex = new Integer(extension.replace("(", "").replace(")", ""));
								if (curIndex > maxIndex) {
									maxIndex = curIndex;
								}
							}
						}
					}
					if (maxIndex != 1)
						property.setName(property.getName() + "(" + (maxIndex + 1) + ")");
				}
			}

			try {
				this.getPropertyTypeService().updateProperty(property);
				this.getSolrServerProperty().addBean(property);
				this.getSolrServerProperty().commit();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SolrServerException e) {
				e.printStackTrace();
			} catch (Exception ex) {
			}
		} else {
			property.setScreenMessage(RITMBSConstants.ERROR_PREFIX + I18nUtils.getProperty("dontHavePermission"));
			// throw new
			// MessageException(I18nUtils.getProperty("dontHavePermission"));
		}
		return property;
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer delete(@PathParam("id") Integer id) {
		// Get current user
		Integer count = 0;
		if (this.havePermission("PropertyTypeResource.delete")) {
			count = this.getPropertyTypeService().deleteProperty(id);
			if (count == 0) {
				throw new NotFoundException(I18nUtils.getProperty("propertyDeleteErrorAction"));
			}
			try {
				this.getSolrServerProperty().deleteById(id.toString());
				this.getSolrServerProperty().commit();
			} catch (SolrServerException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			throw new NotFoundException(I18nUtils.getProperty("dontHavePermission"));
		}
		return count;
	}

	private boolean havePermission(String resource) {
		User user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);

		// Search permisison by role
		if (user.getId_role() != null) {
			List<Integer> idPermissions = this.getRolePermissionService().findIdPermissionByIdRole(user.getId_role());
			if (!CommonUtils.isEmpty(idPermissions)) {
				// Get list permisisons
				for (Integer idPermission : idPermissions) {
					Permission permission = this.getPermissionService().findPermissionById(idPermission);
					if (!CommonUtils.isEmpty(permission) && permission.getName().equals(resource)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public PropertyTypeService getPropertyTypeService() {
		return propertyTypeService;
	}

	public void setPropertyTypeService(PropertyTypeService propertyTypeService) {
		this.propertyTypeService = propertyTypeService;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public FacilityService getFacilityService() {
		return facilityService;
	}

	public void setFacilityService(FacilityService facilityService) {
		this.facilityService = facilityService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public SolrServer getSolrServerProperty() {
		return solrServerProperty;
	}

	public void setSolrServerProperty(SolrServer solrServerProperty) {
		this.solrServerProperty = solrServerProperty;
	}

	public RolePermissionService getRolePermissionService() {
		return rolePermissionService;
	}

	public void setRolePermissionService(RolePermissionService rolePermissionService) {
		this.rolePermissionService = rolePermissionService;
	}

	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}
}