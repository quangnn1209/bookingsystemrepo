package resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import model.Publishing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.PublishingService;

@Path("/publishings/")
@Component
@Scope("prototype")
public class PublishingResource {
	@Autowired
	private PublishingService publishingService;

	@GET
	@Path("checked/publishing/{idStructure}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Publishing findPublishingByIdPropertyType(@PathParam("idStructure") Integer idStructure) {
		return this.getPublishingService().findByIdStructure(idStructure);
	}

	public PublishingService getPublishingService() {
		return publishingService;
	}

	public void setPublishingService(PublishingService publishingService) {
		this.publishingService = publishingService;
	}

}
