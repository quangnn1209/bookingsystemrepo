package resources;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.PropertyTypeService;
import service.RolePermissionService;
import utils.I18nUtils;

import com.sun.jersey.api.NotFoundException;

@Path("/rolePermissions/")
@Component
@Scope("prototype")
public class RolePermissionResource {
	@Autowired
	private PropertyTypeService permissionService = null;
	@Autowired
	private RolePermissionService rolePermissionService = null;

	@GET
	@Path("checked/role/{idRole}/{offset}/{rownum}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Integer> getRolePermissions(@PathParam("idRole") Integer idRole, @PathParam("offset") Integer offset, @PathParam("rownum") Integer rownum) {
		if (idRole != null && idRole >= 0) {
			return this.getRolePermissionService().findIdPermissionByIdRole(idRole);
		}
		return null;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Map insertRolePermission(Map map) {
		Integer id_role = null;
		Integer id_permission;
		Integer id = 0;

		id_role = (Integer) map.get("idRole");
		id_permission = (Integer) ((Map) map.get("permission")).get("id");

		this.getRolePermissionService().insert(id_role, id_permission);
		map.put("id", id);
		return map;
	}

	@DELETE
	@Path("{idRole}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer deleteRolePermission(@PathParam("idRole") Integer id) {
		Integer count = 0;

		count = this.getRolePermissionService().delete(id);
		if (count == 0) {
			throw new NotFoundException(I18nUtils.getProperty("rolePermissionDeleteErrorAction"));
		}
		return count;
	}

	public PropertyTypeService getPropertyTypeService() {
		return permissionService;
	}

	public void setPropertyTypeService(PropertyTypeService permissionService) {
		this.permissionService = permissionService;
	}

	public RolePermissionService getRolePermissionService() {
		return rolePermissionService;
	}

	public void setRolePermissionService(RolePermissionService rolePermissionService) {
		this.rolePermissionService = rolePermissionService;
	}
}
