package resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.Role;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.RolePermissionService;
import service.RoleService;
import utils.CommonUtils;
import utils.I18nUtils;

import com.sun.jersey.api.NotFoundException;

@Path("/roles/")
@Component
@Scope("prototype")
public class RoleResource {
	@Autowired
	private RoleService roleService = null;
	@Autowired
	private SolrServer solrServerRole = null;
	@Autowired
	private RolePermissionService rolePermissionService = null;

	@PostConstruct
	public void init() {
		try {
			List<Role> roles = this.getRoleService().findAll();
			if (!CommonUtils.isEmpty(roles)) {
				this.getSolrServerRole().addBeans(roles);
				this.getSolrServerRole().commit();
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("search/{start}/{rows}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Role> search(@PathParam("start") Integer start, @PathParam("rows") Integer rows, @QueryParam("term") String term) {
		List<Role> roles = null;
		SolrQuery query = null;
		QueryResponse rsp = null;
		SolrDocumentList solrDocumentList = null;
		SolrDocument solrDocument = null;
		Role aRole = null;
		Integer id;

		if (term.trim().equals("")) {
			term = "*:*";
		}
		query = new SolrQuery();
		query.setQuery(term);
		// query.setStart(start);
		// query.setRows(rows);

		try {
			rsp = this.getSolrServerRole().query(query);

		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		roles = new ArrayList<Role>();
		if (rsp != null) {
			solrDocumentList = rsp.getResults();
			for (int i = 0; i < solrDocumentList.size(); i++) {
				solrDocument = solrDocumentList.get(i);
				id = (Integer) solrDocument.getFieldValue("id");
				aRole = this.getRoleService().findRoleById(id);
				if (aRole != null) {
					// Find permission of current role.
					aRole.setIdPermissions(this.getRolePermissionService().findIdPermissionByIdRole(aRole.getId()));
					roles.add(aRole);
				}

			}
		}
		return roles;
	}

	@GET
	@Path("suggest")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> suggest(@QueryParam("term") String term) {
		SolrQuery query = null;
		QueryResponse rsp = null;
		List<String> ret = null;
		List<Count> values = null;

		query = new SolrQuery();
		query.setFacet(true);
		query.addFacetField("text");
		term = term.toLowerCase();
		query.setFacetPrefix(term);

		try {
			rsp = this.getSolrServerRole().query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		ret = new ArrayList<String>();

		if (rsp != null) {
			values = rsp.getFacetField("text").getValues();
			if (values != null) {
				for (Count each : values) {
					// if (each.getCount() > 0) {
					ret.add(each.getName());
					// }
				}
			}
		}
		return ret;
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Role getRole(@PathParam("id") Integer id) {
		return this.getRoleService().findRoleById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Role save(Role role) {
		// Find role by name
		Role pt = this.getRoleService().findRoleByName(role.getName());
		if (!CommonUtils.isEmpty(pt)) {
			// Search property type by name
			List<Role> roleList = this.getRoleService().searchRoleByName(role.getName());
			if (!CommonUtils.isEmpty(roleList)) {
				Integer maxIndex = 1;
				for (Role r : roleList) {
					String extension = r.getName().replace(role.getName(), "");
					if (extension.contains("(") && extension.contains(")")) {
						Integer curIndex = new Integer(extension.substring(extension.indexOf("("), extension.length()).replace(")", "").replace("(", ""));
						if (curIndex > maxIndex) {
							maxIndex = curIndex;
						}
					}
				}

				role.setName(role.getName() + "(" + (maxIndex + 1) + ")");
			}
		}

		this.getRoleService().insertRole(role);
		try {
			this.getSolrServerRole().addBean(role);
			this.getSolrServerRole().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		role = this.getRoleService().findRoleByName(role.getName());
		if (!CommonUtils.isEmpty(role.getIdPermissions())) {
			for (Integer idPermission : role.getIdPermissions()) {
				this.getRolePermissionService().insert(role.getId(), idPermission);
			}
		}

		return role;
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Role update(Role role) {
		// Find role by name
		Role pt = this.getRoleService().findRoleByName(role.getName());
		if (!CommonUtils.isEmpty(pt)) {
			// Search property type by name
			List<Role> roleList = this.getRoleService().searchRoleByName(role.getName());
			if (!CommonUtils.isEmpty(roleList)) {
				Integer maxIndex = 1;
				for (Role r : roleList) {
					if (!r.getId().equals(role.getId())) {
						String extension = r.getName().replace(role.getName(), "");
						if (extension.contains("(") && extension.contains(")")) {
							Integer curIndex = new Integer(extension.substring(extension.indexOf("("), extension.length()).replace(")", "").replace("(", ""));
							if (curIndex > maxIndex) {
								maxIndex = curIndex;
							}
						}
					}
				}
				if (maxIndex != 1)
					role.setName(role.getName() + "(" + (maxIndex + 1) + ")");
			}
		}

		// Delete old role permission
		this.getRolePermissionService().deleteByIdRole(role.getId());
		if (!CommonUtils.isEmpty(role.getIdPermissions())) {
			for (Integer idPermission : role.getIdPermissions()) {
				this.getRolePermissionService().insert(role.getId(), idPermission);
			}
		}

		try {
			this.getRoleService().updateRole(role);
			this.getSolrServerRole().addBean(role);
			this.getSolrServerRole().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}
		return role;
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer delete(@PathParam("id") Integer id) {
		// Get current user
		Integer count = 0;

		count = this.getRoleService().deleteRole(id);
		if (count == 0) {
			throw new NotFoundException(I18nUtils.getProperty("roleDeleteErrorAction"));
		}
		// Delete role permission
		this.getRolePermissionService().deleteByIdRole(id);
		try {
			this.getSolrServerRole().deleteById(id.toString());
			this.getSolrServerRole().commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public SolrServer getSolrServerRole() {
		return solrServerRole;
	}

	public void setSolrServerRole(SolrServer solrServerRole) {
		this.solrServerRole = solrServerRole;
	}

	public RolePermissionService getRolePermissionService() {
		return rolePermissionService;
	}

	public void setRolePermissionService(RolePermissionService rolePermissionService) {
		this.rolePermissionService = rolePermissionService;
	}
}