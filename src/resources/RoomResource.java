package resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import model.Room;
import model.RoomType;
import model.Structure;
import model.User;
import model.listini.Convention;
import model.listini.RoomPriceList;
import model.listini.RoomPriceListItem;
import model.listini.Season;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.BookingService;
import service.ConventionService;
import service.FacilityService;
import service.ImageService;
import service.RoomPriceListService;
import service.RoomService;
import service.RoomTypeService;
import service.SeasonService;
import service.StructureService;
import utils.CommonUtils;
import utils.I18nUtils;
import utils.RITMBSConstants;

import com.sun.jersey.api.NotFoundException;

@Path("/rooms/")
@Component
@Scope("prototype")
public class RoomResource {
	@Autowired
	private StructureService structureService = null;
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private RoomTypeService roomTypeService = null;
	@Autowired
	private BookingService bookingService = null;
	@Autowired
	private FacilityService facilityService = null;
	@Autowired
	private ImageService imageService = null;
	@Autowired
	private SolrServer solrServerRoom = null;
	@Autowired
	private ConventionService conventionService;
	@Autowired
	private SeasonService seasonService;
	@Autowired
	private RoomPriceListService roomPriceListService;

	@PostConstruct
	public void init() {
		try {
			List<Room> rooms = this.getRoomService().findAll();
			if (!CommonUtils.isEmpty(rooms)) {
				this.getSolrServerRoom().addBeans(rooms);
				this.getSolrServerRoom().commit();
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("structure/{idStructure}/search/{start}/{rows}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Room> search(@PathParam("idStructure") Integer idStructure, @PathParam("start") Integer start, @PathParam("rows") Integer rows,
			@QueryParam("term") String term) {
		List<Room> rooms = null;
		SolrQuery query = null;
		QueryResponse rsp = null;
		SolrDocumentList solrDocumentList = null;
		SolrDocument solrDocument = null;
		Room aRoom = null;
		Integer id;

		if (term.trim().equals("")) {
			term = "*:*";
		}
		term = term + " AND id_structure:" + idStructure.toString();
		query = new SolrQuery();
		query.setQuery(term);
		query.setStart(start);
		query.setRows(rows);

		try {
			rsp = this.getSolrServerRoom().query(query);

		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		rooms = new ArrayList<Room>();
		if (rsp != null) {
			solrDocumentList = rsp.getResults();
			for (int i = 0; i < solrDocumentList.size(); i++) {
				solrDocument = solrDocumentList.get(i);
				id = (Integer) solrDocument.getFieldValue("id");
				// System.out.println("----> "+solrDocument.getFieldValues("text")+" <-----");
				aRoom = this.getRoomService().findRoomById(id);
				aRoom.setRoomType(this.getRoomTypeService().findRoomTypeById(aRoom.getId_roomType()));
				aRoom.setAvailable(RITMBSConstants.AVAILABLEFORBOOKING.equals(aRoom.getAvailableforbooking()));
				rooms.add(aRoom);
			}
		}
		return rooms;
	}

	@GET
	@Path("structure/{idStructure}/suggest")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> suggest(@PathParam("idStructure") Integer idStructure, @QueryParam("term") String term) {
		SolrQuery query = null;
		QueryResponse rsp = null;
		List<String> ret = null;
		List<Count> values = null;

		query = new SolrQuery();
		query.setFacet(true);
		query.setQuery("*:* AND id_structure:" + idStructure.toString());

		query.addFacetField("text");
		term = term.toLowerCase();
		query.setFacetPrefix(term);

		try {
			rsp = this.getSolrServerRoom().query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		ret = new ArrayList<String>();

		if (rsp != null) {
			values = rsp.getFacetField("text").getValues();
			if (values != null) {
				for (Count each : values) {
					if (each.getCount() > 0) {
						ret.add(each.getName());
					}
				}
			}
		}
		return ret;
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Room getRoom(@PathParam("id") Integer id) {
		Room ret = null;

		ret = this.getRoomService().findRoomById(id);
		ret.setAvailable(RITMBSConstants.AVAILABLEFORBOOKING.equals(ret.getAvailableforbooking()));
		ret.setRoomType(this.getRoomTypeService().findRoomTypeById(ret.getId_roomType()));
		return ret;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Room save(Room room, @Context HttpServletRequest request) {
		// get user from session
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(RITMBSConstants.SESSION_USER);

		// Search structure
		Structure structure = this.getStructureService().findStructureById(user.getStructure().getId());
		if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVENOROOMAVAILABLE)) {
			room.setAvailableforbooking(RITMBSConstants.NOTAVAILABLEFORBOOKING);
		}

		Integer idRoomType = 0;

		idRoomType = room.getId_roomType();
		// if (idRoomType != null) {
		try {
			room.setRoomType(this.getRoomTypeService().findRoomTypeById(idRoomType));
			this.getRoomService().insertRoom(room);
			this.getSolrServerRoom().addBean(room);
			this.getSolrServerRoom().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return room;
	}

	@POST
	@Path("{idStructure}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Room saveInWizard(Room room, @PathParam("idStructure") Integer idStructure) {

		// Search structure
		Structure structure = this.getStructureService().findStructureById(idStructure);
		if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVENOROOMAVAILABLE)) {
			room.setAvailableforbooking(RITMBSConstants.NOTAVAILABLEFORBOOKING);
		}
		if (room.getAvailableforbooking() == null) {
			room.setAvailableforbooking(RITMBSConstants.AVAILABLEFORBOOKING);
		}
		Integer idRoomType = room.getId_roomType();
		RoomType roomType = this.getRoomTypeService().findRoomTypeById(idRoomType);
		try {
			room.setRoomType(roomType);
			this.getRoomService().insertRoom(room);
			this.getSolrServerRoom().addBean(room);
			this.getSolrServerRoom().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Initial room price list object
		Convention convention = this.getConventionService().findConventionsByIdStructure(idStructure).get(0);
		Season season = this.getSeasonService().findSeasonsByIdStructure(idStructure).get(0);

		// Get room price list info
		RoomPriceList roomPriceList = this.getRoomPriceListService().findRoomPriceListByIdStructureAndIdSeasonAndIdRoomTypeAndIdConvention(structure.getId(),
				season.getId(), roomType.getId(), convention.getId());

		if (roomPriceList != null) {
			for (RoomPriceListItem rpli : roomPriceList.getItems()) {
				// Update room price list item
				// if (rpli.getNumGuests().equals(roomType.getMaxGuests())) {
				rpli.setPriceMonday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// mon
				rpli.setPriceTuesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// tue
				rpli.setPriceWednesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// wed
				rpli.setPriceThursday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// thu
				rpli.setPriceFriday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// fri
				rpli.setPriceSaturday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sat
				rpli.setPriceSunday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sun

				this.getRoomPriceListService().updateRoomPriceListItem(rpli);
				// }
			}
		} else {
			// Initial room price list object
			RoomPriceList newRoomPriceList = new RoomPriceList();
			newRoomPriceList.setId_structure(idStructure);
			newRoomPriceList.setSeason(season);
			newRoomPriceList.setId_season(season.getId());
			newRoomPriceList.setRoomType(roomType);
			newRoomPriceList.setId_roomType(roomType.getId());
			newRoomPriceList.setConvention(convention);
			newRoomPriceList.setId_convention(convention.getId());

			// Initial room price list item
			List<RoomPriceListItem> roomItems = new ArrayList<RoomPriceListItem>();
			RoomPriceListItem newRoomPriceListItem = null;
			for (int i = 1; i <= roomType.getMaxGuests(); i++) {
				newRoomPriceListItem = new RoomPriceListItem();
				newRoomPriceListItem.setNumGuests(i);
				newRoomPriceListItem.setPriceMonday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// mon
				newRoomPriceListItem.setPriceTuesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// tue
				newRoomPriceListItem.setPriceWednesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// wed
				newRoomPriceListItem.setPriceThursday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// thu
				newRoomPriceListItem.setPriceFriday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// fri
				newRoomPriceListItem.setPriceSaturday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sat
				newRoomPriceListItem.setPriceSunday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sun
				roomItems.add(newRoomPriceListItem);
			}
			newRoomPriceList.setItems(roomItems);
			this.getRoomPriceListService().insertRoomPriceList(newRoomPriceList);
		}

		return room;
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Room update(Room room, @Context HttpServletRequest request) {
		// get user from session
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(RITMBSConstants.SESSION_USER);

		// Search structure
		Structure structure = this.getStructureService().findStructureById(user.getStructure().getId());
		if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVENOROOMAVAILABLE)) {
			room.setAvailableforbooking(RITMBSConstants.NOTAVAILABLEFORBOOKING);
		}
		if (room.getAvailableforbooking() == null) {
			room.setAvailableforbooking(RITMBSConstants.AVAILABLEFORBOOKING);
		}
		try {
			this.getRoomService().updateRoom(room);
			this.getSolrServerRoom().addBean(room);
			this.getSolrServerRoom().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}
		room.setRoomType(this.getRoomTypeService().findRoomTypeById(room.getId_roomType()));
		room.setAvailable(RITMBSConstants.AVAILABLEFORBOOKING.equals(room.getAvailableforbooking()));
		return room;
	}

	@POST
	@Path("{id}/{idStructure}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Room updateRoomInWizard(Room room, @PathParam("idStructure") Integer idStructure) {
		// Search structure
		Structure structure = this.getStructureService().findStructureById(idStructure);
		if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVENOROOMAVAILABLE)) {
			room.setAvailableforbooking(RITMBSConstants.NOTAVAILABLEFORBOOKING);
		}
		if (room.getAvailableforbooking() == null) {
			room.setAvailableforbooking(RITMBSConstants.AVAILABLEFORBOOKING);
		}
		try {
			this.getRoomService().updateRoom(room);
			this.getSolrServerRoom().addBean(room);
			this.getSolrServerRoom().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}

		RoomType roomType = this.getRoomTypeService().findRoomTypeById(room.getId_roomType());
		room.setRoomType(roomType);
		room.setAvailable(RITMBSConstants.AVAILABLEFORBOOKING.equals(room.getAvailableforbooking()));

		// Initial room price list object
		Convention convention = this.getConventionService().findConventionsByIdStructure(idStructure).get(0);
		Season season = this.getSeasonService().findSeasonsByIdStructure(idStructure).get(0);

		// Get room price list info
		RoomPriceList roomPriceList = this.getRoomPriceListService().findRoomPriceListByIdStructureAndIdSeasonAndIdRoomTypeAndIdConvention(structure.getId(),
				season.getId(), roomType.getId(), convention.getId());

		if (roomPriceList != null) {
			for (RoomPriceListItem rpli : roomPriceList.getItems()) {
				// Update room price list item
				// if (rpli.getNumGuests().equals(roomType.getMaxGuests())) {
				rpli.setPriceMonday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// mon
				rpli.setPriceTuesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// tue
				rpli.setPriceWednesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// wed
				rpli.setPriceThursday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// thu
				rpli.setPriceFriday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// fri
				rpli.setPriceSaturday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sat
				rpli.setPriceSunday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sun

				this.getRoomPriceListService().updateRoomPriceListItem(rpli);
				// }
			}
		} else {
			// Initial room price list object
			RoomPriceList newRoomPriceList = new RoomPriceList();
			newRoomPriceList.setId_structure(idStructure);
			newRoomPriceList.setSeason(season);
			newRoomPriceList.setId_season(season.getId());
			newRoomPriceList.setRoomType(roomType);
			newRoomPriceList.setId_roomType(roomType.getId());
			newRoomPriceList.setConvention(convention);
			newRoomPriceList.setId_convention(convention.getId());

			// Initial room price list item
			List<RoomPriceListItem> roomItems = new ArrayList<RoomPriceListItem>();
			RoomPriceListItem newRoomPriceListItem = null;
			for (int i = 1; i <= roomType.getMaxGuests(); i++) {
				newRoomPriceListItem = new RoomPriceListItem();
				newRoomPriceListItem.setNumGuests(i);
				newRoomPriceListItem.setPriceMonday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// mon
				newRoomPriceListItem.setPriceTuesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// tue
				newRoomPriceListItem.setPriceWednesday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// wed
				newRoomPriceListItem.setPriceThursday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// thu
				newRoomPriceListItem.setPriceFriday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// fri
				newRoomPriceListItem.setPriceSaturday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sat
				newRoomPriceListItem.setPriceSunday(Double.parseDouble(CommonUtils.formatConcurrency(room.getWeeklyPrice() / 7)));// sun
				roomItems.add(newRoomPriceListItem);
			}
			newRoomPriceList.setItems(roomItems);
			this.getRoomPriceListService().insertRoomPriceList(newRoomPriceList);
		}

		return room;
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer delete(@PathParam("id") Integer id) {
		Integer count = 0;

		if (this.getBookingService().countBookingsByIdRoom(id) > 0) {
			throw new NotFoundException(I18nUtils.getProperty("roomDeleteWithBookingError"));
		}
		count = this.getRoomService().deleteRoom(id);
		if (count == 0) {
			throw new NotFoundException(I18nUtils.getProperty("roomDeleteErrorAction"));
		}
		try {
			this.getSolrServerRoom().deleteById(id.toString());
			this.getSolrServerRoom().commit();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public RoomTypeService getRoomTypeService() {
		return roomTypeService;
	}

	public void setRoomTypeService(RoomTypeService roomTypeService) {
		this.roomTypeService = roomTypeService;
	}

	public BookingService getBookingService() {
		return bookingService;
	}

	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}

	public FacilityService getFacilityService() {
		return facilityService;
	}

	public void setFacilityService(FacilityService facilityService) {
		this.facilityService = facilityService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public SolrServer getSolrServerRoom() {
		return solrServerRoom;
	}

	public void setSolrServerRoom(SolrServer solrServerRoom) {
		this.solrServerRoom = solrServerRoom;
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public ConventionService getConventionService() {
		return conventionService;
	}

	public void setConventionService(ConventionService conventionService) {
		this.conventionService = conventionService;
	}

	public SeasonService getSeasonService() {
		return seasonService;
	}

	public void setSeasonService(SeasonService seasonService) {
		this.seasonService = seasonService;
	}

	public RoomPriceListService getRoomPriceListService() {
		return roomPriceListService;
	}

	public void setRoomPriceListService(RoomPriceListService roomPriceListService) {
		this.roomPriceListService = roomPriceListService;
	}

}