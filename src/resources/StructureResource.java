package resources;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import model.File;
import model.Publishing;
import model.Room;
import model.Structure;
import model.User;

import org.apache.commons.io.IOUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import service.FacilityService;
import service.FileService;
import service.ImageService;
import service.PublishingService;
import service.RoomService;
import service.StructurePropertyTypeService;
import service.StructureService;
import service.UserService;
import utils.CommonUtils;
import utils.I18nUtils;
import utils.RITMBSConstants;

import com.sun.jersey.api.MessageException;

@Path("/structures/")
@Component
@Scope("prototype")
public class StructureResource {
	@Autowired
	private RoomService roomService = null;
	@Autowired
	private StructureService structureService = null;
	@Autowired
	private FacilityService facilityService = null;
	@Autowired
	private ImageService imageService = null;
	@Autowired
	private SolrServer solrServerStructure = null;
	@Autowired
	private ApplicationContext applicationContext = null;
	@Autowired
	private StructurePropertyTypeService structurePropertyTypeService = null;
	@Autowired
	private PublishingService publishingService;
	@Autowired
	private UserService userService;
	@Autowired
	private FileService fileService;

	@PostConstruct
	public void init() {
		try {
			List<Structure> structures = this.getStructureService().findAll();
			if (!CommonUtils.isEmpty(structures)) {
				this.getSolrServerStructure().addBeans(structures);
				this.getSolrServerStructure().commit();
			}
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("search/{start}/{rows}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Structure> search(@PathParam("start") Integer start, @PathParam("rows") Integer rows, @QueryParam("term") String term) {
		List<Structure> structures = null;
		SolrQuery query = null;
		QueryResponse rsp = null;
		SolrDocumentList solrDocumentList = null;
		SolrDocument solrDocument = null;
		Structure aStructure = null;
		Integer id = 0;
		User user = null;

		if (term.trim().equals("")) {
			term = "*:*";
		}

		// If admin then load all structure
		user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);
		if (RITMBSConstants.ROLE_USER_ID.equals(user.getId_role()))
			term = term + " AND id_user:" + user.getId();
		query = new SolrQuery();
		query.setQuery(term);
		query.setStart(start);
		query.setRows(rows);

		try {
			rsp = this.getSolrServerStructure().query(query);

		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		structures = new ArrayList<Structure>();
		if (rsp != null) {
			solrDocumentList = rsp.getResults();
			for (int i = 0; i < solrDocumentList.size(); i++) {
				solrDocument = solrDocumentList.get(i);
				id = (Integer) solrDocument.getFieldValue("id");
				aStructure = this.getStructureService().findStructureById(id);
				if (aStructure != null) {
					// Find publishing by id structure
					Publishing publishing = this.getPublishingService().findByIdStructure(aStructure.getId());
					aStructure.setPublished(publishing == null ? RITMBSConstants.UN_PUBLISH : publishing.getStatus());
					if (RITMBSConstants.PUBLISHED.equals(aStructure.getPublished())) {
						aStructure.setPublishedForList(true);
					} else {
						aStructure.setPublishedForList(false);
					}

					// Search user by user id
					User u = this.getUserService().findUserById(aStructure.getId_user());
					if (!CommonUtils.isEmpty(u)) {
						aStructure.setUserBelong(u.getEmail());
					}

					structures.add(aStructure);
				}
			}
		}
		return structures;
	}

	@GET
	@Path("suggest")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> suggest(@QueryParam("term") String term) {
		SolrQuery query = null;
		QueryResponse rsp = null;
		List<String> ret = null;
		List<Count> values = null;
		User user = null;

		user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);
		query = new SolrQuery();
		if (RITMBSConstants.ROLE_USER_ID.equals(user.getId_role()))
			query.setQuery("*:* AND id_user:" + user.getId());
		query.setFacet(true);
		query.addFacetField("text");
		term = term.toLowerCase();
		query.setFacetPrefix(term);

		try {
			rsp = this.getSolrServerStructure().query(query);
		} catch (SolrServerException e) {
			e.printStackTrace();
		}
		ret = new ArrayList<String>();

		if (rsp != null) {
			values = rsp.getFacetField("text").getValues();
			if (values != null) {
				for (Count each : values) {
					if (each.getCount() > 0) {
						ret.add(each.getName());
					}
				}
			}
		}
		return ret;
	}

	@GET
	@Path("resetStructure/{structureId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<String> resetStructure(@PathParam("structureId") String structureId) {
		// Get user from session
		User user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);

		// Get structure by structure id
		// Because admin can control structure of other users.
		Structure structure = this.getStructureService().findStructureByIdByIdUser(new Integer(structureId), user.getId());
		if (!CommonUtils.isEmpty(structure)) {
			// Set other structures to disable
			// Get all structure of current user have status isEnable = '1'
			List<Structure> structures = this.getStructureService().findStructureByIdUserByStatus(user.getId(), RITMBSConstants.STRUCTURE_IS_ENABLE);
			if (structures != null && !structures.isEmpty()) {
				for (Structure s : structures) {
					// Update status to disable
					s.setIsEnable(RITMBSConstants.STRUCTURE_IS_DISABLE);
					this.updateStructure(s);
				}
			}

			// Set current structure to enable.
			structure.setIsEnable(RITMBSConstants.STRUCTURE_IS_ENABLE);

			// Update structure
			this.updateStructure(structure);

			// Set structure to user
			user.setStructure(structure);

			// set User to session
			RequestContextHolder.currentRequestAttributes().setAttribute(RITMBSConstants.SESSION_USER, user, RequestAttributes.SCOPE_SESSION);
		} else {
			structure = this.getStructureService().findStructureById(new Integer(structureId));
			if (!CommonUtils.isEmpty(structure)) {
				user.setStructure(structure);
				// set User to session
				RequestContextHolder.currentRequestAttributes().setAttribute(RITMBSConstants.SESSION_USER, user, RequestAttributes.SCOPE_SESSION);
			}
		}
		return new ArrayList<String>();
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Structure getStructure(@PathParam("id") Integer idStructure) {
		Structure ret = this.getStructureService().findStructureById(idStructure);

		// Set publishing status
		if (!CommonUtils.isEmpty(ret)) {
			Publishing publishing = this.getPublishingService().findByIdStructure(ret.getId());
			ret.setPublished(publishing == null ? RITMBSConstants.UN_PUBLISH : publishing.getStatus());
		}
		return ret;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Structure save(Structure structure, @QueryParam("id_propertyType") String id_propertyType) {
		// Get current user
		User user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);
		structure.setId_user(user.getId());

		// if this structure set is enable then other structures will be set
		// disable.
		if (structure.getIsEnable().equals(RITMBSConstants.STRUCTURE_IS_ENABLE)) {
			// Get all structure of current user have status isEnable = '1'
			List<Structure> structures = this.getStructureService().findStructureByIdUserByStatus(user.getId(), RITMBSConstants.STRUCTURE_IS_ENABLE);
			if (structures != null && !structures.isEmpty()) {
				for (Structure s : structures) {
					// Update status to disable
					s.setIsEnable(RITMBSConstants.STRUCTURE_IS_DISABLE);
					this.updateStructure(s);
				}
			}
		}

		// Save static map
		if (structure.getAddress() != null && !structure.getAddress().isEmpty() && structure.getCity() != null && !structure.getCity().isEmpty())
			insertStaticMap(structure);

		// Insert structure
		this.getStructureService().insertStructure(structure);

		try {
			this.getSolrServerStructure().addBean(structure);
			this.getSolrServerStructure().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		}

		// Get structure id
		Integer idStructureLatest = structure.getId();

		// Create table structure property type.
		for (String id : id_propertyType.split(",")) {
			if (!id.isEmpty() && !"undefined".equals(id_propertyType))
				this.getStructurePropertyTypeService().insert(idStructureLatest, new Integer(id));
		}

		// Set publishing info
		Publishing publishing = new Publishing();
		publishing.setId_structure(idStructureLatest);
		publishing.setId_user_owner(user.getId());
		publishing.setStatus(RITMBSConstants.UN_PUBLISH);

		// Publishing structure
		if (RITMBSConstants.PUBLISHED.equals(structure.getPublished()) && RITMBSConstants.ROLE_ADMIN_ID.equals(user.getId_role())) {
			publishing.setDate_modify(new Date());
			publishing.setId_user_publisher(user.getId());
			publishing.setStatus(RITMBSConstants.PUBLISHED);
		}

		// Insert publishing to database.
		this.getPublishingService().insertPublishing(publishing);

		// Set publishing for list
		if (RITMBSConstants.PUBLISHED.equals(structure.getPublished())) {
			structure.setPublishedForList(true);
		} else {
			structure.setPublishedForList(false);
		}

		return structure;
	}

	@PUT
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Structure update(Structure structure, @QueryParam("id_propertyType") String id_propertyType) {
		// Get current user
		User user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(RITMBSConstants.SESSION_USER, RequestAttributes.SCOPE_SESSION);

		// Set Have Room Available
		if (structure.getHaveroomavailable() == null) {
			structure.setHaveroomavailable(RITMBSConstants.HAVEROOMAVAILABLE);
		}

		// if this structure set is enable then other structures will be set
		// disable.
		if (structure.getIsEnable().equals(RITMBSConstants.STRUCTURE_IS_ENABLE)) {
			// Get all structure of current user have status isEnable = '1'
			List<Structure> structures = this.getStructureService().findStructureByIdUserByStatus(structure.getId_user(), RITMBSConstants.STRUCTURE_IS_ENABLE);
			if (structures != null && !structures.isEmpty()) {
				for (Structure s : structures) {
					if (!s.getId().equals(structure.getId())) {
						// Update status to disable
						s.setIsEnable(RITMBSConstants.STRUCTURE_IS_DISABLE);
						this.updateStructure(s);
					}
				}
			}

			// Detect if structure changed address
			Structure oldStruct = this.getStructureService().findStructureById(structure.getId());
			if (oldStruct != null && oldStruct.compareTo(structure) == 0) {
				// Remove file
				this.getFileService().delete(oldStruct.getId_static_map());

				// Update static map
				this.insertStaticMap(structure);
			}
			this.updateStructure(structure);
		} else if (structure.getIsEnable().equals(RITMBSConstants.STRUCTURE_IS_DISABLE)) {
			// Get all structure of current user have status isEnable = '1'
			// List<Structure> structures =
			// this.getStructureService().findStructureByIdUserByStatus(structure.getId_user(),
			// RITMBSConstants.STRUCTURE_IS_ENABLE);
			// if (structures == null || structures.isEmpty()) {
			// throw new
			// MessageException(I18nUtils.getProperty("cantDisableStructure"));
			// } else {
			// for (Structure s : structures) {
			// // If this structure enable in database
			// if (s.getId().equals(structure.getId())) {
			// throw new
			// MessageException(I18nUtils.getProperty("cantDisableStructure"));
			// }
			// }

			// Detect if structure changed address
			Structure oldStruct = this.getStructureService().findStructureById(structure.getId());
			if (oldStruct != null && oldStruct.compareTo(structure) == 0) {
				// Remove file
				this.getFileService().delete(oldStruct.getId_static_map());

				// Update static map
				this.insertStaticMap(structure);
			}
			this.updateStructure(structure);
			// }
		}

		// Remove old relation
		this.getStructurePropertyTypeService().deleteByIdStructure(structure.getId());

		// Create table structure property type.
		for (String id : id_propertyType.split(",")) {
			if (!id.isEmpty() && !"undefined".equals(id_propertyType))
				this.getStructurePropertyTypeService().insert(structure.getId(), new Integer(id));
		}

		// Search room list of current structure.
		if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVENOROOMAVAILABLE)
				|| structure.getHaveroomavailable().equals(RITMBSConstants.AUTHORIZED_BOOKING)) {
			List<Room> roomList = this.getRoomService().findRoomsByIdStructure(structure.getId());
			if (!CommonUtils.isEmpty(roomList)) {
				for (Room r : roomList) {
					// Change to not available, only update the room that
					// available
					if (r.getAvailableforbooking().equals(RITMBSConstants.AVAILABLEFORBOOKING)) {
						r.setAvailableforbooking(RITMBSConstants.NOTAVAILABLEFORBOOKING);

						// Update room.
						try {
							this.getRoomService().updateRoom(r);
							this.getSolrServerStructure().addBean(r);
							this.getSolrServerStructure().commit();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (SolrServerException e) {
							e.printStackTrace();
						} catch (Exception ex) {
						}
					}
				}
			}
		} else if (structure.getHaveroomavailable().equals(RITMBSConstants.HAVEROOMAVAILABLE)) {
			List<Room> roomList = this.getRoomService().findRoomsByIdStructure(structure.getId());
			if (!CommonUtils.isEmpty(roomList)) {
				for (Room r : roomList) {
					// Change to available, only update the room that not
					// available
					if (r.getAvailableforbooking().equals(RITMBSConstants.NOTAVAILABLEFORBOOKING)) {
						r.setAvailableforbooking(RITMBSConstants.AVAILABLEFORBOOKING);

						// Update room.
						try {
							this.getRoomService().updateRoom(r);
							this.getSolrServerStructure().addBean(r);
							this.getSolrServerStructure().commit();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (SolrServerException e) {
							e.printStackTrace();
						} catch (Exception ex) {
						}
					}
				}
			}
		}

		// Set publishing status
		Publishing publishing = this.getPublishingService().findByIdStructure(structure.getId());
		if (CommonUtils.isEmpty(publishing)) {
			// Initial
			publishing = new Publishing();
			publishing.setId_structure(structure.getId());
			publishing.setId_user_owner(user.getId());
			publishing.setStatus(RITMBSConstants.UN_PUBLISH);
			// Publishing structure
			if (RITMBSConstants.PUBLISHED.equals(structure.getPublished()) && RITMBSConstants.ROLE_ADMIN_ID.equals(user.getId_role())) {
				publishing.setDate_modify(new Date());
				publishing.setId_user_publisher(user.getId());
				publishing.setStatus(RITMBSConstants.PUBLISHED);
			}

			// Insert publishing to database.
			this.getPublishingService().insertPublishing(publishing);
		} else {
			// Publishing structure
			if (RITMBSConstants.ROLE_ADMIN_ID.equals(user.getId_role())) {
				publishing.setDate_modify(new Date());

				if (RITMBSConstants.PUBLISHED.equals(structure.getPublished())) {
					publishing.setId_user_publisher(user.getId());
					publishing.setStatus(RITMBSConstants.PUBLISHED);
				} else {
					publishing.setStatus(RITMBSConstants.UN_PUBLISH);
				}
				// Update publishing
				this.getPublishingService().updatePublishing(publishing);
			}
		}

		// Set publishing for list
		if (RITMBSConstants.PUBLISHED.equals(structure.getPublished())) {
			structure.setPublishedForList(true);
		} else {
			structure.setPublishedForList(false);
		}

		return structure;
	}

	@DELETE
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer delete(@PathParam("id") Integer id) {
		Integer count = 0;

		Structure structure = this.getStructureService().findStructureById(id);
		if (structure != null) {
			structure.setIsEnable(RITMBSConstants.STRUCTURE_IS_DELETED);
			this.updateStructure(structure);
			count = 1;
		}

		return count;
	}

	private void updateStructure(Structure s) {
		try {
			this.getStructureService().updateStructure(s);
			this.getSolrServerStructure().addBean(s);
			this.getSolrServerStructure().commit();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SolrServerException e) {
			e.printStackTrace();
		} catch (Exception ex) {
		}
	}

	private void insertStaticMap(Structure structure) {
		// Save static map
		if (!structure.getAddress().isEmpty() && !structure.getCity().isEmpty() && !structure.getCountry().isEmpty()) {
			// Build url
			String urlStr = "http://maps.google.com/maps/api/staticmap?size=512x512&maptype=roadmap&format=JPG&center=";
			urlStr += structure.getAddress();
			urlStr += ",";
			urlStr += structure.getCity();
			urlStr += ",";
			urlStr += structure.getCountry();
			urlStr += "&markers=";
			urlStr += structure.getAddress();
			urlStr += ",";
			urlStr += structure.getCity();
			urlStr += ",";
			urlStr += structure.getCountry();
			urlStr = urlStr.replaceAll(" ", "%20");

			// Open url
			URL url;
			try {
				url = new URL(urlStr);
				URLConnection connection = url.openConnection();
				connection.setRequestProperty("User-Agent", "Java web application");

				// Create file model
				File fileModel = new File();
				fileModel.setName(structure.getName() + "_static_map.JPG");

				try {
					fileModel.setData(IOUtils.toByteArray(connection.getInputStream()));
				} catch (IOException e) {
					e.printStackTrace();
				}

				// Insert file
				// set file id to structure
				structure.setId_static_map(this.getFileService().insert(fileModel));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public StructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(StructureService structureService) {
		this.structureService = structureService;
	}

	public FacilityService getFacilityService() {
		return facilityService;
	}

	public void setFacilityService(FacilityService facilityService) {
		this.facilityService = facilityService;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public SolrServer getSolrServerStructure() {
		return solrServerStructure;
	}

	public void setSolrServerStructure(SolrServer solrServerStructure) {
		this.solrServerStructure = solrServerStructure;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public StructurePropertyTypeService getStructurePropertyTypeService() {
		return structurePropertyTypeService;
	}

	public void setStructurePropertyTypeService(StructurePropertyTypeService structurePropertyTypeService) {
		this.structurePropertyTypeService = structurePropertyTypeService;
	}

	public RoomService getRoomService() {
		return roomService;
	}

	public void setRoomService(RoomService roomService) {
		this.roomService = roomService;
	}

	public PublishingService getPublishingService() {
		return publishingService;
	}

	public void setPublishingService(PublishingService publishingService) {
		this.publishingService = publishingService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public FileService getFileService() {
		return fileService;
	}

	public void setFileService(FileService fileService) {
		this.fileService = fileService;
	}
}