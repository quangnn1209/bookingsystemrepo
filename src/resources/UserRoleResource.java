package resources;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import service.UserRoleService;
import service.UserService;
import utils.I18nUtils;

import com.sun.jersey.api.NotFoundException;

@Path("/userRoles/")
@Component
@Scope("prototype")
public class UserRoleResource {
	@Autowired
	private UserService userService = null;
	@Autowired
	private UserRoleService userRoleService = null;

	@GET
	@Path("checked/role/{idRole}/{offset}/{rownum}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Map> getUserRoles(@PathParam("idRole") Integer idRole, @PathParam("offset") Integer offset, @PathParam("rownum") Integer rownum) {
		if (idRole != null && idRole > 0) {
			return this.getUserRoleService().findByIdRole(idRole, offset, rownum);
		} else {
			return this.getUserRoleService().findAll();
		}
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Map insertUserRole(Map map) {
		Integer id_role = null;
		Integer id_user;
		Integer id = 0;

		id_role = (Integer) map.get("idRole");
		id_user = (Integer) ((Map) map.get("user")).get("id");

		this.getUserRoleService().insert(id_role, id_user);
		map.put("id", id);
		return map;
	}

	@DELETE
	@Path("{idRole}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Integer deleteUserRole(@PathParam("idRole") Integer id) {
		Integer count = 0;

		count = this.getUserRoleService().delete(id);
		if (count == 0) {
			throw new NotFoundException(I18nUtils.getProperty("userRoleDeleteErrorAction"));
		}
		return count;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}
}
