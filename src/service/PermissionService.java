package service;

import java.util.List;

import model.Permission;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface PermissionService {
	public Integer insertPermission(Permission permission);

	public Integer updatePermission(Permission permission);

	public Integer deletePermission(Integer id);

	public List<Permission> findAll();

	public Permission findPermissionById(Integer id);

	public List<Permission> searchPermissionByName(String name);

	public Permission findPermissionByName(String name);
}