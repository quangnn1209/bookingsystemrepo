package service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Permission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistence.mybatis.mappers.PermissionMapper;

@Service
public class PermissionServiceImpl implements PermissionService {
	@Autowired
	private PermissionMapper permissionMapper = null;

	@Override
	public List<Permission> findAll() {
		return this.getPermissionMapper().findAll();
	}

	public PermissionMapper getPermissionMapper() {
		return permissionMapper;
	}

	public void setPermissionMapper(PermissionMapper permissionMapper) {
		this.permissionMapper = permissionMapper;
	}

	@Override
	public Permission findPermissionById(Integer id) {
		Permission property = null;

		property = this.getPermissionMapper().findPermissionById(id);
		return property;

	}

	@Override
	public Integer insertPermission(Permission property) {
		Integer ret = 0;

		ret = this.getPermissionMapper().insertPermission(property);
		return ret;
	}

	@Override
	public Integer updatePermission(Permission property) {
		Integer ret = 0;

		ret = this.getPermissionMapper().updatePermission(property);
		return ret;
	}

	@Override
	public Integer deletePermission(Integer id) {
		return this.getPermissionMapper().deletePermission(id);
	}

	@Override
	public Permission findPermissionByName(String name) {
		Map param = new HashMap();
		param.put("name", name);
		return this.getPermissionMapper().findPermissionByName(param);
	}

	@Override
	public List<Permission> searchPermissionByName(String name) {
		Map param = new HashMap();
		param.put("name", name);
		return this.getPermissionMapper().searchPermissionByName(param);
	}
}