/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface RolePermissionService {

	public Integer insert(Integer id_role, Integer id_permission);

	public List<Integer> findIdPermissionByIdRole(Integer id_role, Integer offset, Integer rownum);

	public List<Integer> findIdByIdRole(Integer id_role, Integer offset, Integer rownum);

	public List<Integer> findIdPermissionByIdRole(Integer id_role);

	public List<Map> findByIdRole(Integer id_role, Integer offset, Integer rownum);

	public List<Map> findAll();

	public Integer findPermissionIdById(Integer id);

	public Integer findIdByIdRoleAndIdPermission(Integer id_role, Integer id_permission);

	public Integer delete(Integer id);

	public Integer deleteByIdPermission(Integer id_permission);

	public Integer deleteByIdRole(Integer id_role);
}