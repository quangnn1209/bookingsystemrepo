/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistence.mybatis.mappers.RolePermissionMapper;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {
	@Autowired
	private RolePermissionMapper rolePermissionMapper = null;

	@Override
	public Integer insert(Integer id_role, Integer id_permission) {
		Map map = null;

		map = new HashMap();
		map.put("id_role", id_role);
		map.put("id_permission", id_permission);
		return this.getRolePermissionMapper().insert(map);
	}

	@Override
	public List<Integer> findIdPermissionByIdRole(Integer id_permission, Integer offset, Integer rownum) {
		List<Integer> ret = null;
		Map map = null;

		ret = new ArrayList<Integer>();
		map = new HashMap();
		map.put("id_permission", id_permission);
		map.put("offset", offset);
		map.put("rownum", rownum);
		for (Map each : this.getRolePermissionMapper().findByIdRole(map)) {
			ret.add((Integer) each.get("id_image"));
		}
		return ret;
	}

	@Override
	public List<Integer> findIdByIdRole(Integer id_role, Integer offset, Integer rownum) {
		List<Integer> ret = null;
		Map map = null;

		ret = new ArrayList<Integer>();
		map = new HashMap();
		map.put("id_role", id_role);
		map.put("offset", offset);
		map.put("rownum", rownum);
		for (Map each : this.getRolePermissionMapper().findByIdRole(map)) {
			ret.add((Integer) each.get("id"));
		}
		return ret;
	}

	@Override
	public Integer findIdByIdRoleAndIdPermission(Integer id_permission, Integer id_image) {
		Map map = null;

		map = new HashMap();
		map.put("id_permission", id_permission);
		map.put("id_image", id_image);
		return this.getRolePermissionMapper().findIdByIdRoleAndIdPermission(map);
	}

	@Override
	public Integer findPermissionIdById(Integer id) {
		Map map = new HashMap();
		map.put("id", id);
		return this.getRolePermissionMapper().findPermissionIdById(map);
	}

	@Override
	public Integer delete(Integer id) {
		return this.getRolePermissionMapper().delete(id);
	}

	@Override
	public Integer deleteByIdPermission(Integer id_image) {
		return this.getRolePermissionMapper().deleteByIdPermission(id_image);
	}

	@Override
	public Integer deleteByIdRole(Integer id_permission) {
		return this.getRolePermissionMapper().deleteByIdRole(id_permission);
	}

	@Override
	public List<Map> findByIdRole(Integer id_role, Integer offset, Integer rownum) {
		Map map = new HashMap();
		map.put("id_role", id_role);
		map.put("offset", offset);
		map.put("rownum", rownum);
		return this.getRolePermissionMapper().findByIdRole(map);
	}

	public RolePermissionMapper getRolePermissionMapper() {
		return rolePermissionMapper;
	}

	public void setRolePermissionMapper(RolePermissionMapper rolePermissionMapper) {
		this.rolePermissionMapper = rolePermissionMapper;
	}

	@Override
	public List<Map> findAll() {
		return this.getRolePermissionMapper().findAll();
	}

	@Override
	public List<Integer> findIdPermissionByIdRole(Integer id_role) {
		return this.getRolePermissionMapper().findIdPermissionByIdRole(id_role);
	}
}