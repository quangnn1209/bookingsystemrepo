package service;

import java.util.List;

import model.Role;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface RoleService {
	public Integer insertRole(Role role);

	public Integer updateRole(Role role);

	public Integer deleteRole(Integer id);

	public List<Role> findAll();

	public Role findRoleById(Integer id);

	public List<Role> searchRoleByName(String name);

	public Role findRoleByName(String name);
}