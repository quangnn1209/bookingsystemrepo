package service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistence.mybatis.mappers.RoleMapper;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleMapper roleMapper = null;

	@Override
	public List<Role> findAll() {
		return this.getRoleMapper().findAll();
	}

	public RoleMapper getRoleMapper() {
		return roleMapper;
	}

	public void setRoleMapper(RoleMapper roleMapper) {
		this.roleMapper = roleMapper;
	}

	@Override
	public Role findRoleById(Integer id) {
		Role property = null;

		property = this.getRoleMapper().findRoleById(id);
		return property;

	}

	@Override
	public Integer insertRole(Role property) {
		Integer ret = 0;

		ret = this.getRoleMapper().insertRole(property);
		return ret;
	}

	@Override
	public Integer updateRole(Role property) {
		Integer ret = 0;

		ret = this.getRoleMapper().updateRole(property);
		return ret;
	}

	@Override
	public Integer deleteRole(Integer id) {
		return this.getRoleMapper().deleteRole(id);
	}

	@Override
	public Role findRoleByName(String name) {
		Map param = new HashMap();
		param.put("name", name);
		return this.getRoleMapper().findRoleByName(param);
	}

	@Override
	public List<Role> searchRoleByName(String name) {
		Map param = new HashMap();
		param.put("name", name);
		return this.getRoleMapper().searchRoleByName(param);
	}
}