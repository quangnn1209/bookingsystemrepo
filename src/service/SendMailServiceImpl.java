/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import model.Booking;
import model.File;
import model.Structure;
import model.User;
/*import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utils.GMailAuthenticator;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.StringWriter;
import java.util.Properties;

@Service
public class SendMailServiceImpl implements SendMailService {
	@Autowired
	private StructureService structureService;
	@Autowired
	private UserService userService;
	@Autowired
	private FileService fileService;
	@Autowired
	private RoomImageService roomImageService;

	@Override

	public void sendMailGuest(Booking booking) throws Exception {
		/*Structure structure = structureService.findStructureById(booking.getId_structure());
		*//*
		 * first, get and initialize an engine
		 *//*
		VelocityEngine ve = new VelocityEngine();
		ve.init();

		VelocityContext context = new VelocityContext();
		context.put("userName", booking.getGuests().get(0).getFirstName());
		context.put("propertyInfo", structure.getName() + structure.getCity() + structure.getAddress() + booking.getRoom().getName());
		context.put("dateIn", booking.getDateIn());
		context.put("dateOut", booking.getDateOut());
		context.put("bookingAmount", booking.getRoom().getPrice());
		*//*
		 * get the Template
		 *//*
		Template t = ve.getTemplate("./src/email_guest.vm");

		*//*
		 * now render the template into a Writer, here a StringWriter
		 *//*

		StringWriter writer = new StringWriter();

		t.merge(context, writer);
		Properties prop = new Properties();
		prop.load(SendMailServiceImpl.class.getResourceAsStream("email.properties"));
		final String username = prop.getProperty("gmail.username");
		final String password = prop.getProperty("gmail.password");

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new GMailAuthenticator(username, password));

		try {
			// Set header
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(booking.getGuests().get(0).getEmail()));
			message.setSubject(prop.getProperty("email.subject"));

			// Create mail content
			Multipart multipart = new MimeMultipart("related");
			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(writer.toString(), "text/html");
			multipart.addBodyPart(htmlPart);

			BodyPart imgPart = new MimeBodyPart();

			// Loading the image
			File file = fileService.findById(roomImageService.findIdImageByIdRoom(booking.getRoom().getId(), 0, 1).get(0));
			ByteArrayDataSource rawData = new ByteArrayDataSource(file.getData());
			imgPart.setDataHandler(new DataHandler(rawData));

			// Setting the header
			imgPart.setHeader("Content-ID", "the-img-1");

			multipart.addBodyPart(imgPart);

			// attaching the multi-part to the message
			message.setContent(multipart);

			String host = "smtp.gmail.com";
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			message.saveChanges();
			Transport.send(message);
			System.out.println("Send mail success!");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}*/
	}

	@Override
	public void sendMailOwner(Booking booking) throws Exception {
/*
		Structure structure = structureService.findStructureById(booking.getId_structure());
		User user = userService.findUserById(structure.getId_user());
		*/
/*
		 * first, get and initialize an engine
		 *//*

		VelocityEngine ve = new VelocityEngine();
		ve.init();

		VelocityContext context = new VelocityContext();
		context.put("userName", user.getName());
		context.put("propertyInfo", structure.getName() + structure.getCity() + structure.getAddress() + booking.getRoom().getName());
		context.put("dateIn", booking.getDateIn());
		context.put("dateOut", booking.getDateOut());
		context.put("bookingAmount", booking.getRoom().getPrice());
		*/
/*
		 * get the Template
		 *//*

		Template t = ve.getTemplate("./src/email_owner.vm");

		*/
/*
		 * now render the template into a Writer, here a StringWriter
		 *//*


		StringWriter writer = new StringWriter();

		t.merge(context, writer);

		Properties prop = new Properties();
		prop.load(SendMailServiceImpl.class.getResourceAsStream("email.properties"));
		final String username = prop.getProperty("gmail.username");
		final String password = prop.getProperty("gmail.password");

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new GMailAuthenticator(username, password));

		try {
			// Set header
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
			message.setSubject(prop.getProperty("email.subject"));

			// Create mail content
			Multipart multipart = new MimeMultipart("related");
			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(writer.toString(), "text/html");
			multipart.addBodyPart(htmlPart);

			BodyPart imgPart = new MimeBodyPart();

			// Loading the image
			File file = fileService.findById(roomImageService.findIdImageByIdRoom(booking.getRoom().getId(), 0, 1).get(0));
			ByteArrayDataSource rawData = new ByteArrayDataSource(file.getData());
			imgPart.setDataHandler(new DataHandler(rawData));

			// Setting the header
			imgPart.setHeader("Content-ID", "the-img-1");

			multipart.addBodyPart(imgPart);

			// attaching the multi-part to the message
			message.setContent(multipart);

			String host = "smtp.gmail.com";
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			message.saveChanges();
			Transport.send(message);
			System.out.println("Send mail success!");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
*/
	}
}