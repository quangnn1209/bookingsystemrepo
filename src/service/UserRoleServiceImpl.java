/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing users and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistence.mybatis.mappers.UserRoleMapper;

@Service
public class UserRoleServiceImpl implements UserRoleService {
	@Autowired
	private UserRoleMapper userRoleMapper = null;

	@Override
	public Integer insert(Integer id_role, Integer id_user) {
		Map map = null;

		map = new HashMap();
		map.put("id_role", id_role);
		map.put("id_user", id_user);
		return this.getUserRoleMapper().insert(map);
	}

	@Override
	public List<Integer> findIdUserByIdRole(Integer id_user, Integer offset, Integer rownum) {
		List<Integer> ret = null;
		Map map = null;

		ret = new ArrayList<Integer>();
		map = new HashMap();
		map.put("id_user", id_user);
		map.put("offset", offset);
		map.put("rownum", rownum);
		for (Map each : this.getUserRoleMapper().findByIdRole(map)) {
			ret.add((Integer) each.get("id_image"));
		}
		return ret;
	}

	@Override
	public List<Integer> findIdByIdRole(Integer id_role, Integer offset, Integer rownum) {
		List<Integer> ret = null;
		Map map = null;

		ret = new ArrayList<Integer>();
		map = new HashMap();
		map.put("id_role", id_role);
		map.put("offset", offset);
		map.put("rownum", rownum);
		for (Map each : this.getUserRoleMapper().findByIdRole(map)) {
			ret.add((Integer) each.get("id"));
		}
		return ret;
	}

	@Override
	public Integer findIdByIdRoleAndIdUser(Integer id_user, Integer id_image) {
		Map map = null;

		map = new HashMap();
		map.put("id_user", id_user);
		map.put("id_image", id_image);
		return this.getUserRoleMapper().findIdByIdRoleAndIdUser(map);
	}

	@Override
	public Integer findUserIdById(Integer id) {
		Map map = new HashMap();
		map.put("id", id);
		return this.getUserRoleMapper().findUserIdById(map);
	}

	@Override
	public Integer delete(Integer id) {
		return this.getUserRoleMapper().delete(id);
	}

	@Override
	public Integer deleteByIdUser(Integer id_image) {
		return this.getUserRoleMapper().deleteByIdUser(id_image);
	}

	@Override
	public Integer deleteByIdRole(Integer id_user) {
		return this.getUserRoleMapper().deleteByIdRole(id_user);
	}

	@Override
	public List<Map> findByIdRole(Integer id_role, Integer offset, Integer rownum) {
		Map map = new HashMap();
		map.put("id_role", id_role);
		map.put("offset", offset);
		map.put("rownum", rownum);
		return this.getUserRoleMapper().findByIdRole(map);
	}

	public UserRoleMapper getUserRoleMapper() {
		return userRoleMapper;
	}

	public void setUserRoleMapper(UserRoleMapper userRoleMapper) {
		this.userRoleMapper = userRoleMapper;
	}

	@Override
	public List<Map> findAll() {
		return this.getUserRoleMapper().findAll();
	}
}