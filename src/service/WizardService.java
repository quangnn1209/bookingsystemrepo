/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import java.util.List;

import model.RoomWz;
import model.Wizard;
import model.WzImage;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface WizardService {
	public List<Wizard> findAll();

	public List<Wizard> findWizardByIdUser(Integer id_user);

	public Wizard findWizardById(Integer id);

	public Wizard findWizardByIdByIdUser(Integer id, Integer id_user);

	public Integer updateWizard(Wizard wizard);

	public Integer insertWizard(Wizard wizard);

	public Integer deleteWizard(Integer idWz);

	public Integer insertRoomWz(RoomWz model);

	public Integer deleteRoomWz(Integer idWz);

	public Integer updateRoomWz(RoomWz model);

	public List<RoomWz> findRoomWzByWzId(Integer wzId);

	public RoomWz findRoomWzById(Integer roomWzId);

	public Integer insertWzImage(Integer wzId, Integer imageId);

	public List<WzImage> findWzImageByWzId(Integer wzId);

	public List<WzImage> findAllWzImageByWzId(Integer wzId);

	public WzImage findWzImageByImageId(Integer imageId);

	public Integer deleteWzImage(Integer wzId);

	public Integer deleteWzImageByIdImage(Integer idImage);

	public Integer updateWzImage(WzImage wzImage);
}