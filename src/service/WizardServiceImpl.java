/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package service;

import java.util.List;
import java.util.Map;

import model.RoomWz;
import model.Wizard;
import model.WzImage;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import persistence.mybatis.mappers.RoomWzMapper;
import persistence.mybatis.mappers.WizardMapper;
import persistence.mybatis.mappers.WzImageMapper;

@Service
public class WizardServiceImpl implements WizardService {
	@Autowired
	private WizardMapper wizardMapper;
	@Autowired
	private RoomWzMapper roomWzMapper;
	@Autowired
	private WzImageMapper wzImageMapper;

	@Override
	public List<Wizard> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Wizard> findWizardByIdUser(Integer id_user) {
		return this.getWizardMapper().findWizardByIdUser(id_user);
	}

	@Override
	public Wizard findWizardById(Integer id) {
		return this.getWizardMapper().findWizardById(id);
	}

	@Override
	public Wizard findWizardByIdByIdUser(Integer id, Integer id_user) {
		Map param = new HashedMap();
		param.put("id", id);
		return this.getWizardMapper().findWizardByIdByIdUser(param);
	}

	@Override
	public Integer updateWizard(Wizard wizard) {
		return this.getWizardMapper().updateWizard(wizard);
	}

	@Override
	public Integer insertWizard(Wizard wizard) {
		return this.getWizardMapper().insertWizard(wizard);
	}

	public WizardMapper getWizardMapper() {
		return wizardMapper;
	}

	public void setWizardMapper(WizardMapper wizardMapper) {
		this.wizardMapper = wizardMapper;
	}

	@Override
	public Integer insertRoomWz(RoomWz model) {
		return this.getRoomWzMapper().insertRoomWz(model);
	}

	public RoomWzMapper getRoomWzMapper() {
		return roomWzMapper;
	}

	public void setRoomWzMapper(RoomWzMapper roomWzMapper) {
		this.roomWzMapper = roomWzMapper;
	}

	@Override
	public List<RoomWz> findRoomWzByWzId(Integer wzId) {
		return this.getRoomWzMapper().findRoomWzByWzId(wzId);
	}

	@Override
	public RoomWz findRoomWzById(Integer roomWzId) {
		return this.getRoomWzMapper().findRoomWzById(roomWzId);
	}

	@Override
	public Integer updateRoomWz(RoomWz model) {
		return this.getRoomWzMapper().updateRoomWz(model);
	}

	@Override
	public Integer deleteRoomWz(Integer idWz) {
		return this.getRoomWzMapper().deleteRoomWz(idWz);
	}

	@Override
	public Integer deleteWizard(Integer idWz) {
		return this.getWizardMapper().deleteWizard(idWz);
	}

	@Override
	public Integer insertWzImage(Integer wzId, Integer imageId) {
		WzImage model = new WzImage();
		model.setIdImage(imageId);
		model.setIdWizard(wzId);
		model.setStatus(1);
		return this.getWzImageMapper().insertWzImage(model);
	}

	public WzImageMapper getWzImageMapper() {
		return wzImageMapper;
	}

	public void setWzImageMapper(WzImageMapper wzImageMapper) {
		this.wzImageMapper = wzImageMapper;
	}

	@Override
	public List<WzImage> findWzImageByWzId(Integer wzId) {
		return this.getWzImageMapper().findWzImageByWzId(wzId);
	}

	@Override
	public Integer deleteWzImage(Integer wzId) {
		return this.getWzImageMapper().deleteWzImage(wzId);
	}

	@Override
	public Integer updateWzImage(WzImage wzImage) {
		return this.getWzImageMapper().updateWzImage(wzImage);
	}

	@Override
	public WzImage findWzImageByImageId(Integer imageId) {
		return this.getWzImageMapper().findWzImageByImageId(imageId);
	}

	@Override
	public List<WzImage> findAllWzImageByWzId(Integer wzId) {
		return this.getWzImageMapper().findAllWzImageByWzId(wzId);
	}

	@Override
	public Integer deleteWzImageByIdImage(Integer idImage) {
		return this.getWzImageMapper().deleteWzImageByIdImage(idImage);
	}
}