/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
package struts.interceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import service.PermissionService;
import service.RolePermissionService;
import utils.CommonUtils;
import utils.RITMBSConstants;
import model.Permission;
import model.User;
import model.UserAware;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class UserAwareInterceptor implements Interceptor {
	@Autowired
	private RolePermissionService rolePermissionService;
	@Autowired
	private PermissionService permissionService;

	@Override
	public void destroy() {
	}

	@Override
	public void init() {
	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		UserAware action = null;
		ActionProxy proxy = null;
		Map<String, Object> session = null;
		User user = null;
		boolean havePermission = false;
		if (invocation.getAction() instanceof UserAware) {
			session = invocation.getInvocationContext().getSession();
			user = (User) session.get(RITMBSConstants.SESSION_USER);
			if (user != null) {
				// Check permisison
				if (user.getId_role() != null) {
					// Search permission id by role id
					List<Integer> idPermissions = this.getRolePermissionService().findIdPermissionByIdRole(user.getId_role());
					if (!CommonUtils.isEmpty(idPermissions)) {
						// Find permission by id
						List<String> pName = new ArrayList<String>();
						for (Integer id : idPermissions) {
							Permission p = this.getPermissionService().findPermissionById(id);
							if (!CommonUtils.isEmpty(p)) {
								pName.add(p.getName());
							}
						}
						if (!CommonUtils.isEmpty(pName)) {
							proxy = invocation.getProxy();
							if (pName.contains(proxy.getActionName())) {
								action = (UserAware) invocation.getAction();
								if (user.getStructure() != null)
									action.setIdStructure(user.getStructure().getId());
								havePermission = true;
							}
						}
					}
				}
			}
			if (!havePermission)
				return "accessDenied";
		}
		return invocation.invoke();
	}

	public RolePermissionService getRolePermissionService() {
		return rolePermissionService;
	}

	public void setRolePermissionService(RolePermissionService rolePermissionService) {
		this.rolePermissionService = rolePermissionService;
	}

	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

}