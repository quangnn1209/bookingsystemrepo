package utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.imageio.ImageIO;

public class CommonUtils {
	public static boolean isEmpty(List objs) {
		if (objs != null && !objs.isEmpty()) {
			return false;
		}
		return true;
	}

	public static boolean isEmpty(Object objs) {
		if (objs != null && !objs.toString().isEmpty()) {
			return false;
		}
		return true;
	}

	public static String formatConcurrency(Double number) {
		if (isEmpty(number) || number == 0) {
			return "0.00";
		}

		DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance(Locale.US);
		formatter.applyPattern(RITMBSConstants.CONCURRENCY_PATTERN);
		String numberFormatted = formatter.format(number);
		return numberFormatted;
	}

	public static Date changeFormatLikeMysql(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(RITMBSConstants.MYSQL_DATE_FORMAT);
		try {
			return sdf.parse(sdf.format(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(RITMBSConstants.DEFAULT_DATE_PATTERN);
		return sdf.format(date);
	}

	public static Date convertStringToDate(String dateStr) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(RITMBSConstants.DEFAULT_DATE_PATTERN);
		return sdf.parse(dateStr);
	}

	public static Integer parseInt(String numb) {
		try {
			return Integer.parseInt(numb);
		} catch (Exception e) {
			return 0;
		}
	}

	public static Integer parseInt(String[] numb, int defaultValue) {
		try {
			return Integer.parseInt(numb[0]);
		} catch (Exception e) {
			e.printStackTrace();
			return defaultValue;
		}
	}

	public static Integer parseInt(String numb, int defaultValue) {
		try {
			return Integer.parseInt(numb);
		} catch (Exception e) {
			e.printStackTrace();
			return defaultValue;
		}
	}

	public static String randomSimplePassword() {
		Random rng = new Random();
		String characters = "qwertyuiopasdfghjklzxcvbnm";
		int length = 8;
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	/**
	 * Embeds an image watermark over a source image to produce a watermarked
	 * one.
	 * 
	 * @param watermarkImageFile
	 *            The image file used as the watermark.
	 * @param sourceImageFile
	 *            The source image file.
	 * @param destImageFile
	 *            The output image file.
	 * @throws IOException
	 */
	public static byte[] addImageWatermark(InputStream sourceImageFile, String extension) throws IOException {
		BufferedImage sourceImage = ImageIO.read(sourceImageFile);
		BufferedImage watermarkImage = ImageIO.read(CommonUtils.class.getResourceAsStream("/watermark.png"));
		int type = watermarkImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : watermarkImage.getType();
		watermarkImage = resizeImage(watermarkImage, type, (int) (sourceImage.getWidth() * 0.9),
				(int) (sourceImage.getWidth() * 0.9 * (watermarkImage.getHeight() * 1f / watermarkImage.getWidth())));
		// initializes necessary graphic properties
		Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f);
		g2d.setComposite(alphaChannel);

		// calculates the coordinate where the image is painted
		int topLeftX = (sourceImage.getWidth() - watermarkImage.getWidth()) / 2;
		int topLeftY = (sourceImage.getHeight() - watermarkImage.getHeight()) / 2;

		// paints the image watermark
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
		g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(sourceImage, extension, baos);
		g2d.dispose();
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height) {
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	}
}
