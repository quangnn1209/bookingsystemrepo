/**
 * 
 */
package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import model.User;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;

public final class I18nUtils {
	private static Logger logger = Logger.getLogger(Logger.class);

	// private constructor to avoid unnecessary instantiation of the class
	private I18nUtils() {
	}

	public static String getProperty(String key) {
		String value = null;
		// Object langObject =
		// ActionContext.getContext().getSession().get("WW_TRANS_I18N_LOCALE");
		// logger.info("Sessione trovata per " + key + " :" +
		// ActionContext.getContext().getSession() + "**"
		// +
		// ActionContext.getContext().getSession().get("WW_TRANS_I18N_LOCALE"));
		// lang = (langObject == null) ?
		// ActionContext.getContext().getLocale().getLanguage() :
		// langObject.toString();
		// Get user from session
		User user = (User) ActionContext.getContext().getSession().get(RITMBSConstants.SESSION_USER);
		ResourceBundle resourceBundle = null;
		try {
			resourceBundle = ResourceBundle.getBundle("global_" + RITMBSConstants.LANGUAGE_MAP.get(user.getId_language()));
			value = resourceBundle.getString(key);
		} catch (MissingResourceException missingResourceException) {
			// Default will be engligh.
			resourceBundle = ResourceBundle.getBundle("global_en");
			value = resourceBundle.getString(key);
			logger.info("Resource Bundle not found " + missingResourceException.getMessage());
		} catch (Exception e) {
			// Default will be engligh.
			resourceBundle = ResourceBundle.getBundle("global_en");
			value = resourceBundle.getString(key);
			logger.info("Resource Bundle not found " + e.getMessage());
		}
		return value;
	}

	public static String getDatePattern() {
		String value = null;
		Object langObject = ActionContext.getContext().getSession().get("datePattern");
		if (langObject == null) {
			SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.LONG, ActionContext.getContext().getLocale());
			value = sdf.toPattern();
		} else {
			value = langObject.toString();
		}

		return value;

	}

}
