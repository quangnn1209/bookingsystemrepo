package utils;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RITMBSConstants implements Serializable {
	public static final Character STRUCTURE_IS_ENABLE = '1';
	public static final Character STRUCTURE_IS_DISABLE = '0';
	public static final Character STRUCTURE_IS_DELETED = '2';
	public static final Character HAVEROOMAVAILABLE = '1';
	public static final Character HAVENOROOMAVAILABLE = '0';
	public static final Character AVAILABLEFORBOOKING = '1';
	public static final Character NOTAVAILABLEFORBOOKING = '0';
	public static final Character AUTHORIZED_BOOKING = '2';
	public static final Integer STRING_MAX_LENGTH = 255;
	public static final Integer STRING_IDRITMUSER_MAX_LENGTH = 26;
	public static final String CONCURRENCY_UNIT = "GBP";
	public static final String PAYMENT_METHOD = "paypal";
	public static final String PAYMENT_INTENT = "sale";
	public static final String CONCURRENCY_PATTERN = "0.00";
	public static final Integer DEFAULT_NUMBER_OF_GUEST = 1;
	public static final Date DEFAULT_BOOKING_DATE = new Date();
	public static final Integer DEFAULT_BOOKING_TERM = 7;
	public static final Map<String, String> LANGUAGE_MAP;
	static {
		Map<String, String> aMap = new HashMap<String, String>();
		aMap.put("0", "en");
		aMap.put("1", "it");
		aMap.put("2", "fr");
		aMap.put("3", "es");
		LANGUAGE_MAP = Collections.unmodifiableMap(aMap);
	}
	public static final Map<String, String> LOCALE_MAP;
	static {
		Map<String, String> aMap = new HashMap<String, String>();
		aMap.put("0", "US");
		aMap.put("1", "IT");
		aMap.put("2", "CA");
		aMap.put("3", "ES");
		LOCALE_MAP = Collections.unmodifiableMap(aMap);
	}
	public static final String SESSION_USER = "user";
	public static final String ROLE_ADMIN = "admin";
	public static final Integer ROLE_ADMIN_ID = 1;
	public static final Integer ROLE_USER_ID = 2;
	public static final String DEFAULT_LANGUAGE_ID = "0";
	public static final String PUBLISHED = "1";
	public static final String UN_PUBLISH = "0";
	public static final String ERROR_PREFIX = "ERROR:";
	public static final String SUCCESS_PREFIX = "SUCCESS:";
	public static final String MYSQL_DATE_FORMAT = "YYYY-MM-DD";
	public static final String ACTIVE_CODE = "thisconventionshouldntneverberemoved";
	public static final String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";
	public static final String DEFAULT_DATE_PATTERN_JS = "dd/MM/yy";
	public static final String ONLINE_BOOKING_SESSION = "onlineBooking";
	public static final String PAYMENT_DESCRIPTION = "This is payment for room in the moon system.";
	public static final Integer AUTHORIZED = 1;
	public static final String BOOKING_ASKED = "asked";
	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";
	public static final Integer PLANNER_DAYS = 14;
	public static final Double SERVICE_COST = 1.05; // 5%
	public static final Integer QUARTZ_SCHEDULE_MINUTES = 30;
	public static final Map<String, String> CONCURRENCY_MAP;
	static {
		Map<String, String> aMap = new HashMap<String, String>();
		aMap.put("GBP", "£");
		aMap.put("EUR", "€");
		aMap.put("VND", "₫");
		aMap.put("USD", "$");
		CONCURRENCY_MAP = Collections.unmodifiableMap(aMap);
	}
}
