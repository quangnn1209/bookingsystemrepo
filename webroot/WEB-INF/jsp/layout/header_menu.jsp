<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ page import="utils.RITMBSConstants" %>
<%@ page import="model.User" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<link rel='stylesheet' type='text/css' href='css/reset.css' />
	<link rel="stylesheet" type="text/css" href="css/colorbox.css" />
	<link rel='stylesheet' type='text/css' href='css/south-street/jquery-ui-1.8.9.custom.css' />
	<link rel='stylesheet' type='text/css' href='css/jquery.jgrowl.css' />
	<link rel="stylesheet" type='text/css' href="css/jquery.fileupload-ui.css" />
	<link rel="stylesheet" type="text/css" href="css/layout_sliding_door.css" />
  	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<title><s:text name="titleExtended"/></title>

	<!-- (de) Fuegen Sie hier ihre Meta-Daten ein -->
  	<!--[if lte IE 7]>
		<link href="css/patches/patch_sliding_door.css" rel="stylesheet" type="text/css" />
  	<![endif]-->
	<!--<script src="js/lib/jquery.min.js"></script>-->
	<script>
			var url = window.location.href;
	</script>
</head>
<body>
	<!-- skip link navigation -->
	<ul id="skiplinks">
		<li><a class="skip" href="#nav">Skip to navigation (Press Enter).</a></li>
		<li><a class="skip" href="#col3">Skip to main content (Press Enter).</a></li>
	</ul>
	
	<s:url action="logout" var="url_logout"></s:url> 
	<s:url action="home" var="url_home"></s:url>
	<s:url action="findAllRooms" var="url_findallroom"></s:url>
	<s:url action="findAllRoomTypes" var="url_findallroomtypes"></s:url>
	<s:url action="findAllGuests" var="url_findallguest"></s:url>
	<s:url action="findAllExtras" var="url_findallextra"></s:url>
	<s:url action="findAllSeasons" var="url_findallseasons"></s:url>
	<s:url action="findAllConventions" var="url_findallconventions"></s:url>
	<s:url action="findAllFacilities" var="url_findallfacilities"></s:url>
	<s:url action="findAllImages" var="url_findallimages"></s:url>
	<s:url action="goUpdateDetails" var="url_details"></s:url>
	<s:url action="findAllPropertyTypes" var="url_findallpropertytypes"></s:url>
	<s:url action="goFindAllRoomPriceLists" var="url_findallroompricelists"></s:url>
	<s:url action="goFindAllExtraPriceLists" var="url_findallextrapricelists"></s:url>
	<s:url action="goOnlineBookings" var="url_onlinebookings"></s:url>
	<%-- <s:url action="goExport" var="url_export"></s:url> --%>
	<s:url action="goAboutInfo" var="url_about"></s:url>
	<s:url action="goUpdateAccount" var="url_useraccount"></s:url>
	<s:url action="findAllRoles" var="url_findallrole"></s:url>
	<s:url action="findAllPermissions" var="url_findallpermission"></s:url>
	<s:url action="goWizard" var="url_goWizard"></s:url>

	<div class="page_margins">
		<div class="page">
			<!-- begin: main content area #main -->
			
	<header class="header-main">
		<div class="row-fluid">
            <div class="logo">
                <a href="www.roominthemoon.com"><img src="images/ritmlogo.png"></a>
            </div>
		<s:i18n name="%{session.languageFile}">
		<div class="main-nav pull-right">			    	
			<nav class="clearfix" style="display:inline">
				<ul id="icons" class="clearfix">
					<%-- <li><span style="color: rgb(255, 112, 0);font-size: 23px;"><s:property value="#session.user.structure.name"/></span></li> --%>
					<li><a id="planner" href="<s:property value="url_home"/>?sect=planner"><s:text name="menuPlannerTxt" /></a></li>
					<% 
					User user = (User)session.getAttribute(RITMBSConstants.SESSION_USER);
					if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
					<li><a id="authorize" href="#"><s:text name="menuAuthorizeTxt" /></a>
						<ul class="submenu">
							<li><a href="<s:property value="url_findallrole"/>?sect=authorize"><s:text name="menuRoleTxt" /></a></li>
							<li><a href="<s:property value="url_findallpermission"/>?sect=authorize"><s:text name="menuPermissionTxt" /></a></li>
						</ul>
					</li>
					<%} %>
					<li><a id="accomodation" href="#"><s:text name="menuAccommodationTxt" /></a>
						<ul class="submenu">
							<li><a href="<s:property value="url_findallroom"/>?sect=accomodation"><s:text name="menuRoomTxt"/></a></li>
                            <%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
							<li><a href="<s:property value="url_findallroomtypes"/>?sect=accomodation"><s:text name="menuRoomTypeTxt" /></a></li>
                            <%}%>
							<li><a href="<s:property value="url_findallextra"/>?sect=accomodation"><s:text name="menuExtrasTxt" /></a></li>
						</ul>
					</li>
					<li><a id="guests" href="<s:property value="url_findallguest"/>?sect=guests"><s:text name="menuGuestTxt" /></a></li>
					<li><a id="settings" href="#"><s:text name="menuSettingTxt" /></a>
						<ul class="submenu">
                            <li><a href="<s:property value="url_details"/>?sect=settings"><s:text name="menuStructureSettingTxt" /></a></li>
							<li><a href="<s:property value="url_findallseasons"/>?sect=settings"><s:text name="menuSeasonTxt" /></a></li>
							<li><a href="<s:property value="url_findallroompricelists"/>?sect=settings"><s:text name="menuRoomPriceListTxt" /></a></li>
                            <li>____________________</li>
                            <li><a href="<s:property value="url_findallimages"/>?sect=settings"><s:text name="menuImageTxt" /></a></li>
                            <%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
                            <li><a href="<s:property value="url_findallpropertytypes"/>?sect=settings"><s:text name="propertyTypes" /></a></li>
                            <%}%>
                            <%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
                            <li><a href="<s:property value="url_findallfacilities"/>?sect=settings"><s:text name="menuFacilitiesTxt" /></a></li>
                            <%}%>
                            <li><a href="<s:property value="url_findallextrapricelists"/>?sect=settings"><s:text name="menudExtraPriceListTxt" /></a></li>
                            <li><a href="<s:property value="url_findallconventions"/>?sect=settings"><s:text name="menuConventionTxt" /></a></li>
                            <li><a href="<s:property value="url_onlinebookings"/>?sect=settings"><s:text name="menuOnlineBookingTxt" /></a></li>
                            <li>____________________</li>
                            <li><a href="<s:property value="url_goWizard"/>?sect=settings">Wizard</a></li>
                            <li>____________________</li>
                            <li><a href="<s:property value="url_useraccount"/>?sect=settings"><s:text name="menuUserAccountTxt" /></a></li>
						</ul>
					</li>
					<li><span><a title="logout" class="logout" href="<s:property value="url_logout"/>"><s:text name="menuSignOutTxt" /></a></span> </li>					
				</ul>
			</nav>
		</div>
		</s:i18n>
	</header>