<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<jsp:include page="../layout/header_widget.jsp" />
<!-- Start of first page -->
<div data-role="page" id="foo3">

	<!-- /header -->

	<div data-role="content">
		<s:actionerror />
		<s:fielderror></s:fielderror>
		<s:property value="#request.message.description" />
	</div>
	<!-- /content -->
	<!-- /header -->
	<div data-role="fieldcontain">
        <button type="submit" data-theme="b" id="btn-goback-widget" data-icon="arrow-r">
          <s:text name="goBackBtn"/>
        </button>        
      </div>
</div>
<!-- /page -->