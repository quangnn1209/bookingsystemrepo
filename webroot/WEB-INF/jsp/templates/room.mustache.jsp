<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page import="utils.RITMBSConstants" %>
<%@ page import="model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

	<script id="edit-template" type="text/x-handlebars-template">
		<form id="edit-form" class="yform json full" role="application">
                <div class="c50l">
                  	<div class="type-text">	
                  		<label for="FormName"><s:text name="shortDescription"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                		<input type="text" class="required" name="name" id="FormName" value="{{name}}" aria-required="true" style="width: 95%;"/>
                  	</div>
                  	<div class="type-select">           
       					<label for="FormRoomType"><s:text name="roomType"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                    	<select name="id_roomType" style="width: 45%;">
							{{#availableRoomTypes}}<option value="{{value_id}}" {{#selected}}selected="selected"{{/selected}}>{{value_name}}</option>{{/availableRoomTypes}}
						</select>
      		      	</div>
					<div class="type-select">           
       					<label for="FormMinRentTime"><s:text name="minRentTime"/></label>
                    	<select name="minRentalTime" style="width: 45%;">
							{{#rentTimes}}<option value="{{value}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/rentTimes}}
						</select>
      		      	</div>
					<div class="type-text">	
                  		<label for="FormCode"><s:text name="code"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                		<input type="text" class="required" name="code" id="FormCode" value="{{code}}" aria-required="true" style="width: 43%;"/>
                  	</div> 
				  	<div class="type-text">	
                  		<label for="FormAfb"><s:text name="roomAvailableForBooking"/></label> 
						<input type="checkbox" {{#availableforbookingChecked}}checked="checked"{{/availableforbookingChecked}} id="FormAfb" style="position: absolute;top: 0;margin-top: 4px;"/>
						<input type="hidden" value="1" name="availableforbooking"/>
                  	</div>
                  	<div class="type-button">
						<button class="btn_save"><s:text name="save"/></button>
						<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button">
							<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span>
							<span class="ui-button-text"><s:text name="cancel"/></span>
						</button>
                    </div>	
                </div>
				<div class="c50l">
					<div class="type-check">
					<label for="FormNotes"><s:text name="longDescription"/></label>
					<textarea name="notes" id="FormNotes" style="width: 359px; height: 221px;">{{notes}}</textarea>
                	</div>
				</div>
		</form>
	</script>

	<script id="view-template" type="text/x-handlebars-template">
		<form id="view-form" class="yform json inview" role="application">
            	<div class="c50l">
                  	<div class="type-text">	
                  		<strong><s:text name="shortDescription"/></strong>
						<span>{{name}}</span>
                  	</div>
                  	<div class="type-text">           
       					<strong><s:text name="roomType"/></strong>
						<span>{{roomType.name}}</span>
      		      	</div>
					<div class="type-text">           
       					<strong><s:text name="minRentTime"/></strong>
						<span>{{minRentalTime}} days</span>
      		      	</div>
					<div class="type-text">	
                  		<strong><s:text name="code"/></strong>
						<span>{{code}}</span>
                  	</div>
				  	<div class="type-text">	
                  		{{#availableforbookingChecked}}
  							<strong style="color:green;"><s:text name="roomAvailableForBooking"/></strong>
						{{/availableforbookingChecked}}
						{{^availableforbookingChecked}}
  							<strong style="color:red;"><s:text name="roomNotAvailableForBooking"/></strong>
						{{/availableforbookingChecked}}
                  	</div>
                </div>
				<div class="c50l">
                	<div class="type-text">
						<strong><s:text name="longDescription"/></strong>
						<span>{{notes}}</span> 
                  	</div>
			  	</div>
		</form>
	</script>
	<script id="no-roomtype-template" type="text/x-handlebars-template">
		<h3><strong><s:text name="roomTypesNotPresent" /></strong></h3>
		<%User user = (User)session.getAttribute(RITMBSConstants.SESSION_USER);
			if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
			<a href="<s:property value="url_findallroomtypes"/>?sect=accomodation"><img src="images/add-icon.png" alt="<s:text name="roomType" />"/><s:text name="roomType" /></a>
		<%}%>
	</script>
	<script id="row-template" type="text/x-handlebars-template">
		<div class="row-item">
			<ul>
				<li><b><s:text name="name"/>: </b>{{name}}</li>
				<li><b><s:text name="roomType"/>: </b>{{roomType.name}}</li>
				<li><b><s:text name="notes"/>: </b>{{sub_description}}</li>
				<li><input type="hidden" name="id" value="{{id}}"/></li>
				{{#available}}
					<strong style="color:green;"><s:text name="roomAvailableForBooking"/></strong>
				{{/available}}
				{{^available}}
  					<strong style="color:red;"><s:text name="roomNotAvailableForBooking"/></strong>
				{{/available}}
			</ul>
			<span class="row-item-destroy"></span>
		</div>
	</script>

	<script id="toolbar-template" type="text/x-handlebars-template">
		<li><font class="search-title"><s:text name="searchRoom" /></font><input id="item-autocomplete" type="text" value=""/>
			<div id="form-filter-container"></div>
		</li>
	</script>

    <script id="form-filter-template" type="text/x-handlebars-template">
		<form  id="filter-form" class="yform json full" role="application">
			<span class="filter-close"></span>          	
  			<div class="c80l">
               	<div class="type-text">	
                  	<label for="fFormName"><s:text name="name"/></label>
                	<input type="text" name="name" id="fFormName" value="{{name}}" aria-required="true"/>
               	</div>
               	<div class="type-text">           
       				<label for="fFormType"><s:text name="roomType"/></label>
                   	<input type="text" name="roomType" id="fFormType" value="{{roomType}}" aria-required="true"/>
      		    </div>
                <div class="type-button">
					<button class="btn_submit"><s:text name="search"/></button>
                </div>
            </div>
		</form>
	</script>

	<script id="facility-row-template" type="text/x-handlebars-template">
		{{#id}}<span class="title-elem">{{image.caption}}</span><img src="<%=request.getContextPath( )%>/rest/file/{{image.file.id}}"/>{{/id}}
	</script>

	<script id="facility-row-edit-template" type="text/x-handlebars-template">
		<input class="choose-elem" {{#id}}checked="checked"{{/id}} type="checkbox" name="facilities[]" value="{{id}}"/>
		<span class="title-elem">{{facility.name}}</span>	
		<img src="<%=request.getContextPath( )%>/rest/file/{{facility.image.file.id}}"/>
	</script>

	<script id="image-row-template" type="text/x-handlebars-template">
		{{#id}}<span class="title-elem">{{caption}}</span>
		<img width="100" src="<%=request.getContextPath( )%>/rest/file/{{file.id}}"/>{{/id}}
	</script>

	<script id="image-row-edit-template" type="text/x-handlebars-template">
		<input class="choose-elem" {{#id}}checked="checked"{{/id}} type="checkbox" name="images[]" value="{{id}}"/>
		<span class="title-elem">{{image.caption}}</span>
		<img width="100" src="<%=request.getContextPath( )%>/rest/file/{{image.file.id}}"/>
	</script>

	<script id="facilities-view-template" type="text/x-handlebars-template">
		<div class="wrapper inview">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="images-view-template" type="text/x-handlebars-template">
		<div class="wrapper inview">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="facilities-edit-template" type="text/x-handlebars-template" >
		<%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
		<div class="add-new">
			<a href="<%=request.getContextPath( )%>/findAllFacilities.action?sect=settings" class="btn_add"><s:text name="facilityEdit" /></a>
		</div>
		<%}%>
		<div class="wrapper">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="images-edit-template" type="text/x-handlebars-template" >
		<div class="add-new">
			<a href="<%=request.getContextPath( )%>/findAllImages.action?sect=settings" class="btn_add"><s:text name="editImages" /></a>
		</div>
		<div class="wrapper">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>