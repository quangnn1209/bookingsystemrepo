<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page import="utils.RITMBSConstants" %>
<%@ page import="model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<script id="structure-propertyTypeList-template" type="text/x-handlebars-template">
		{{#propertyTypes}}
			<option value="{{id}}">{{name}}</option>
		{{/propertyTypes}}
	</script>
    <script id="edit-template" type="text/x-handlebars-template">
		<%User user = (User)session.getAttribute(RITMBSConstants.SESSION_USER);%>
		<form id="edit-form" class="yform json full" role="application">
          	<div class="c50l">   
            	  <div class="type-text">
                  	<label for="FormName"><s:text name="nameCodeStruct"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                  	<input type="text" class="required" name="name" id="FormName" value="{{name}}" aria-required="true" style="width:70%;"/>
					<input type="hidden" name="redirect_form" value="false" />
					<label for="FormPropertyType" style="position: absolute;top: 0;right: 0;margin-right: 50px;"><s:text name="propertyType"/></label>
					<select name="id_propertyType" size="1" aria-required="true" name="isEnable" style="position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 21px;">
						{{#availablePropertyTypes}}<option value="{{id}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/availablePropertyTypes}}
					</select>
                  </div>
                  <div class="type-text">
               	 	<label for="FormAddress"><s:text name="address"/></label>
                  	<input type="text" name="address" id="FormAddress" value="{{address}}" aria-required="true" style="width:70%;" />
					<label for="FormNumber" style="position: absolute;top: 0;right: 0;margin-right: 50px;"><s:text name="numberStruct"/></label>
                  	<input type="text" name="number" id="FormNumber" value="{{number}}" aria-required="true" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 18px;"/>
              	  </div>
             	  <div class="type-text">
               	 	<label for="FormCity"><s:text name="city"/></label>
                  	<input type="text" name="city" id="FormCity" value="{{city}}" aria-required="true" style="width:70%;" />
					<label for="FormZipCode" style="position: absolute;top: 0;right: 0;margin-right: 50px;"><s:text name="zipCode"/></label>
                  	<input type="text" name="zipCode" id="FormZipCode" value="{{zipCode}}" aria-required="true" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 18px;" />
              	  </div>
              	  <div class="type-select">
                	<label for="FormCountry"><s:text name="country"/></label>
                  	<select  name="country" id="FormCountry" size="1" aria-required="true" style="width: 185px;">
						{{#availableCountries}}<option value="{{code}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/availableCountries}}
					</select>
					<label for="FormCleaningFee" style="position: absolute;top: 0;right: 0;margin-right: 120px;"><s:text name="cleaningFee"/></label>
                  	<span style="width: 10%; position: absolute;  top: 0;  right: 0;margin-right: 160px;margin-top: 20px;height: 22px;font-size: 20px;"><%=user.getUnitSymbol()%></span><input type="text" name="cleaningFee" id="FormCleaningFee" value="{{cleaningFee}}" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 140px;margin-top: 18px;height: 22px;" />
					
					<label for="FormDeposit" style="position: absolute;top: 0;right: 0;margin-right: 60px;"><s:text name="deposit"/></label>
                  	<span style="width: 10%; position: absolute;  top: 0;  right: 0;margin-right: 70px;margin-top: 20px;height: 22px;font-size: 20px;"><%=user.getUnitSymbol()%></span><input type="text" name="deposit" id="FormDeposit" value="{{deposit}}" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 52px;margin-top: 18px;height: 22px;" />
					<!-- View map -->
					<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" onclick="window.open('goGoogleMap?structure.id={{id}}','_blank')" type="button">
						<span class="ui-button-icon-primary ui-icon"></span>
						<span class="ui-button-text"><s:text name="viewMap"/></span>
					</button>
           	 	  </div>
 				  <div class="type-text">
					<label for="FormVideoLink"><s:text name="videoLink"/></label>
                	<input type="text" name="videolink" id="FormVideoLink" value="{{videolink}}" aria-required="true" size="20" />
              	  </div>
				  <div class="type-text">
					<label for="FormEmail"><s:text name="email"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                	<input type="text" class="required email" name="email" id="FormEmail" value="{{email}}" aria-required="true" size="20" style="width:50%;" />
					<label for="FormPhone" style="position: absolute;top: 0;right: 0;margin-right: 165px;margin-top: 3px;"><s:text name="phone"/><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
               	 	<input type="text" class="required validPhone" name="phone" id="FormPhone" value="{{phone}}" aria-required="true" style="width:30%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 21px;" />
              	  </div>
				  <div class="type-text">				
					<label for="FormBookingType"><s:text name="bookingType"/></label>
					<select name="haveroomavailable">
						{{#availableBookingTypes}}<option value="{{value}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/availableBookingTypes}}
					</select>
					<label for="FormPersonalPhone" style="position: absolute;top: 0;right: 0;margin-right: 115px;margin-top: 3px;"><s:text name="personalPhone"/></label>
               	 	<input type="text" class="validPhone" name="personalPhone" id="FormPersonalPhone" value="{{personalPhone}}" aria-required="true" style="width:30%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 17px;" />
              	  </div> 
				  <div class="type-select">
					<%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
				  	<div class="type-check">
                  		<label for="FormPublishing"><s:text name="publishTheProperty"/></label> 
						<input type="checkbox" {{#isPublished}}checked="checked"{{/isPublished}} id="FormPublishing"/>
						<input type="hidden" value="1" name="published"/>
                  	</div>
					<%}%>
                	<label for="FormStatus" style="margin-left: 6px;"><s:text name="status"/>:</label>
					<input type="checkbox" {{#isEnabled}}checked="checked"{{/isEnabled}} id="FormStatus" style="margin-top: -13px;margin-left: 95px;"/>
					<input type="hidden" value="1" name="isEnable"/>
           	 	  </div>
				  <div class="type-button">
                  	<button class="btn_save" id="btn-save-structure"><s:text name="save"/></button>
					<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button">
						<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span>
						<span class="ui-button-text"><s:text name="cancel"/></span>
					</button>
              	  </div>
			</div>
			<div class="c50l">
            	  <div class="type-text">
                  	<label for="FormNotes"><s:text name="description"/></label> 
                  	<textarea name="notes" id="FormNotes" style="height: 190px;width: 457px;">{{notes}}</textarea>
                  </div>
				  <div class="type-text">
                  	<label for="FormHouseRules"><s:text name="houseRules"/></label> 
                  	<textarea name="houseRules" id="FormHouseRules" style="height: 90px;width: 457px;">{{houseRules}}</textarea>
                  </div>
				  <div class="type-text">
                  	<label for="FormReachUS"><s:text name="reachUs"/></label> 
                  	<textarea name="reach_us" id="FormReachUS" style="height: 90px;width: 457px;">{{reach_us}}</textarea>
                  </div>
            </div>
        </form>
	</script>

    <script id="view-template" type="text/x-handlebars-template">
		<form id="view-form" class="yform inview" role="application">
                <div class="c50l">
                  	<div class="type-text">           
       					<strong><s:text name="nameCodeStruct"/>:</strong>
                    	<span>{{name}}</span>
						&nbsp;
						<strong><s:text name="propertyType"/>:</strong>
                		<span>{{propertyTypeIds}}</span>
      		      	</div> 
                  	<div class="type-text">	
                  		<strong><s:text name="address"/>:</strong>
                		<span>{{address}}</span>
						&nbsp;
						<strong><s:text name="numberStruct"/>:</strong>
                		<span>{{number}}</span>
                  	</div>
					<div class="type-text">	
                  		<strong><s:text name="city"/>:</strong>
                		<span>{{city}}</span>
						&nbsp;
						<strong><s:text name="zipCode"/>:</strong>
                		<span>{{zipCode}}</span>
                  	</div>
					<div class="type-text">	
                  		<strong><s:text name="country"/>:</strong>
                		<span>{{country}}</span>
						<strong><s:text name="cleaningFee"/>:</strong>
                		<span><%=user.getUnitSymbol()%>{{cleaningFee}}</span>
						<strong><s:text name="deposit"/>:</strong>
                		<span><%=user.getUnitSymbol()%>{{deposit}}</span>
                  	</div>
					<div class="type-text">	
                  		<strong><s:text name="videoLink"/>:</strong>
                		<span>{{videolink}}</span>
                  	</div>
                  	<div class="type-text">           
       					<strong><s:text name="email"/>:</strong>
                    	<span>{{email}}</span>
						&nbsp;
						<strong><s:text name="phone"/>:</strong>
                		<span>{{phone}}</span>
      		      	</div>
					<div class="type-text">	
                  		<strong><s:text name="bookingType"/>:</strong>
                		<span>{{bookingTypeName}}</span>
						<strong><s:text name="personalPhone"/>:</strong>
                		<span>{{personalPhone}}</span>
                  	</div>
					<div class="type-text">	
						<strong><s:text name="publishTheProperty"/></strong>
						{{#isPublished}}
  							<strong style="color:green;"><s:text name="published"/></strong>
						{{/isPublished}}
						{{^isPublished}}
  							<strong style="color:red;"><s:text name="notPublished"/></strong>
						{{/isPublished}}
						&nbsp;
                  		<strong><s:text name="status"/>:</strong>
                		<span>{{status}}</span>
                  	</div>
					
              	</div>
				<div class="c50l">
					<div class="type-text">	
                  		<strong><s:text name="description"/>:</strong>
                		<span>{{notes}}</span>
                  	</div>
					<div class="type-text">	
                  		<strong><s:text name="houseRules"/>:</strong>
                		<span>{{houseRules}}</span>
                  	</div>
					<div class="type-text">	
                  		<strong><s:text name="reachUs"/>:</strong>
                		<span>{{reach_us}}</span>
                  	</div>
              	</div>
		</form>
	</script>
	
	<script id="row-template" type="text/x-handlebars-template">
		<div class="row-item">
		  	<ul>
				<li><b><s:text name="name"/>: </b>{{name}}</li>
				<li><b><s:text name="email"/>: </b>{{email}}</li>
				<li><b><s:text name="notes"/>: </b>{{sub_description}}</li>
				{{#publishedForList}}
					<li><strong style="color:green;"><s:text name="published"/></strong></li>  				
				{{/publishedForList}}
				{{^publishedForList}}
  					<li><strong style="color:red;"><s:text name="notPublished"/></strong></li>
				{{/publishedForList}}
		  		<li><input type="hidden" name="id" value="{{id}}"/></li>
				<%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
				<li><b><s:text name="belongToUser"/>: </b>{{userBelong}}</li>
				<%}%>
		  	</ul>
			<span class="row-item-destroy"></span>
		</div>
	</script>
	
	<script id="toolbar-template" type="text/x-handlebars-template">
		<li><font class="search-title"><s:text name="searchStructure" /></font><input id="item-autocomplete" type="text" value=""/>
			<div id="form-filter-container"></div>
		</li>
	</script>

    <script id="form-filter-template" type="text/x-handlebars-template">
		<form id="filter-form" class="yform json full" role="application">
			<span class="filter-close"></span>
          	<div class="c80l">
            	<div class="type-text">	
                  	<label for="fFormName"><s:text name="name"/></label>
                	<input type="text" name="name" id="fFormName" value="{{name}}" aria-required="true"/>
                </div>
				<div class="type-text">
               	 	<label for="FormAddress"><s:text name="address"/></label>
                  	<input type="text" name="address" id="FormAddress" value="{{address}}" aria-required="true" />
              	</div>
             	<div class="type-text">
               	 	<label for="FormCity"><s:text name="city"/></label>
                  	<input type="text" name="city" id="FormCity" value="{{city}}" aria-required="true" />
              	</div>
              	<div class="type-text">
               	 	<label for="FormZipCode"><s:text name="zipCode"/></label>
                  	<input type="text" name="zipCode" id="FormZipCode" value="{{zipCode}}" aria-required="true" />
              	</div>
				<div class="type-text">
               	 	<label for="FormCountry"><s:text name="country"/></label>
                  	<input type="text" name="country" id="FormCountry" value="{{country}}" aria-required="true" />
              	</div>
               	<div class="type-button">
					<button class="btn_submit"><s:text name="search"/></button>
               	</div>
           	</div>
		</form>
	</script>
	
	<script id="facility-row-template" type="text/x-handlebars-template">
		{{#id}}<span class="title-elem">{{name}}</span><img src="<%=request.getContextPath( )%>/rest/file/{{image.file.id}}"/>{{/id}}
	</script>

	<script id="facility-row-edit-template" type="text/x-handlebars-template">
		<input class="choose-elem" {{#id}}checked="checked"{{/id}} type="checkbox" name="facilities[]" value="{{id}}"/>
		<span class="title-elem">{{facility.name}}</span>		
		<img src="<%=request.getContextPath( )%>/rest/file/{{facility.image.file.id}}"/>
	</script>

	<script id="image-row-template" type="text/x-handlebars-template">
		{{#id}}<span class="title-elem">{{caption}}</span>
		<img src="<%=request.getContextPath( )%>/rest/file/{{file.id}}"/>{{/id}}
	</script>

	<script id="image-row-edit-template" type="text/x-handlebars-template">
		<input class="choose-elem" {{#id}}checked="checked"{{/id}} type="checkbox" name="images[]" value="{{id}}"/>
		<span class="title-elem">{{image.caption}}</span>
		<img src="<%=request.getContextPath( )%>/rest/file/{{image.file.id}}"/>
	</script>

	<script id="facilities-view-template" type="text/x-handlebars-template">
		<div class="wrapper inview">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="images-view-template" type="text/x-handlebars-template">
		<div class="wrapper inview">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="facilities-edit-template" type="text/x-handlebars-template" >
		<div class="add-new">
			<%if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
				<a href="<%=request.getContextPath( )%>/findAllFacilities.action?sect=settings" class="btn_add"><s:text name="facilityEdit" /></a>
			<%}%>
		</div>
		<div class="wrapper">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>

	<script id="images-edit-template" type="text/x-handlebars-template" >
		<div class="add-new">
			<a href="<%=request.getContextPath( )%>/findAllImages.action?sect=settings" class="btn_add"><s:text name="editImages" /></a>
		</div>
		<div class="wrapper">
			<ul></ul>
		</div>
		<span class="ui-rcarousel-next"></span>
		<span class="ui-rcarousel-prev disable"></span>
	</script>
