<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page import="utils.RITMBSConstants" %>
<%@ page import="model.User" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script id="edit-template" type="text/x-handlebars-template">
</script>
<script id="view-template" type="text/x-handlebars-template">
    <form method="post" action="updateAccount.action" id="edit-form" class="yform json full" role="application">
        <div class="c50l">
            <div class="type-text">
                <label for="FormFirstName"><s:text name="firstName"></s:text><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                <input type="text" class="required" name="user.surname" id="FormFirstName" value="{{name}}" aria-required="true"/>
              </div>
              <div class="type-text">
                <label for="FormLastName"><s:text name="lastName"></s:text><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                <input type="text" class="required" name="user.name" id="FormLastName" value="{{surname}}" aria-required="true"/>
              </div>
			  <div class="type-text">
              	<label for="FormPassword"><s:text name="password"></s:text><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                <input type="password" class="required" name="password" id="FormPassword" aria-required="true" />
              </div>
              <div class="type-text">
                <label for="FormRetyped"><s:text name="reTypePassword"></s:text> <sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                <input type="password" class="required" name="reTyped" id="FormRetyped"  aria-required="true" />
              </div>
              <div class="type-button">
                <button class="btn_save"><s:text name="save"/></button>
                <button class="btn_reset btn_cancel_form"><s:text name="cancel"/></button>
              </div>
			</div>
			<div class="c50l">
              <div class="type-text">
              	<label for="FormPhone"><s:text name="phone"></s:text><sup title="<s:text name="thisFileMandatory"/>.">*</sup></label>
                <input type="text" class="required" name="user.phone" id="FormPhone" value="{{phone}}" aria-required="true"/>
              </div>
              <div class="type-text">
				<label for="FormEmail"><s:text name="email"></s:text></label>
				{{email}}
              </div>
			  <div class="type-select">           
       			<label for="FormLanguage"><s:text name="selectLanguage"/></label>
                <select name="user.id_language">
				{{#availableLanguage}}<option value="{{id}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/availableLanguage}}
				</select>
      		  </div>
			  <div class="type-select">
				<%User user = (User)session.getAttribute(RITMBSConstants.SESSION_USER);
					if(user.getId_role().equals(RITMBSConstants.ROLE_ADMIN_ID)){ %>
       			<label for="FormRole"><s:text name="setRole"/></label>
                <select name="user.id_role">
						<option>Please select a role</option>
				{{#roles}}<option value="{{value}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>{{/roles}}
				</select>
				<%}else if(RITMBSConstants.ROLE_USER_ID.equals(user.getId_role())){%>
				<label for="FormRole"><s:text name="yourRoleIsUser"/></label>
				<%}%>
      		  </div>
            </div>
			<input type="hidden" name="redirect_form" value="goUpdateAccount.action" />
        </form>
	</script>
	<script id="row-template" type="text/x-handlebars-template">
	</script>

	<script id="toolbar-template" type="text/x-handlebars-template">
	</script>

    <script id="form-filter-template" type="text/x-handlebars-template">
	</script>