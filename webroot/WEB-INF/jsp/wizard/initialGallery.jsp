<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<style>
	.slick-prev, .slick-next {
		background: orange !important;
	}

</style>
<script type="text/javascript">
  $(document).ready(function() {
	  $('.gallery-slide').slick({
		  dots: true,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 6,
		  slidesToScroll: 6,
		  arrows: true
		});
	  
	  $(".image-enlarge").fancybox({
		'hideOnContentClick': true
	  });
	  
	  $("[name=idFile]").change(function(){
		  if($(this).is(":checked")){
			  saveImageRelationship($(this).val(), "add");
		  }else{
			  saveImageRelationship($(this).val(), "delete");
		  }
	  });
  });
</script>
<span style="font-size: 1.8vw;font-weight: bold;">Please select image</span>
<div class="gallery-slide">
	<s:iterator value="imageList" var="image">
		<div style="cursor: pointer;">
			<input type="checkbox" name="idFile" value="${image.id}" ${isChecked ? 'checked="checked"':'' } data-value="${image.file.id}"/>
			<a class="image-enlarge" href="#image-${image.file.id}"><img src="/RITMBS/rest/file/${image.file.id}" alt="${image.caption}" class="wz-image"/></a>
			<div style="display: none;"><img src="/RITMBS/rest/file/${image.file.id}" id="image-${image.file.id}" width="700px"/></div>
		</div>
	</s:iterator>
</div>
