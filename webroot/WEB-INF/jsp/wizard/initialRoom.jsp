<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<script type="text/javascript">
	$(document).ready(function(){
		enableButton();
		
     	$("#save-room-button").click(function(){
     	// Remove message before validate
			$(".wzmessage").remove();
			
			var invalid = false;
			$(".required").each(function(){
				if($(this).val() == ""){
					$(this).after(message);
					invalid = true;
				}
			});
			
			if(invalid){
				return false;
			}
     		// Save room
     		$.ajax({
     			contentType: 'application/json',
     			data: JSON.stringify($('#edit-room-form').serializeObject()),
     			type: "post",
				url: 'rest/rooms/<s:property value="#session.wizard.idStructure"/>'
            }).done(function(response) {
            	console.log(response);
            	$('[name=isSaved-<s:property value="roomWzId"/>]').val("true");
            	$('[name=idRoom-<s:property value="roomWzId"/>]').val(response.id);
            	$('#icon-<s:property value="roomWzId"/>').css("background-color","rgb(116, 252, 116)");
            	$('#icon-<s:property value="roomWzId"/>').addClass("ui-icon-check");
            	$('#icon-<s:property value="roomWzId"/>').removeClass("ui-icon-circle-plus");

            	enableButton();
           		// Save room
           		$.ajax({
      				url: 'updateProgress'
                }).done(function(response) {
                  	console.log(response);
                  	$.jGrowl("Register new room success!", { header: this.alertOK });
                  	$('#icon-<s:property value="roomWzId"/>').parents("li.room-type:first").trigger("click");
                });
            });
     	});
	});
</script>
<form id="edit-room-form" class="yform json full" role="application">
	<input type="hidden" value="<s:property value="#session.wizard.idStructure"/>" name="id_structure"/>
	<div class="c50l">
		<div class="type-text">
			<label for="FormName">Short Description<sup
				title="This Field is Mandatory.">*</sup></label> <input type="text"
				class="required" name="name" id="FormName" aria-required="true" style="width: 95%;">
		</div>
		<div class="type-select">
			<label for="FormRoomType">Room Type:</label> <s:property value="roomTypeName"/>
			<input type="hidden" name="id_roomType" value="<s:property value="idRoomTypeSingle"/>"/>
			<label for="FormWeeklyPrice">Weekly price<sup
				title="This Field is Mandatory.">*</sup></label> <s:property value="#session.user.unitSymbol"/><input type="text"
				class="required" name="weeklyPrice" id="FormWeeklyPrice"
				aria-required="true" style="width: 43%;" value="<s:property value="room.weeklyPrice"/>">
		</div>
		<div class="type-select">
			<label for="FormMinRentTime">Minimum rental time</label> <select
				name="minRentalTime" style="width: 45%;">
				<option value="7" selected="selected">7 days</option>
				<option value="14">14 days</option>
				<option value="30">30 days</option>
				<option value="60">60 days</option>
				<option value="120">120 days</option>
				<option value="182">182 days</option>
				<option value="365">365 days</option>
			</select>
		</div>
		<div class="type-text">
			<label for="FormCode">Code<sup
				title="This Field is Mandatory.">*</sup></label> <input type="text"
				class="required" name="code" id="FormCode"
				aria-required="true" style="width: 43%;">
		</div>
		<div class="type-button">
			<button
				class="btn_save ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary"
				role="button" type="button" id="save-room-button">
				<span class="ui-button-icon-primary ui-icon ui-icon-check"></span><span
					class="ui-button-text">Save</span>
			</button>
			<button
				class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary"
				role="button" type="button">
				<span
					class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span>
				<span class="ui-button-text">Cancel</span>
			</button>
		</div>
	</div>
	<div class="c50l">
		<div class="type-check">
			<label for="FormNotes">Long Description</label>
			<textarea name="notes" id="FormNotes" style="width: 359px; height: 221px;"></textarea>
		</div>
	</div>
</form>