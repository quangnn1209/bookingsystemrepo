<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<script type="text/javascript">
	$(document).ready(function(){
		enableButton();
		
		if('<s:property value="action"/>' != 'saveWizard5' && ${sessionScope.wizard.progress > 45}){
			$(".room-type").click(function(){
				window.location.href='goWizard5';
			});	
		}
		
		if(${(requestScope.action == 'goWizard3' || requestScope.action == 'goWizard4' || requestScope.action == 'goWizard5')}){
			$("#save-button").removeAttr('disabled');
    		$("#save-button").css("background","");
		}
	});
</script>
<div id="main-app" style="width: 20%;float: left;">
	<div id="row-list">
	  	<ul>
			<li onclick="window.location.href='goWizard2'">Photo<span class="ui-button-icon-primary ui-icon ${sessionScope.wizard.progress > 15 ? 'ui-icon-check':'ui-icon-circle-plus' }" style="float:right;${sessionScope.wizard.progress > 15 ? 'background-color: rgb(116, 252, 116);':'' }"></span></li>
			<li onclick="${sessionScope.wizard.progress > 15 ? "window.location.href='goWizard3'":""}">
			Property<span class="ui-button-icon-primary ui-icon ${sessionScope.wizard.progress > 30 ? 'ui-icon-check':'ui-icon-circle-plus' }" style="float:right;${sessionScope.wizard.progress > 30 ? 'background-color: rgb(116, 252, 116);':'' }"></span>
			<br/><span style="margin-left: 30px;">details</span>
			<br/><span style="margin-left: 30px;">address</span>
			<br/><span style="margin-left: 30px;">facilities</span>
			</li>
			<li onclick="${sessionScope.wizard.progress > 30 ? "window.location.href='goWizard4'":""}">Calendar<span class="ui-button-icon-primary ui-icon ${sessionScope.wizard.progress > 45 ? 'ui-icon-check':'ui-icon-circle-plus' }" style="float:right;${sessionScope.wizard.progress > 45 ? 'background-color: rgb(116, 252, 116);':'' }"></span></li>
			<s:iterator value="%{#session.roomWzList}" var="roomWz" status="status">
				<li class="room-type">
					Room ${status.index+1}<span class="ui-button-icon-primary ui-icon ${roomWz.idRoom != null?'ui-icon-check':'ui-icon-circle-plus'}" style="float:right;${roomWz.idRoom != null?'background-color: rgb(116, 252, 116);':''}" id="icon-${roomWz.id}"></span>
					<br/><span style="margin-left: 30px;">details</span>
					<br/><span style="margin-left: 30px;">price</span>
					<input type="hidden" value="${roomWz.idRoomType}" name="idRoomType"/>
					<input type="hidden" name="isSaved-${roomWz.id}" value="${roomWz.idRoom != null}"/>
					<input type="hidden" name="idRoom-${roomWz.id}" value="${roomWz.idRoom}"/>
					<input type="hidden" name="roomWzId" value="${roomWz.id}" />
				</li>
			</s:iterator>
	  	</ul>
	</div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" type="button"
	style="margin-top: 100px;margin-left: 100px;background: gray;border: gray;" onclick="window.location.href='${sessionScope.wizard.progress == 100 ? 'saveWizard5' : requestScope.action}'" id="save-button" ${(requestScope.action == 'goWizard3' || requestScope.action == 'goWizard4' || requestScope.action == 'goWizard5') ? '':'disabled="disabled"'}>
		<span class="ui-button-text">${sessionScope.wizard.progress == 100 ? "End":"Continue"}</span>
	</button>
</div>