<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="wizardHeader.jsp" />
<script type="text/javascript">
	$(document).ready(function(){
		$("#room-numb").change(function(){
			// Keep the select box
			var slBoxHtml = $("#wz-tbl-1 .room-type:first").html();
			if($(this).val() >= 1){
				// Remove all old room type
				$("#wz-tbl-1 .room-type").each(function(index){
					if(index != 0){
						$(this).remove();
					}
				});
				
				// Append
				for(var i = 2; i<= $(this).val(); i++){
					// Append tr to table
					$("#wz-tbl-1").append('<tr class="room-type">' + slBoxHtml + '</tr>');
					var newTr = $("#wz-tbl-1").find("tr.room-type:last");
					$(newTr).find("td.guest-numb").text($(newTr).find("select[name=idRoomType]").val().split("-")[1]);
				}
				
				// Re-index
				$("th.room-index").each(function(index){
					$(this).text("Room "+(index+1));
				});
				
				// Remove numb of guest
				// $(".guest-numb").text("");
				
				// Set event for new element
				$("[name=idRoomType]").change(function(){
					$(this).parents("tr.room-type:first").find("span.guest-numb").text($(this).val().split("-")[1]);
				});
			}
		});
		
		$("[name=idRoomType]").change(function(){
			$(this).parents("tr.room-type:first").find("span.guest-numb").text($(this).val().split("-")[1]);
		});
	});
</script>
<body>
<div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" onclick="window.location.href='home'" type="button">
		<span class="ui-button-text">Exit</span>
	</button>
	 <span class="progress-span">Completed <s:property value="#session.wizard.progress"/>%</span>
</div>
<div>
	<form action="saveWizard1" method="post">
		<table id="wz-tbl-1" style="margin: 0 auto;" class="yform json full">
			<tr>
				<th>Home type</th>
				<td>
					<select name="idPropertyType">
						<s:iterator value="propertyTypeList" var="prop">
							<option value="${prop.id}">${prop.name}</option>
						</s:iterator>
					</select>
				</td>
			</tr>
			<tr>
				<th>Rooms available</th>
				<td>
					<select id="room-numb">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</td>
			</tr>
			<tr class="room-type">
				<th class="room-index">Room 1</th>
				<td>
					<select name="idRoomType">
						<s:iterator value="roomTypeList" var="roomType">
							<option value="${roomType.id}-${roomType.maxGuests}">${roomType.name}</option>
						</s:iterator>
					</select>
					Number of guest <span class="guest-numb">1</span>
				</td>
			</tr>
		</table>
		<br/>
		<div>
			<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" type="submit">
				<span class="ui-button-text">Continue</span>
			</button>
		</div>
	</form>
</div>
</body>