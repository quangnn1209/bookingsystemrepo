<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="wizardHeader.jsp" />
<script type="text/javascript">
	$(document).ready(function(){
        // Initial gallery
        $.ajax({
          url: "initialGallery"
        }).done(function(response) {
        	$("#images").html(response);
        });
        
        $("[name=upload]").change(function(){
        	$("[name=caption]").val($("[name=facility_name]").val());
        	$("#uploadFacility").ajaxSubmit({
     	      success: function(response) {
	     	    	// Initial gallery
	     	         $.ajax({
	     	           url: "initialGallery"
	     	         }).done(function(response) {
	     	         	$("#images").html(response);
	     	         });
	     	    	
	     	    	// Enable button
	     	        $("#save-button").removeAttr('disabled');
	        		$("#save-button").css("background","");
     	        }
     	      });
        });
        
        $("#delete-image").click(function (){
        	var idImage = "";
        	$("#images").find("input:checkbox").each(function(){
        		if($(this).is(":checked")){
        			idImage+= $(this).attr("data-value");
            		idImage+=",";	
        		}
        	});
        	
        	if(idImage != ""){
				$.ajax({
				      url: "deleteImage",
				      data:{
				    	  idFiles:idImage
				      }
				}).done(function(response) {
					$.jGrowl("Delete image success!", { header: this.alertOK });
					console.log(response);
					 // Initial gallery
			        $.ajax({
			          url: "initialGallery"
			        }).done(function(response) {
			        	$("#images").html(response);
			        });
				});
        	}
        });
	});
	
	function saveImageRelationship(){
		return false;
	}
</script>
<body>
<div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary ${sessionScope.disabledBack?'disabled-button':'' }" onclick="window.location.href='home'" type="button" ${sessionScope.disabledBack?'disabled="disabled"':'' }>
		<span class="ui-button-text">Exit</span>
	</button>
	<span class="progress-span">Completed <s:property value="#session.wizard.progress"/>%</span>
	
</div>
<div>
		<jsp:include page="menuSide.jsp" />		
		<div class="beauty" style="width: 70%;float: right;">
     	  	<div class="subcolumns">
 			  	<div class="c20l">
 					<form id="uploadFacility" action="rest/images" method="post" enctype="multipart/form-data" class="file_upload">
						<input type="hidden" name="caption" value="Wizard image"/>
						<input type="hidden" name="wzId" value="<s:property value="#session.wizard.id"/>"/>
						<input type="hidden" name="idStructure" value="<s:property value="#session.wizard.idStructure"/>"/>
     			  		<input type="file" name="upload"/>
    			  		<button>Upload</button> 
    			  		<div>Upload Image</div>  
					</form>
		   	  	</div>
		   	  	<div class="c20l">
			   	  	<button class="file_upload" id="delete-image" style="height: 32px;margin-left: 30px;" type="button">
						<span class="ui-button-text">Delete image</span>
					</button>
				</div>
		   	</div>
		   	<br/>
		   	<br/>
		   	<br/>
			<div id="images"/>
        </div>
</div>
</body>