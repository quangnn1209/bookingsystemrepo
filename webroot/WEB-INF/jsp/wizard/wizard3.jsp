<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="wizardHeader.jsp" />
<script type="text/javascript">
	$(document).ready(function(){
        // Initial gallery
        if('<s:property value="#session.wizard.idStructure"/>' != ''){
        	initialGalleryStructure();	
        }
        
        // Add event
        $("button#btn-save-structure").click(function(){
        	// Validate data
        	if($("input#FormCleaningFee").val() == ""){
        		$("input#FormCleaningFee").val(0);
        	}
        	if($("input#FormDeposit").val() == ""){
        		$("input#FormDeposit").val(0);
        	}
        	
        	var videoLink = $("input#FormVideoLink").val();
        	if(videoLink != ""){
            	// Validate length
            	if(videoLink.length > 100){
            		$.jGrowl($.i18n("invalidMaxlength"),  { theme: "notify-error",sticky: true  });
            		return false;
            	// Validate URL	
            	}else if(!(/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(videoLink))){
            		$.jGrowl($.i18n("invalidURL"),  { theme: "notify-error",sticky: true  });
            		return false;
            	// Validate host
            	}else if(videoLink.toUpperCase().indexOf("youtube.com".toUpperCase()) == -1 && videoLink.toUpperCase().indexOf("vimeo.com".toUpperCase())  == -1 && videoLink.toUpperCase().indexOf("dailymotion.com".toUpperCase()) == -1){
            		$.jGrowl($.i18n("invalidVideoContent"),  { theme: "notify-error",sticky: true  });
            		return false;
            	}        		
        	}
        });
        
        $("button#view-map-button").click(function(){
        	var message = '<label class="wzmessage error">This field is required.</label>';

        	// Validate data
        	if($("#FormAddress").val() == "" || $("#FormCity").val() == ""){
        		$.jGrowl("You must input address and city to view the map",  { theme: "notify-error",sticky: true  });
        		return false;
        	}
        	window.open('goGoogleMap?structure.address='+$("#FormAddress").val()+"&structure.city="+$("#FormCity").val()+"&structure.country="+$("#FormCountry").val(),'_blank');
        });
        
        if('<s:property value="structure.country"/>' != '')
        	$("#FormCountry").val('<s:property value="structure.country"/>');
        if('<s:property value="structure.haveroomavailable"/>' != '')
        	$("#bookingType").val('<s:property value="structure.haveroomavailable"/>');
	});
	
	function saveImageRelationship(idImage,command){
		$.ajax({
 			contentType: 'application/json',
			url: 'saveStructureImage',
			data:{
				idStructure:'<s:property value="#session.wizard.idStructure"/>',
				idImage:idImage,
				command:command
			}
        }).done(function(response) {
        	console.log(response);
            $.jGrowl("Update image relationship success!", { header: this.alertOK });
        });
	}
</script>
<body>
<div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary ${sessionScope.disabledBack?'disabled-button':'' }" onclick="window.location.href='home'" type="button" ${sessionScope.disabledBack?'disabled="disabled"':'' }>
		<span class="ui-button-text">Exit</span>
	</button>
	<span class="progress-span">Completed <s:property value="#session.wizard.progress"/>%</span>
</div>
<div>
		<jsp:include page="menuSide.jsp" />
		<div class="subcolumns beauty" id="row-edit-container" style="width: 75%;float: right;">		
			<form id="edit-form" class="yform json full" role="application" action="saveStructure" method="post">
			<input type="hidden" name="idPropertyType" value="<s:property value="#session.wizard.idPropertyType"/>"/>
			<input type="hidden" name="structure.id" value="<s:property value="structure.id"/>"/>
          	<div class="c50l">
            	  <div class="type-text">
                  	<label for="FormName">Code<sup title="This Field is Mandatory.">*</sup></label>
                  	<input type="text" class="required" name="structure.name" id="FormName"  style="width:70%;" value="<s:property value="structure.name"/>"/>
					<label for="FormPropertyType" style="position: absolute;top: 0;right: 0;margin-right: 50px;">Property type:</label><s:property value="propertyTypeName"/>
                  </div>
                  <div class="type-text">
               	 	<label for="FormAddress">Street</label>
                  	<input type="text" name="structure.address" id="FormAddress"  style="width:70%;" value="<s:property value="structure.address"/>"/>
					<label for="FormNumber" style="position: absolute;top: 0;right: 0;margin-right: 50px;">Number</label>
                  	<input type="text" name="structure.number" id="FormNumber"  style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 18px;" value="<s:property value="structure.number"/>"/>
              	  </div>
             	  <div class="type-text">
               	 	<label for="FormCity">City</label>
                  	<input type="text" name="structure.city" id="FormCity"  style="width:70%;" value="<s:property value="structure.city"/>"/>
					<label for="FormZipCode" style="position: absolute;top: 0;right: 0;margin-right: 50px;">ZIP/Post Code</label>
                  	<input type="text" name="structure.zipCode" id="FormZipCode"  style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 50px;margin-top: 18px;" value="<s:property value="structure.zipCode"/>"/>
              	  </div>
              	  <div class="type-select">
                	<label for="FormCountry">Country</label>
                  	<select name="structure.country" id="FormCountry" size="1"  style="width: 185px;">
						<option value="AF">Afghanistan</option><option value="AX">Åland Islands</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo</option><option value="CD">Congo, The Democratic Republic of the</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="CI">Cote D'Ivoire</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and Mcdonald Islands</option><option value="VA">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran, Islamic Republic Of</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KP">Korea, Democratic People'S Republic of</option><option value="KR">Korea, Republic of</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Lao People'S Democratic Republic</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libyan Arab Jamahiriya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao</option><option value="MK">Macedonia, The Former Yugoslav Republic of</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia, Federated States of</option><option value="MD">Moldova, Republic of</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory, Occupied</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russian Federation</option><option value="RW">Rwanda</option><option value="SH">Saint Helena</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="CS">Serbia and Montenegro</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syrian Arab Republic</option><option value="TW">Taiwan, Province of China</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania, United Republic of</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="UK"  selected="selected">United Kingdom</option><option value="US">United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela</option><option value="VN">Viet Nam</option><option value="VG">Virgin Islands, British</option><option value="VI">Virgin Islands, U.S.</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select>
					<label for="FormCleaningFee" style="position: absolute;top: 0;right: 0;margin-right: 120px;">Cleaning fee</label>
                  	<span style="width: 10%; position: absolute;  top: 0;  right: 0;margin-right: 160px;margin-top: 20px;height: 22px;font-weight: bold;font-size: 20px;"><s:property value="#session.user.unitSymbol"/></span><input type="text" name="structure.cleaningFee" id="FormCleaningFee" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 140px;margin-top: 18px;height: 22px;" value="<s:property value="structure.cleaningFee"/>">
					<label for="FormDeposit" style="position: absolute;top: 0;right: 0;margin-right: 60px;">Deposit</label>
                  	<span style="width: 10%; position: absolute;  top: 0;  right: 0;margin-right: 70px;margin-top: 20px;height: 22px;font-weight: bold;font-size: 20px;"><s:property value="#session.user.unitSymbol"/></span><input type="text" name="structure.deposit" id="FormDeposit" style="width:10%; position: absolute;  top: 0;  right: 0;margin-right: 52px;margin-top: 18px;height: 22px;" value="<s:property value="structure.deposit"/>"/>
           	 	  </div>
           	 	  <!-- View map -->
					<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" type="button" id="view-map-button">
						<span class="ui-button-icon-primary ui-icon"></span>
						<span class="ui-button-text"><s:text name="viewMap"/></span>
					</button>
 				  <div class="type-text">
					<label for="FormVideoLink">Video link</label>
                	<input type="text" name="structure.videolink" id="FormVideoLink"  size="20" value="<s:property value="structure.videolink"/>"/>
              	  </div>
				  <div class="type-text">
					<label for="FormEmail">Email<sup title="This Field is Mandatory.">*</sup></label>
                	<input type="text" class="required email" name="structure.email" id="FormEmail"  size="20" style="width:50%;" value="<s:property value="structure.email"/>"/>
					<label for="FormPhone" style="position: absolute;top: 0;right: 0;margin-right: 165px;margin-top: 0px;">Phone<sup title="This Field is Mandatory.">*</sup></label>
               	 	<input type="text" class="required validPhone" name="structure.phone" id="FormPhone"  style="width:30%; position: absolute;  top: 0;  right: 0;margin-right: 45px;margin-top: 26px;" value="<s:property value="structure.phone"/>"/>
              	  </div>
				  <div class="type-text">				
					<label for="FormBookingType">Booking type</label>
					<select name="structure.haveroomavailable" id="bookingType">
						<option value="1">Direct booking when rooms available</option><option value="2">Booking after ownder authorization</option><option value="0" selected="selected">Temporary NOT available for booking</option></select>
					<label for="FormPersonalPhone" style="position: absolute;top: 0;right: 0;margin-right: 100px;margin-top: 0px;">Personal Phone</label>
               	 	<input type="text" class="validPhone" name="structure.personalPhone" id="FormPersonalPhone"  style="width:30%; position: absolute;  top: 0;  right: 0;margin-right: 43px;margin-top: 20px;" value="<s:property value="structure.personalPhone"/>"/>
              	  </div> 
				  <div class="type-button">
                  	<button class="btn_save ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="btn-save-structure" role="button" type="submit"><span class="ui-button-icon-primary ui-icon ui-icon-check"></span><span class="ui-button-text">Save</span></button>
					<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" type="button">
						<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span>
						<span class="ui-button-text">Cancel</span>
					</button>
              	  </div>
			</div>
				<div class="c50l">
	            	  <div class="type-text">
	                  	<label for="FormNotes">Description</label> 
	                  	<textarea name="structure.notes" id="FormNotes" style="height: 190px;width: 457px;"><s:property value="structure.notes"/></textarea>
	                  </div>
					  <div class="type-text">
	                  	<label for="FormHouseRules">House rules</label> 
	                  	<textarea name="structure.houseRules" id="FormHouseRules" style="height: 90px;width: 457px;"><s:property value="structure.houseRules"/></textarea>
	                  </div>
					  <div class="type-text">
	                  	<label for="FormReachUS">How to reach us</label> 
	                  	<textarea name="structure.reach_us" id="FormReachUS" style="height: 90px;width: 457px;"><s:property value="structure.reach_us"/></textarea>
	                  </div>
	            </div>
	        </form>
			<div id="images"/>
		</div>
	</div>
</body>