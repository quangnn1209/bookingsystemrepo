<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="wizardHeader.jsp" />
<script type="text/javascript">
	$(document).ready(function(){
		$("#startDate").datepicker({ dateFormat: '<s:property value="#session.datePatternJs"/>'.toLowerCase() });
		$("#endDate").datepicker({ dateFormat: '<s:property value="#session.datePatternJs"/>'.toLowerCase() });
	});
</script>
<body>
<div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary ${sessionScope.disabledBack?'disabled-button':'' }" onclick="window.location.href='home'" type="button" ${sessionScope.disabledBack?'disabled="disabled"':'' }>
		<span class="ui-button-text">Exit</span>
	</button>
	<span class="progress-span">Completed <s:property value="#session.wizard.progress"/>%</span>
</div>
<div>
		<jsp:include page="menuSide.jsp" />
		<div class="subcolumns" id="row-edit-container" style="width: 75%;float: right;">	
			<div class="c50l">
               	<div class="type-text">           
    					<strong>Year:</strong>
                 	<span>2014</span>
   		      	</div> 
               	<div class="type-text">	
               		<strong>Name:</strong>
             		<span>default</span>
               	</div>
           	</div>
           	<div class="subcolumns period">
           		<form action="savePeriod" method="post">
           		<s:property value="#session.message.description"/>
           		<input type="hidden" name="period.id" value="<s:property value="period.id"/>"/>
           		<input type="hidden" name="period.id_season" value="<s:property value="period.id_season"/>"/>
					<div class="type-text">
						<div class="c33l">
			    			<div class="subcl type-text">
			      				<span>From:</span> 
			      				<input type="text" name="startDate" style="display: inline;" id="startDate" value="<s:date name="period.startDate" format="dd/MM/yyyy" />"/><img class="ui-datepicker-trigger" src="images/calendar.gif" />
			    			</div>
						</div>
						<div class="c33l">
			    			<div class="subcl type-text">
			      				<span>To:</span> 
			      				<input type="text" name="endDate" style="display: inline;" id="endDate" value="<s:date name="period.endDate" format="dd/MM/yyyy" />"/><img class="ui-datepicker-trigger" src="images/calendar.gif" />
			    			</div>
						</div>
					</div>
	            	<div class="type-button">
						<button class="btn_save ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-check"></span><span class="ui-button-text">Save</span></button>
							<button class="btn-close-edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" type="button">
								<span class="ui-button-icon-primary ui-icon ui-icon-arrowreturnthick-1-w"></span>
								<span class="ui-button-text">Cancel</span>
							</button>
	           		</div>
	           	</form>
			</div>
		</div>
</div>
</body>