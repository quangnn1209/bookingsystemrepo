<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:include page="wizardHeader.jsp" />
<script type="text/javascript">
	$(document).ready(function(){
     	$(".room-type").click(function(){
     		var roomWzId = $(this).find("[name=roomWzId]").val();
     		if($(this).find("[name=isSaved-"+roomWzId+"]").val() == "true"){
     			// Load room by id
                $.ajax({
                  url: "loadRoom?idRoom="+$(this).find("[name=idRoom-"+roomWzId+"]").val() +"&roomWzId="+roomWzId
                }).done(function(response) {
                	$("#row-edit-container").html(response);
                	initialGalleryRoom();
                });	
     		}else{
     			// Load room
                $.ajax({
                  url: "initialRoom?idRoomTypeSingle="+$(this).find("[name=idRoomType]").val()+"&roomWzId="+roomWzId
                }).done(function(response) {
                	$("#row-edit-container").html(response);
                	$("#images").html("");
                });	
     		}
     	});
     	
     	$(".room-type:eq(0)").trigger("click");
	});
</script>
<body>
<div>
	<button class="btn_add_form ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary ${sessionScope.disabledBack?'disabled-button':'' }" onclick="window.location.href='home'" type="button" ${sessionScope.disabledBack?'disabled="disabled"':'' }>
		<span class="ui-button-text">Exit</span>
	</button>
	<span class="progress-span">Completed <s:property value="#session.wizard.progress"/>%</span>
</div>
<div>
	<jsp:include page="menuSide.jsp" />
	<div class="subcolumns beauty" style="width: 75%;float: right;">
		<div id="row-edit-container">
		</div>		
		<div id="images"/>
	</div> 	
</div>
</body>