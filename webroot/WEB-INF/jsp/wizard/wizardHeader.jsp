<%--
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
<title>RITM Booking System</title><!-- (en) Add your meta data here -->
<link rel="stylesheet" type="text/css" href="css/rcarousel.css" />
<link rel="stylesheet" type="text/css" href="css/layout_sliding_door.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.fileupload-ui.css" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/south-street/jquery-ui-1.8.9.custom.css" />
<link rel="stylesheet" type="text/css" href="css/screen/content.css" />
<link rel="stylesheet" type="text/css" href="css/screen/basemod.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.jgrowl.css" />
<link rel="stylesheet" type="text/css" href="yaml/core/base.css" />
<link rel="stylesheet" type="text/css" href="yaml/screen/forms.css" />
<link rel="stylesheet" type="text/css" href="yaml/print/print_003_draft.css" />
<link rel="stylesheet" type="text/css" href="yaml/navigation/nav_slidingdoor.css" />

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.css"/>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js"></script>
<script type='text/javascript' src='js/lib/jquery-ui-1.10.4.min.js'></script>
<%-- <script type='text/javascript' src='js/lib/backbone.js'></script> --%>
<script type='text/javascript' src='js/lib/ftod.js'></script>
<script type='text/javascript' src='js/lib/jquery.colorbox.js'></script>
<script type='text/javascript' src='js/lib/jquery.fileupload-ui.js'></script>
<script type='text/javascript' src='js/lib/jquery.fileupload-uix.js'></script>
<script type='text/javascript' src='js/lib/jquery.fileupload.js'></script>
<script type='text/javascript' src='js/lib/jquery.form.js'></script>
<script type='text/javascript' src='js/lib/jquery.metadata.js'></script>
<script type='text/javascript' src='js/lib/jquery.overlay.min.js'></script>
<script type='text/javascript' src='js/lib/jquery.slimscroll.min.js'></script>
<script type='text/javascript' src='js/lib/jquerymx-1.0.custom.js'></script>
<script type='text/javascript' src='js/lib/underscore-min.js'></script>
<script type='text/javascript' src='js/lib/jquery.i18n.js'></script>
<script type='text/javascript' src='js/lib/jquery.jgrowl_minimized.js'></script>
<script type='text/javascript' src='js/lang/jquery.en.json'></script>
<%-- <script type='text/javascript' src='yaml/core/js/yaml-focusfix.js'></script> --%>
<%-- <script type='text/javascript' src='js/views/common.js'></script> --%>
<%-- <script type='text/javascript' src='js/routers/common.js'></script> --%>
<script type='text/javascript' src='js/helpers/upload.js'></script>
<%-- <script type='text/javascript' src='js/helpers/common.js'></script>
<script type='text/javascript' src='js/collections/image.js'></script> --%>
<script type="text/javascript" src="js/lib/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" href="js/lib/fancybox/jquery.fancybox.css" type="text/css" media="screen" />

<style>
	li{
		padding:10px;
		cursor: pointer;
	}

	ul{
		cursor: default;
	}
	
	ul:HOVER {
		background: transparent;
	}
	
	#images{
		width: 90%;
		padding-left: 25px;
	}
	
	.progress-span{
        background: none repeat scroll 0 0 #f4f4f4;
        border: 1px solid #dddddd;
        border-radius: 5px;
        color: #666;
        float: right;
        font-weight: bold;
        padding: 5px;
	}

	#main-app li{
		border: 1px solid transparent;
	}
	#main-app li:hover{
		border: 1px solid orange;
		background: rgba(247, 200, 14, 0.31);
		border-radius: 5px;
	}
	
	#main-app ul{
		border-radius: 10px;
		border: 5px solid orange;
	}
	
	.yform{
		border: 0px;
	}
	
	.disabled-button{
		background: gray;
	    border: gray;
    }
</style>
<script type="text/javascript">
	var message = '<label class="wzmessage error">This field is required.</label>';
	$(document).ready(function(){
		// Validate required
		$("button:submit").live("click", function(){
			// Remove message before validate
			$(".wzmessage").remove();
			
			var invalid = false;
			$(".required").each(function(){
				if($(this).val() == ""){
					$(this).after(message);
					invalid = true;
				}
			});
			
			if(invalid){
				return false;
			}
		});
		
		$(".btn-close-edit").live("click",function(){
			location.reload();
		});
	});
	
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	
	function enableButton(){
 		var isEnabled = true;
    	$(".room-type").find("span:first").each(function (){
    		if($(this).hasClass("ui-icon-circle-plus")){
    			isEnabled = false;
    		}
    	});
    	
    	if(isEnabled){
    		// Update progress
    		$.ajax({
      			url: 'finishWizard'
            }).done(function(response) {
              	console.log(response);
            });
    		
    		$("#save-button").removeAttr('disabled');
    		$("#save-button").css("background","");
    		$("#save-button").find("span.ui-button-text").text("End");
    	}
 	}
	
	function initialGalleryStructure(){
		// Initial gallery
        $.ajax({
          url: "initialGalleryStructure?idStructure=<s:property value="#session.wizard.idStructure"/>"
        }).done(function(response) {
        	$("#images").html(response);
        });
	}
</script>	