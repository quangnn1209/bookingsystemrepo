/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************//*

 * @class EditPermissionView
 * @parent Backbone.View
 * @constructor
 * Edit a row selected in the listing.
 * @tag views
 * @author LabOpenSource
 
window.EditPermissionView = EditView.extend({
    events: {
        "submit form": "save",
        "click div": "switchMode"
    },
    initialize: function () {
        this.model.bind('change', this.render, this);
		
        var self = this;
        this.id = null;
    },
    render: function () {
		var modelToRender = this.model.toJSON();
        // render main edit view
        $(this.el).html(Mustache.to_html(this.indexTemplate.html(), modelToRender));
        this.$(".yform.json.full").validate();
        $(".btn_save").button({
            icons: {
                primary: "ui-icon-check"
            }
        });
        //button for form reset  
        $(".btn_reset").button({
            icons: {
                primary: "ui-icon-arrowreturnthick-1-w"
            }
        }).click(function (event) {
            var validator = $(this).parents(".yform.json").validate();
            validator.resetForm();
            return false;
        });
        return this;
    },
    clear: function(){
		 this.resetModel(Entity.model({
   		 }));
		 $(this.el).undelegate("div", "click");
    }
});*/