/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
/*
 * @class EditRoleView
 * @parent Backbone.View
 * @constructor
 * Edit a row selected in the listing.
 * @tag views
 * @author LabOpenSource
 */
window.EditRoleView = EditView.extend({
    events: {
        "submit form": "save",
        "click div": "switchMode"
    },
    initialize: function () {
        this.model.bind('change', this.render, this);
        var self = this;
		
		// Load all permission
        this.permissions = [];
        this.permissionCollection = new Permissions();
        this.permissionCollection.fetch({
            success: function() {
           	 self.initializePermissions();

           	 // render again if success is called after render method.           	 
           	 self.render();
           	 },
        });
        
        // Load permission by role id
        this.checkedPermissions = [];
        this.id = null;
    },
    /**
     * Set the list of available propertyTypes.
     * @return {Array} array of { value_name:"", value_id:"", selected: ""} objects.
     */
    initializePermissions: function () {
      	 var self = this;
           _.each(self.permissionCollection.models, function (val) {
          	 self.permissions.push({
          		id: val.attributes.id,
				name: val.attributes.name,
				description: val.attributes.description,
				checked: false
          	 });
           });
           return self.permissions;
	},
	setCheckedPermissions: function () {
    	 var self = this;
    	 _.each(self.permissions, function (val) {
    		 val.checked = false;
    		 if(self.checkedPermissions != undefined)
	    		 for(var i = 0; i < self.checkedPermissions.length; i++){
	    			 if(val.id == self.checkedPermissions[i]){
	    				 val.checked = true;
	    			 }
	    		 }
          });
    	 return self.permissions;
	},
    render: function () {
		var modelToRender = this.model.toJSON();
		this.checkedPermissions = modelToRender.idPermissions;
		// Set permission list
		this.setCheckedPermissions();
		modelToRender.permissions = this.permissions;
        // render main edit view
        $(this.el).html(Mustache.to_html(this.indexTemplate.html(), modelToRender));
        this.$(".yform.json.full").validate();
        $("input#checkall").click(function(){
        	if($(this).is(":checked")){
        		$("input[name=idPermissions]").attr("checked",true);
        	}else{
        		$("input[name=idPermissions]").attr("checked",false);
        	}
        });
        $(".btn_save").button({
            icons: {
                primary: "ui-icon-check"
            }
        });
        //button for form reset  
        $(".btn_reset").button({
            icons: {
                primary: "ui-icon-arrowreturnthick-1-w"
            }
        }).click(function (event) {
            var validator = $(this).parents(".yform.json").validate();
            validator.resetForm();
            return false;
        });
        // call for render associated views
        this.renderAssociated();
        this.delegateEvents();

        return this;
    },
    /**
     * Render associated views
     */
    renderAssociated: function () {
        // check if model has changed, then update collections in associated views
        if (this.model.isNew()) {
            //disable sliders when you add a new PropertyType

        } else if (this.model.get("id") != this.id) {
            this.id = this.model.get("id");
        }
    },
    clear: function(){
		 this.resetModel(Entity.model({
            id_structure: Entity.idStructure
   		 }));
		 $(this.el).undelegate("div", "click");
    }
});