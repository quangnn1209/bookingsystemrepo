/*******************************************************************************
 *
 *  Copyright 2011 - Sardegna Ricerche, Distretto ICT, Pula, Italy
 *
 * Licensed under the EUPL, Version 1.1.
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 *  http://www.osor.eu/eupl
 *
 * Unless required by applicable law or agreed to in  writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 * In case of controversy the competent court is the Court of Cagliari (Italy).
 *******************************************************************************/
  /*
  * @class EditImagesFacilitiesView
  * @parent Backbone.View
  * @constructor
  * Edit a row selected in the listing.
  * @tag views
  * @author LabOpenSource
  */
 window.EditUserAccountView = EditView.extend({
     events: {
        "submit #edit-form": "updateAccount"
     },
     initialize: function () {
         this.model.bind('change', this.render, this);
    	 this.id = null;
    	 this.userAccountList = [];
    	 this.userAccounts = new UserAccounts(null);    	 
    	 var self = this;
         this.userAccounts.fetch({
             success: function() {
            	 self.initializeUserAccounts();
            	 // render again if success is called after render method.
            	 self.render();
            	 },
         });
         
         this.availableLanguage = [];
         this.initializeLanguage();
         this.roles = [];
         this.roleCollections = new Roles();
         this.roleCollections.fetch({
             success: function() {
            	 self.initializeRole();
            	 // render again if success is called after render method.
            	 self.render();
            	 },
         });
     },
     initializeRole: function () {
    	 var self = this;
         _.each(self.roleCollections.models, function (val) {
        	 self.roles .push({
        		 name: val.attributes.name,
        		 value:val.attributes.id
        	 });
         });
         return self.roles;
     },
     setRole: function (role) {
       	 var self = this;
            _.each(self.roles, function (val) {
            val.selected = false;
            if (val.value == role) {
           	 val.selected = true;
            	}   		 
            });
         return self.roles;
     },
     initializeLanguage: function () {
       	 this.availableLanguage = [{name: 'English', id: '0'},
       	                           {name: 'Italia', id: '1'},
       	                           {name: 'Francais', id: '2'},
       	                           {name: 'Espanol', id: '3'}];
     },
     setLanguage: function (id_language) {
       	 var self = this;
            _.each(self.availableLanguage, function (val) {
            val.selected = false;
            if (val.id == id_language) {
           	 val.selected = true;
            	}   		 
            });
            return self.availableLanguage;
     },
     /**
      * Set the list of available roomTypes.
      * @return {Array} array of { value_name:"", value_id:"", selected: ""} objects.
      */
     initializeUserAccounts: function () {
    	 var self = this;
         _.each(self.userAccounts.models, function (val) {
        	 self.userAccountList.push({
        		 email: val.attributes.email,
        		 name: val.attributes.name,
        		 phone: val.attributes.phone,
        		 surname: val.attributes.surname,
        		 id_language: val.attributes.id_language,
        		 idritmuser: val.attributes.idritmuser,
        		 id_role:val.attributes.id_role
        	 });
         });
         return self.userAccountList;
     },     
     render: function () {
    	 if(this.userAccountList.length > 0){
    		 this.setLanguage(this.userAccountList[0].id_language);
    		 this.userAccountList[0].availableLanguage = this.availableLanguage;
    		 this.setRole(this.userAccountList[0].id_role);
    		 this.userAccountList[0].roles = this.roles;
	    	 // render main edit view
	    	 $("#row-edit-container").html(Mustache.to_html(this.indexTemplate.html(), this.userAccountList[0]));
	         // add validation check
	         this.$(".yform").validate();
	         // renderize buttons
	         $(".btn_save").button({
	             icons: {
	                 primary: "ui-icon-check"
	             }
	         });
	         //button for form reset  
	         $(".btn_reset").button({
	             icons: {
	                 primary: "ui-icon-arrowreturnthick-1-w"
	             }
	         }).click(function (event) {
	             var validator = $(this).parents(".yform.json").validate();
	             validator.resetForm();
	             return false;
	         });
    	 }
         return this;
     },
     clear: function(){
		 this.resetModel(Entity.model({}));
		 $("#row-edit-container").undelegate("div", "click");
     },
     updateAccount: function (e) {
     	e.preventDefault();
     	$("#edit-form").submitForm("updateAccount.action");
     }
 });